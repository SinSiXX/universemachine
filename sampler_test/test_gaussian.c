#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <string.h>
#include "../sampler.h"
#include "../mt_rand.h"
#include "../check_syscalls.h"
#include <inttypes.h>

int64_t NDIM = 3;
double ASPECT = 1; //0.01;
double *inv_radii = NULL;
int64_t NUM_STEPS = 100000;

double chi2(double *pos) {
  int64_t i;
  double c2 = 0;
  for (i=0; i<NDIM; i++) {
    double dx = pos[i]*inv_radii[i];
    c2 += dx*dx;
  }
  //if (pos[0] < -0.1) return -1;
  //if (NDIM>1 && pos[1] > 0.3) return -1;
  return c2;
}

int main(int argc, char **argv) {
  int64_t i, j;
  double *pos = NULL;

  if (argc > 1) NDIM = atol(argv[1]);

  check_realloc_s(pos, sizeof(double), NDIM);
  check_realloc_s(inv_radii, sizeof(double), NDIM);
  for (i=0; i<NDIM; i++) inv_radii[i] = pow(ASPECT, -(double)i/((double)NDIM-1.0));

  int64_t nwalkers = 684; //(NDIM < 10) ? (100) : (10*NDIM);
  NUM_STEPS = (NDIM < 20) ? 40000 : (NDIM*2000);
    
  struct EnsembleSampler *e = new_sampler(nwalkers, NDIM, 2.0);
  e->mode = DJ_SAMPLER;
  
  for (i=0; i<e->num_walkers; i++) {
    for (j=0; j<NDIM; j++) pos[j] = normal_random(0, 1); //dr250()*0.01;
    //printf("%f %f %f\n", pos[0], pos[1], pos[2]);
    init_walker_position(e->w+i, pos);
  }
  for (i=0; i<NUM_STEPS; i++) {
    struct Walker *w = get_next_walker(e);
    update_walker(e, w, chi2(w->params));
    if (ensemble_ready(e)) {
      update_ensemble(e);
      if (i>NUM_STEPS/2)
	write_accepted_steps_to_file(e, stdout, 0);
    }
  }  
  fprintf(stderr, "Acceptance fraction: %f\n", acceptance_fraction(e));
  if (e->mode == DJ_SAMPLER)
    for (i=0; i<NDIM; i++) {
      fprintf(stderr, "%"PRId64" %g\n", i, e->avg[i]);
    }
  return 0;
}

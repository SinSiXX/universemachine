#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <math.h>
#include <stdint.h>
#include <sys/mman.h>
#include "check_syscalls.h"
#include "corr_lib.h"
#include "stringparse.h"
#include "make_sf_catalog.h"
#include "config.h"
#include "config_vars.h"
#include "mt_rand.h"
#include "correl/cross_correl_lib.h"
#include "correl/matrices.h"
#include "correl/regions.h"
#include "distance.h"

#define SPEED_OF_LIGHT (299792.458)

extern struct catalog_halo *halos;
int64_t nh=0, ng=0;
FILE *logfile; //Unused

#define FAST3TREE_TYPE   struct catalog_galaxy
#define FAST3TREE_PREFIX SAMPLE_CORR_SM
#define FAST3TREE_DIM    2
#include "fast3tree.c"

void load_vbins(char *filename);
int sort_by_lvmp(const void *a, const void *b);
void scorr_load_boxes(float target, float orphan_thresh);
double calc_sm(double lvmp);

struct vbin {
  double v, nd, sm, qf;
};
struct vbin *vbins = NULL;
int64_t nvb = 0;


int main(int argc, char **argv)
{
  int64_t i, j, k;

  if (argc < 16) {
    fprintf(stderr, "Usage: %s config.cfg scale sm_vmp.dat sm_low sm_high vmp_q0.5 q_w f_nr sig_sm min_z max_z regions_file completeness redshift_error samples...\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  do_config(argv[1]);
  setup_config();
  float scale = atof(argv[2]);
  load_vbins(argv[3]);
  float smlow = atof(argv[4]);
  float smhigh = atof(argv[5]);
  float vmp_q = atof(argv[6]);
  float vmp_qw = atof(argv[7]);
  float fnr = atof(argv[8]);
  float sig_sm = atof(argv[9]);
  float min_z = atof(argv[10]);
  float max_z = atof(argv[11]);
  float completeness = atof(argv[13]);
  float z_error = atof(argv[14]);
  int64_t samples = atoll(argv[15]);

  float max_z_error = (z_error > 1) ? z_error : (1.0+max_z)*z_error*SPEED_OF_LIGHT;
  
  double *output_corr = NULL;
  cross_correl_library_init(smlow, smhigh, CORR_LENGTH_PI/h0, 10.0/h0, min_z, max_z, 1e6, argv[12], &output_corr);


  r250_init(87L);
  scorr_load_boxes(scale, 0.6);
  for (i=0; i<nh; i++) {
    double sm = calc_sm(halos[i].lvmp) + normal_random(0, sig_sm);
    halos[i].sm = sm;
    if ((sm < smlow || sm > smhigh) && sm < 11) {
      halos[i] = halos[nh-1];
      nh--;
      i--;
      continue;
    }
  }
  //double volume = pow(BOX_SIZE/h0, 3.0);
  int64_t ng = nh;
  check_realloc_s(galaxies, sizeof(struct catalog_galaxy), ng);

  float ra1 = fnr;
  float ra2 = sqrt(1.0-fnr*fnr);
  float hz = 100.0*sqrt(Om*pow(scale, -3.0)+Ol)*scale;

  struct catalog_galaxy *galaxies2 = NULL;
  int64_t num_g2 = 0;

  for (i=0; i<nh; i++) {
    float ra = ra1*halos[i].rank1 + ra2*halos[i].ra;
    float rank = 0.5*(1.0+erf(ra*M_SQRT1_2));
    struct catalog_galaxy *g = galaxies + i;
    float fq = 0.5*(1.0+erf((halos[i].lvmp-vmp_q)*M_SQRT1_2/(vmp_qw)));

    g->lsfr = (rank > fq) ? 1 : -1;
    g->lsm = halos[i].sm;

    memcpy(g->opos, halos[i].pos, sizeof(float)*6);
    g->pos[0] = halos[i].pos[0];
    g->pos[1] = halos[i].pos[1];
    g->pos[2] = halos[i].pos[2] + halos[i].pos[5]/hz;
    while (g->pos[2]<0) g->pos[2] += BOX_SIZE;
    while (g->pos[2]>BOX_SIZE) g->pos[2] -= BOX_SIZE;

    for (j=-1; j<=1; j++) {
      for (k=-1; k<=1; k++) {
	struct catalog_galaxy g2 = *g;
	g2.pos[0] += BOX_SIZE*j;
	g2.pos[1] += BOX_SIZE*k;
	if ((g2.pos[0] > -CORR_LENGTH_RP) && (g2.pos[0] < BOX_SIZE+CORR_LENGTH_RP) &&
	    (g2.pos[1] > -CORR_LENGTH_RP) && (g2.pos[1] < BOX_SIZE+CORR_LENGTH_RP)) {
	    check_realloc_every(galaxies2, sizeof(struct catalog_galaxy), num_g2, 1000);
	    galaxies2[num_g2] = g2; 
	    num_g2++;
	}
      }
    }
  }

  struct fast3tree *t = fast3tree_init(ng, galaxies);
  struct fast3tree_results *r = fast3tree_results_init();
  _fast3tree_set_minmax(t, 0, BOX_SIZE);

  init_dists();

  int64_t nq=0, na=0, nsf=0, na11=0;
  if (0) {
    init_corr_tree(galaxies2, num_g2);
    int64_t *bin_counts = NULL;
    check_realloc_s(bin_counts, sizeof(int64_t), NUM_BINS*3*ng);
    for (i=0; i<ng; i++) {
      clear_corrs();
      bin_single_galaxy(galaxies+i);
      memcpy(bin_counts+i*NUM_BINS*3,            corr,    NUM_BINS*sizeof(int64_t));
      memcpy(bin_counts+i*NUM_BINS*3+NUM_BINS,   corr_sf, NUM_BINS*sizeof(int64_t));
      memcpy(bin_counts+i*NUM_BINS*3+NUM_BINS*2, corr_q,  NUM_BINS*sizeof(int64_t));
    }
    
    //Compute full correlation function
    printf("#Sample 0\n");
    for (j=0; j<ng; j++) {
      na++;
      if (galaxies[j].lsfr > 0) nsf++;
      if (galaxies[j].lsfr > 0) nsf++;
      else nq++;
      int64_t *gcorr = bin_counts + j*NUM_BINS*3;
      for (k=0; k<NUM_BINS; k++) {
	corr[k] += gcorr[k];
	corr_sf[k] += gcorr[k+NUM_BINS];
	corr_q[k] += gcorr[k+NUM_BINS*2];
      }
    }
    printf("#NA: %"PRId64"; NSF: %"PRId64"; NQ: %"PRId64"\n", na, nsf, nq);
    float vol = pow(BOX_SIZE, 3);
    float da = na/vol, dsf = nsf/vol, dq=nq/vol;
    printf("#Vol: %g\n", vol);
    printf("#R C(all-all) C(sf-sf) C(q-q)\n");
    if (na < 1) na = 1;
    if (nq < 1) nq = 1;
    if (nsf < 1) nsf = 1;
    for (j=0; j<NUM_BINS; j++) {
      float a1 = corr_dists[j]*M_PI;
      float a2 = corr_dists[j+1]*M_PI;
      float av_r = sqrt(0.5*(a1+a2)/M_PI);
      float a = a2-a1;
      float raa = 2.0*da*a*CORR_LENGTH_PI;
      float rasf = 2.0*dsf*a*CORR_LENGTH_PI;
      float raq = 2.0*dq*a*CORR_LENGTH_PI;
      printf("%f %e %e %e %e %g %g %g %g\n", av_r, (corr[j]-na*raa)/(na*da*a), (corr_sf[j]-nsf*rasf)/(nsf*dsf*a), (corr_q[j]-nq*raq)/(nq*dq*a), (float)corr[j], da, a, raa, (float)corr_bin_of(av_r*av_r));
    }
    fflush(stdout);
  }

  float rad = comoving_distance_h(max_z)+(600.0+2.0*3000.0+3.0*max_z_error)/hz; //For peculiar velocities
  //Compute correlation function
  struct correl_galaxy *hg = NULL;
  struct correl_galaxy *hg11 = NULL;
  int64_t alloced_hg = 0;
  for (i=0; i<samples; i++) {
    float c[3];
    int64_t min_wrap[3] = {0}, max_wrap[3] = {1,1,1}, w[3], use_wrap = 0;
    printf("#Sample %"PRId64"\n", i+1);
    c[0] = dr250()*BOX_SIZE;
    c[1] = dr250()*BOX_SIZE;
    c[2] = dr250()*BOX_SIZE;
    float rot[3][3];
    rand_rotation(rot);
    if (rad < 0.5*BOX_SIZE)
      fast3tree_find_sphere_periodic(t, r, c, rad);
    else {
      fast3tree_find_sphere(t, r, c, 2.0*BOX_SIZE);
      for (j=0; j<3; j++) {
	min_wrap[j] = floor((c[j] - rad)/BOX_SIZE);
	max_wrap[j] = ceil((c[j] + rad)/BOX_SIZE);
      }
      use_wrap = 1;
    }
    
    int64_t nq=0, na=0, nsf=0;
    //    check_realloc_s(hg, sizeof(struct correl_galaxy), r->num_points);
    double rs = 0;
    for (w[0]=min_wrap[0]; w[0]<max_wrap[0]; w[0]++) {
    for (w[1]=min_wrap[1]; w[1]<max_wrap[1]; w[1]++) {
    for (w[2]=min_wrap[2]; w[2]<max_wrap[2]; w[2]++) {
      for (j=0; j<r->num_points; j++) {
	if ((completeness < 1.0) && (dr250() > completeness)) continue;
	struct catalog_galaxy cag = *(r->points[j]);
	for (k=0; k<3; k++) {
	  cag.opos[k] -= c[k];
	  if (use_wrap) {
	    cag.opos[k] += w[k]*BOX_SIZE;
	  } else {
	    if (cag.opos[k] > BOX_SIZE/2.0) cag.opos[k] -= BOX_SIZE;
	    if (cag.opos[k] < -BOX_SIZE/2.0) cag.opos[k] += BOX_SIZE;
	  }
	  rs += cag.opos[k]*cag.opos[k];
	}
	rs = sqrt(rs);
	if (rs > rad) continue;

	float pos[3];
	mult_vector(rot, cag.opos, pos);
	memcpy(cag.opos, pos, sizeof(float)*3);
	mult_vector(rot, cag.opos+3, pos);
	memcpy(cag.opos+3, pos, sizeof(float)*3);
	memset(pos, 0, sizeof(float)*3);
	
	for (k=0; k<3; k++) {
	  if (rs > 0) pos[k] = cag.opos[k] / rs;
	}

	double dec = asin(pos[2])*180.0/M_PI;
	double ra = atan2(pos[1],pos[0])*180.0/M_PI;
	if (ra<0) ra+=360.0;
	if (!within_r(ra, dec)) continue;

	
	double vp = 0;
	for (k=0; k<3; k++) vp += cag.opos[k+3]*pos[k];

	double zp = vp / SPEED_OF_LIGHT;
	double zcosmo = comoving_distance_to_redshift(rs/h0);
	double zfinal = (1.0+zp)*(1.0+zcosmo) - 1.0;

	double tz_err = (z_error < 1) ? ((1.0+zfinal)*z_error) : (z_error / SPEED_OF_LIGHT);
	zfinal += normal_random(0, tz_err);
	if (zfinal < min_z || zfinal > max_z) continue;
	
	check_realloc_smart(hg, sizeof(struct correl_galaxy), alloced_hg, na+1);
	
	struct correl_galaxy *cg = hg+na;
	na++;
	cg->ra = ra;
	cg->dec = dec;
	cg->z = zfinal;
	cg->sm = cag.lsm;
	cg->specsfr = cag.lsfr;
      }
    }}}

    na11 = na;
    hg11 = hg;
    if (smhigh <= 11) {
      for (j=0; j<na; j++) {
	if (hg[j].sm > 11) {
	  struct correl_galaxy tmp = hg[j];
	  na--;
	  hg[j] = hg[na];
	  hg[na] = tmp;
	  j--;
	}
      }
      hg11 = hg+na;
      na11 -= na;
    }

    nsf = na;
    for (j=0; j<nsf; j++) {
      if (hg[j].specsfr < 0) {
	struct correl_galaxy tmp = hg[j];
	nsf--;
	hg[j] = hg[nsf];
	hg[nsf] = tmp;
	j--;
      }
    }
    nq = na - nsf;
    
    double ac[10], acsf[10], acq[10], a11[10];
    set_sm_limits(smlow, smhigh);
    nq = set_hg2(hg+nsf, nq);
    obs_calc_cross_corr(output_corr);
    memcpy(acq, output_corr+5, 10*sizeof(double));
    nsf = set_hg2(hg, nsf);
    obs_calc_cross_corr(output_corr);
    memcpy(acsf, output_corr+5, 10*sizeof(double));
    na = set_hg2(hg, na);
    obs_calc_cross_corr(output_corr);
    memcpy(ac, output_corr+5, 10*sizeof(double));

    if (smhigh <= 11) {
      switch_trees();
      set_sm_limits(11, 15);
      set_hg(hg11, na11);
      obs_calc_cross_corr(output_corr);
      memcpy(a11, output_corr+5, 10*sizeof(double));
    } else {
      memcpy(a11, ac, 10*sizeof(double));
    }
    switch_trees(); //Free tree1
    switch_trees(); //Free tree2
    
      
    printf("#NA: %"PRId64"; NSF: %"PRId64"; NQ: %"PRId64"; NA11: %"PRId64"\n", na, nsf, nq, na11);
    printf("#R C(all-all) C(sf-sf) C(q-q) C(A11 x A)\n");
    for (j=0; j<NUM_BINS; j++) {
      float a1 = corr_dists[j]*M_PI;
      float a2 = corr_dists[j+1]*M_PI;
      float av_r = sqrt(0.5*(a1+a2)/M_PI);
      printf("%f %e %e %e %e\n", av_r, ac[j], acsf[j], acq[j], a11[j]);
    }
    fflush(stdout);
  }
  return 0;
}


int sort_by_lvmp(const void *a, const void *b) {
  const struct catalog_halo *c = a;
  const struct catalog_halo *d = b;
  if (c->lvmp > d->lvmp) return -1;
  if (c->lvmp < d->lvmp) return 1;
  return 0;
}


void scorr_load_boxes(float target, float orphan_thresh) {
  char buffer[1024];
  int64_t i, box_num, min_snap, offset, snap, n;
  float scale;

  for (box_num=0; box_num<NUM_BLOCKS; box_num++) {
    num_scales = 0;
    //First load offsets from file:
    snprintf(buffer, 1024, "%s/offsets.box%"PRId64".txt", INBASE, box_num);
    FILE *in = check_fopen(buffer, "r");
    while (fgets(buffer, 1024, in)) {
      if (buffer[0] == '#') {
	if (!strncmp(buffer, "#Total halos: ", 14)) {
	  offset = atol(buffer+14);
	  check_realloc_every(offsets, sizeof(int64_t), num_scales, 100);
	  offsets[num_scales] = offset;
	}
	continue;
      }
      n = sscanf(buffer, "%"SCNd64" %f %"SCNd64, &snap, &scale, &offset);
      if (n!=3) continue;
      check_realloc_every(offsets, sizeof(int64_t), num_scales, 100);
      check_realloc_every(scales, sizeof(float), num_scales, 100);
      scales[num_scales] = scale;
      offsets[num_scales] = offset;
      num_scales++;
    }
    fclose(in);
    
    min_snap=0;
    for (i=1; i<num_scales; i++)
      if (fabs(target-scales[min_snap])>fabs(target-scales[i])) min_snap = i;

    int64_t to_read = offsets[min_snap+1]-offsets[min_snap];

    //Then load halos
    snprintf(buffer, 1024, "%s/cat.box%"PRId64".dat", INBASE, box_num);
    in = check_fopen(buffer, "r");
    check_fseeko(in, offsets[min_snap]*sizeof(struct catalog_halo), 0);
    check_realloc_s(halos, sizeof(struct catalog_halo), nh+to_read);
    check_fread(halos+nh, sizeof(struct catalog_halo), to_read, in);
    fclose(in);
    nh += to_read;
  }

  for (i=0; i<nh; i++) {
    if ((halos[i].flags & ORPHAN_FLAG) && (halos[i].v < orphan_thresh*halos[i].vmp)) {
      nh--;
      halos[i] = halos[nh];
      i--;
    }
  }
}


int sort_vbins(const void *a, const void *b) {
  const struct vbin *c = a;
  const struct vbin *d = b;
  if (c->v < d->v) return -1;
  if (c->v > d->v) return 1;
  return 0;
}

void load_vbins(char *filename) {
  FILE *in = check_fopen(filename, "r");
  char buffer[1024];
  struct vbin v = {0};
  while (fgets(buffer, 1024, in)) {
    if (sscanf(buffer, "%lf %lf %lf", &v.v, &v.nd, &v.sm)<3) continue;
    if (!v.nd) continue;
    check_realloc_every(vbins,sizeof(struct vbin), nvb, 10);
    vbins[nvb] = v;
    nvb++;
  }
  fclose(in);
  qsort(vbins, nvb, sizeof(struct vbin), sort_vbins);
}

double calc_sm(double lvmp) {
  int64_t i;
  if (lvmp <= vbins[0].v) return vbins[0].sm;
  for (i=1; i<nvb; i++)
    if (vbins[i].v > lvmp) break;
  if (i==nvb) return vbins[nvb-1].sm;
  double sm = vbins[i-1].sm;
  sm += (lvmp - vbins[i-1].v)*(vbins[i].sm-vbins[i-1].sm)/(vbins[i].v - vbins[i-1].v);
  return sm;
}

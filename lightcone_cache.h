#ifndef _LIGHTCONE_CACHE_H_
#define _LIGHTCONE_CACHE_H_

#include <inttypes.h>
#include "make_sf_catalog.h"

#define CACHE_SIZE 50
#define MASS_LIMIT 1e9

struct halo_cat {
  float scale;
  int64_t num_halos, first_halo_index;
  struct catalog_halo *loc[CACHE_SIZE][CACHE_SIZE][CACHE_SIZE];
  int ncache[CACHE_SIZE][CACHE_SIZE][CACHE_SIZE];
  struct halo_cat *next;
};

extern int64_t num_halos;
extern struct catalog_halo *halos;

void load_halos(struct halo_cat **head);
void load_cat_data(struct halo_cat *cat);
void clear_halos(void);

#endif /* _LIGHTCONE_CACHE_H_ */

#ifndef LINALG_H
#define LINALG_H

#include <inttypes.h>

int solve_lineq(int64_t dim, double *matrix, double *res);

#endif /*LINALG_H*/

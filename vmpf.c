#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <math.h>
#include <stdint.h>
#include <sys/mman.h>
#include "check_syscalls.h"
#include "corr_lib.h"
#include "stringparse.h"
#include "make_sf_catalog.h"
#include "config.h"
#include "config_vars.h"
#include "mt_rand.h"

struct catalog_halo *halos;
int64_t nh=0, ng=0;
FILE *logfile; //Unused
int64_t *offsets=NULL, *num_halos=NULL, num_scales=0;
float *scales = NULL;

void scorr_load_boxes(float target);

#define VMP_MIN 1
#define VMP_NBINS 40
#define VMP_BPDEX 16

int main(int argc, char **argv)
{
  int64_t i;

  if (argc < 3) {
    fprintf(stderr, "Usage: %s config.cfg scale\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  int64_t counts[VMP_NBINS] = {0};
  do_config(argv[1]);
  setup_config();
  float scale = atof(argv[2]);
  r250_init(87L);
  scorr_load_boxes(scale);
  
  for (i=0; i<nh; i++) {
    int64_t bin = VMP_BPDEX*(halos[i].lvmp-VMP_MIN);
    if (bin<0 || bin >= VMP_NBINS) continue;
    counts[bin]++;
  }

  double volume = pow(BOX_SIZE/h0, 3.0);
  printf("#errors: linear\n");
  for (i=0; i<VMP_NBINS; i++) {
    printf("%f %g\n", VMP_MIN+(double)(i+0.5)/(double)VMP_BPDEX, (double)counts[i]*(double)VMP_BPDEX/volume);
  }
  return 0;
}


void scorr_load_boxes(float target) {
  char buffer[1024];
  int64_t i, box_num, min_snap, offset, snap, n;
  float scale;

  for (box_num=0; box_num<NUM_BLOCKS; box_num++) {
    num_scales = 0;
    //First load offsets from file:
    snprintf(buffer, 1024, "%s/offsets.box%"PRId64".txt", INBASE, box_num);
    FILE *in = check_fopen(buffer, "r");
    while (fgets(buffer, 1024, in)) {
      if (buffer[0] == '#') {
	if (!strncmp(buffer, "#Total halos: ", 14)) {
	  offset = atol(buffer+14);
	  check_realloc_every(offsets, sizeof(int64_t), num_scales, 100);
	  offsets[num_scales] = offset;
	}
	continue;
      }
      n = sscanf(buffer, "%"SCNd64" %f %"SCNd64, &snap, &scale, &offset);
      if (n!=3) continue;
      check_realloc_every(offsets, sizeof(int64_t), num_scales, 100);
      check_realloc_every(scales, sizeof(float), num_scales, 100);
      scales[num_scales] = scale;
      offsets[num_scales] = offset;
      num_scales++;
    }
    fclose(in);
    
    min_snap=0;
    for (i=1; i<num_scales; i++)
      if (fabs(target-scales[min_snap])>fabs(target-scales[i])) min_snap = i;

    printf("#Loading box %"PRId64" snap %"PRId64" (a=%f)\n", box_num, min_snap, scales[min_snap]);
    int64_t to_read = offsets[min_snap+1]-offsets[min_snap];

    //Then load halos
    snprintf(buffer, 1024, "%s/cat.box%"PRId64".dat", INBASE, box_num);
    in = check_fopen(buffer, "r");
    check_fseeko(in, offsets[min_snap]*sizeof(struct catalog_halo), 0);
    check_realloc_s(halos, sizeof(struct catalog_halo), nh+to_read);
    check_fread(halos+nh, sizeof(struct catalog_halo), to_read, in);
    fclose(in);
    nh += to_read;
  }
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <math.h>
#include <stdint.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <gsl/gsl_interp.h>
#include <assert.h>
#include "mt_rand.h"
#include "check_syscalls.h"
#include "stringparse.h"

#define SPOINTS_PER_DEX 5.0
#define FPOINTS_PER_SPOINT 100
#define SUB_PER_FBIN 1000

int64_t num_halos=0;

struct p_vmp {
  double p, lvmp;
};

struct p_vmp *p_vmps = NULL;

double perc_to_normal(double p) {
  double x1 = -14;
  double x2 = 14;
  while (x2-x1 > 1e-7) {
    double half = 0.5*(x1+x2);
    double perc = 0.5*(1.0+erf(half));
    if (perc > p) { x2 = half; }
    else { x1 = half; }    
  }
  return ((x1+x2)*M_SQRT1_2);
}


static inline double spline_offset(gsl_interp *spline, gsl_interp_accel *acc, int64_t fbmin, int64_t fbmax, int64_t *counts, int64_t *perc, double *min_p, double *max_p, double *vmp3, double *lvmp, double *xa, double *ya) {
  int64_t i;
  double tot=0, wsum=0;
  gsl_interp_accel_reset(acc);
  for (i=fbmin; i<fbmax; i++) {
    if (!counts[i]) continue;
    wsum += counts[i]*vmp3[i];
    double pthresh = gsl_interp_eval(spline, xa, ya, lvmp[i], acc);
    if ((max_p[i]<=min_p[i]+0.00001*min_p[i]) || pthresh > max_p[i] || pthresh < min_p[i]) {
      /*if (min_p[i] != 0 && (max_p[i]<=min_p[i]+0.00001*fabs(min_p[i])) && 
	  ((pthresh > min_p[i]-0.01*fabs(min_p[i])) && (pthresh < min_p[i]+0.01*fabs(min_p[i]))))
	tot += counts[i]*vmp3[i]*(pthresh - (min_p[i]-0.01*fabs(min_p[i])))/(0.02*fabs(min_p[i]));
	else*/ if (pthresh > min_p[i]) tot += counts[i]*vmp3[i];
      continue;
    }
    int64_t pbin = i*SUB_PER_FBIN;
    double sf = (pthresh - min_p[i])/(max_p[i]-min_p[i])*(double)(SUB_PER_FBIN);
    int64_t sbin = sf;
    sf -= sbin;
    int64_t pbm1 = 0;
    if (sbin > 0) pbm1 = perc[pbin+sbin-1];
    tot += (pbm1 + sf*(perc[pbin+sbin]-pbm1))*vmp3[i];
  }
  return ((tot / wsum) - 0.5);
}


void fit_splines(FILE *out, double low, double high, struct p_vmp *p, int64_t np)
{
  //Basic sanity checks; min/max calculation
  if ((((high-low) < 0.01) && !(high==1.0 || low==0.0)) || (np < 30)) return;
  if ((high-low) < 0.00001) return; 
  int64_t i, j, error=0;
  int64_t np_low = np;
  double min_lv=p[0].lvmp, max_lv=p[0].lvmp;
  for (i=1; i<np; i++) {
    if (min_lv > p[i].lvmp) min_lv = p[i].lvmp;
    if (max_lv < p[i].lvmp) max_lv = p[i].lvmp;
  }
  if (min_lv == max_lv) return;
  min_lv *= 0.9999;
  max_lv *= 1.0001;

  //Decide on number of spline points, allocate spline
  int64_t npoints = SPOINTS_PER_DEX*(max_lv - min_lv)+1.5;
  if (npoints < 3) npoints = 3;
  double bpdex = (double)(npoints-1) / (max_lv-min_lv);
  double inv_bpdex = 1.0/bpdex;
  const gsl_interp_type *T = (npoints > 4) ? gsl_interp_akima : gsl_interp_cspline;
  gsl_interp *spline = gsl_interp_alloc(T, npoints);
  gsl_interp_accel *acc = gsl_interp_accel_alloc();
  double *xa=NULL, *ya = NULL;
  check_realloc_s(xa, sizeof(double), npoints);
  check_realloc_s(ya, sizeof(double), npoints);
  for (i=0; i<npoints; i++) { xa[i] = min_lv + inv_bpdex*i; }
  
  //Allocate fine bins
  int64_t fbins = (npoints-1)*FPOINTS_PER_SPOINT;
#define alloc_clear_set(x,t,s) t *x = NULL; check_realloc_s(x, sizeof(t), s); memset(x, 0, sizeof(t)*s);
  alloc_clear_set(counts, int64_t, fbins);
  alloc_clear_set(perc, int64_t, fbins*(1+SUB_PER_FBIN));
  alloc_clear_set(min_p, double, fbins);
  alloc_clear_set(max_p, double, fbins);
  alloc_clear_set(vmp3, double, fbins);
  alloc_clear_set(lvmp, double, fbins);
  alloc_clear_set(smax, double, npoints);
  alloc_clear_set(smin, double, npoints);
  alloc_clear_set(old_ya, double, npoints);

  //Populate fine bins
  double f_bpdex = (double)fbins/(max_lv-min_lv);
  //double f_inv_bpdex = 1.0/f_bpdex;
  for (i=0; i<np; i++) {
    int64_t fbin = f_bpdex*(p[i].lvmp-min_lv);
    if (!counts[fbin] || p[i].p < min_p[fbin]) min_p[fbin] = p[i].p;
    if (!counts[fbin] || p[i].p > max_p[fbin]) max_p[fbin] = p[i].p;
    lvmp[fbin] += p[i].lvmp;
    vmp3[fbin] += pow(10, 3.0*p[i].lvmp);
    counts[fbin]++;
  }
  for (i=0; i<np; i++) {
    int64_t fbin = f_bpdex*(p[i].lvmp-min_lv);
    int64_t pbin = fbin*SUB_PER_FBIN;
    if (min_p[fbin]==max_p[fbin]) { perc[pbin]++; continue; }
    int64_t sbin = (p[i].p - min_p[fbin])/(max_p[fbin]-min_p[fbin])*(double)(SUB_PER_FBIN-0.01);
    perc[pbin+sbin]++;
  }
  for (i=0; i<fbins; i++) {
    int64_t pbin = i*SUB_PER_FBIN;
    if (counts[i]) {
      lvmp[i] /= (double)counts[i];
      vmp3[i] /= (double)counts[i];
    }
    for (j=1; j<SUB_PER_FBIN; j++)
      perc[j+pbin] += perc[j+pbin-1];
  }

  //Initial guess for spline based on bin medians
  int64_t zero_range = 0;
  for (i=0; i<npoints; i++) {
    int64_t fbmin = (i-0.5)*FPOINTS_PER_SPOINT;
    int64_t fbmax = (i+0.5)*FPOINTS_PER_SPOINT;
    if (fbmin < 0) fbmin = 0;
    if (fbmax > fbins) fbmax = fbins;
    double wsum = 0;
    int64_t j=0;
    smin[i] = -1e300;
    smax[i] = 1e300;
    for (j=fbmin; j<fbmax; j++) {
      if (!counts[j]) continue;
      wsum += vmp3[j]*counts[j];
      if (smin[i]==-1e300 || smin[i] > min_p[j]) smin[i] = min_p[j];
      if (smax[i]==1e300 || smax[i] < max_p[j]) smax[i] = max_p[j];
    }
    if (smax[i] == 1e300) { assert(i>0); smin[i] = smin[i-1]; smax[i] = smax[i-1]; }
    if (smax[i] <= smin[i]+0.00001*fabs(smin[i])) { ya[i] = 0.5*(smin[i]+smax[i]); zero_range++; continue; }
    double pthresh=smin[i], dpthresh=(smax[i]-smin[i])/1000.0;
    for (; pthresh < smax[i]; pthresh += dpthresh) {
      double wsum2 = 0;
      for (j=fbmin; j<fbmax; j++) {
	if (!counts[j]) continue;
	if (max_p[j]==min_p[j] || min_p[j]>pthresh || max_p[j]<pthresh) {
	  if (max_p[j] < pthresh) wsum2 += vmp3[j]*counts[j];
	  continue;
	}
	int64_t pbin = j*SUB_PER_FBIN;
	int64_t sbin = (pthresh - min_p[j])/(max_p[j]-min_p[j])*(double)(SUB_PER_FBIN);
	if (sbin > 0)
	  wsum2 += vmp3[j]*perc[pbin+sbin-1];
      }
      if (wsum2 > 0.5*wsum) break;
    }
    ya[i] = pthresh;
  }

  //Can't subdivide in this case
  if (zero_range == npoints) { error=1; goto out; }
  
  //Fit spline to fine bins
#define CALC_ERROR spline_offset(spline, acc, fbmin, fbmax, counts, perc, min_p, max_p, vmp3, lvmp, xa, ya)
  double last_error = 0;
  for (i=0; i<npoints; i++) {
    int64_t fbmin = (i-0.5)*FPOINTS_PER_SPOINT;
    int64_t fbmax = (i+0.5)*FPOINTS_PER_SPOINT;
    if (fbmin < 0) fbmin = 0;
    if (fbmax > fbins) fbmax = fbins;
    double ce = CALC_ERROR;
    last_error += ce*ce;
  }

  double cur_error = last_error;
  last_error *= 1.011;
  while (cur_error*1.0001 < last_error) {
    last_error = cur_error;
    memcpy(old_ya, ya, sizeof(double)*npoints);
    for (i=0; i<npoints; i++) {
      double min = smin[i];
      double max = smax[i];
      int64_t fbmin = (i-0.5)*FPOINTS_PER_SPOINT;
      int64_t fbmax = (i+0.5)*FPOINTS_PER_SPOINT;
      if (fbmin < 0) fbmin = 0;
      if (fbmax > fbins) { fbmax = fbins; fbmin -= FPOINTS_PER_SPOINT; }
      gsl_interp_init(spline, xa, ya, npoints);
      double ce = CALC_ERROR;
      double ce2 = ce;
      while (max > min+0.00001*fabs(min) && (min!=0 || max > 1e-9)) {
	ya[i] = 0.5*(max+min);
	gsl_interp_init(spline, xa, ya, npoints);
	ce2 = CALC_ERROR;
	if (ce2 > 0) max = ya[i];
	else min = ya[i];
      }
      if (fabs(ce) < fabs(ce2)) { ya[i] = old_ya[i]; }
    }

    gsl_interp_init(spline, xa, ya, npoints);
    cur_error = 0;
    for (i=0; i<npoints; i++) {
      int64_t fbmin = (i-0.5)*FPOINTS_PER_SPOINT;
      int64_t fbmax = (i+0.5)*FPOINTS_PER_SPOINT;
      if (fbmin < 0) fbmin = 0;
      if (fbmax > fbins) { fbmax = fbins; fbmin -= FPOINTS_PER_SPOINT; }
      double ce = CALC_ERROR;
      cur_error += ce*ce;
    }
    if (cur_error > last_error) {
      fprintf(stderr, "Warning: %f > %f for %.03f - %.03f\n", cur_error, last_error, low, high); 
      memcpy(ya, old_ya, sizeof(double)*npoints);
      cur_error = last_error;
    }
  }
#undef CALC_ERROR

  gsl_interp_init(spline, xa, ya, npoints);
  gsl_interp_accel_reset(acc);
  //Final sort by spline
  struct p_vmp tmp;
  for (i=0; i<np_low; i++) {
    double pthresh = gsl_interp_eval(spline, xa, ya, p[i].lvmp, acc);
    if (p[i].p > pthresh) {
      tmp = p[np_low-1];
      p[np_low-1] = p[i];
      p[i] = tmp;
      np_low--;
      i--;
    }
  }

  //Print out spline points
  fprintf(out, "%.12f %"PRId64, perc_to_normal(0.5*(high+low)), npoints);
  for (i=0; i<npoints; i++) fprintf(out, " %.12e %.12e", xa[i], ya[i]);
  fprintf(out, "\n");

  //Calculate spline errors
  alloc_clear_set(slowc, int64_t, npoints);
  alloc_clear_set(stotc, int64_t, npoints);
  alloc_clear_set(sloww, double, npoints);
  alloc_clear_set(stotw, double, npoints);
  for (i=0; i<np; i++) {
    int64_t sbin = (p[i].lvmp - min_lv)*bpdex + 0.5;
    assert(sbin < npoints);
    double w = pow(10,3.0*p[i].lvmp);
    stotc[sbin]++;
    stotw[sbin]+=w;
    if (i < np_low) { slowc[sbin]++; sloww[sbin]+=w; }
  }
  fprintf(out, "#Spline quality check: terr(%f) ", cur_error);
  for (i=0; i<npoints; i++) {
    double tc = (stotc[i]>0) ? stotc[i] : 1.0;
    double tw = (stotw[i]>0) ? stotw[i] : 1.0;
    fprintf(out, " %.3f=%"PRId64"/%"PRId64" (%.3f=wsum)", (double)slowc[i]/tc, 
	    slowc[i], stotc[i], sloww[i]/tw);
  }
  fprintf(out, "\n\n");
  free(slowc); free(stotc); free(sloww); free(stotw);

  /*if (high==1.0 && low == 0.0) {
    FILE *out2 = check_fopen("splines/data.test", "w");
    for (i=0; i<np; i++) {
      if (p[i].lvmp > 2.3)
	fprintf(out2, "%f %e %d\n", p[i].lvmp, p[i].p, (i<np_low) ? 0 : 1);
    }
    fclose(out2);
    }*/

  //Free data
 out:
  free(xa); free(ya); free(old_ya); gsl_interp_free(spline); gsl_interp_accel_free(acc); 
  free(counts); free(perc); free(min_p); free(max_p); free(vmp3); free(smax); free(smin); free(lvmp);
  if (error) return;
  if (np_low == 0 || np_low==np) return;

  //Recurse
  fit_splines(out, low, 0.5*(low + high), p, np_low);
  fit_splines(out, 0.5*(low+high), high, p+np_low, np-np_low);
#undef alloc_clear_set
}

struct spline {
  double scale;
  double value;
  int64_t num_points;
  gsl_interp *spline;
  gsl_interp_accel *accel;
  double *x, *y;
};

struct spline *splines = NULL;
int64_t num_splines = 0;
//double scaling = 0;

int sort_splines(const void *a, const void *b) {
  const struct spline *c = a;
  const struct spline *d = b;
  if (c->value < d->value) return -1;
  if (c->value > d->value) return 1;
  return 0;
}

double rand_rank(double value) {
  double neg_val = copysign(value, -1.0);
  double cumulative = 0.5*(1.0+erf(neg_val * M_SQRT1_2));
  double ra_perc = dr250()*cumulative;
  double ra_sd = perc_to_normal(ra_perc);
  return copysign(ra_sd, value);
}

double spline_rank(double lvmp, double val) {
  struct spline *l = splines;
  int64_t ns = num_splines;
  struct spline *h = l + ns - 1;
  while (l < h && ((l->x[0]>lvmp) || (l->x[l->num_points-1] < lvmp))) l++;
  while (h > l && ((h->x[0]>lvmp) || (h->x[h->num_points-1] < lvmp))) h--;
  if (h==l) return h->value;

#define EVAL(sp) (gsl_interp_eval(sp->spline, sp->x, sp->y, lvmp, sp->accel))
  if (EVAL(l) > val) return rand_rank(l->value);
  if (EVAL(h) < val) return rand_rank(h->value);
  while ((l+1)<h) {
    struct spline *m = l + (h-l)/2;
    double res = EVAL(m);
    if (res > val) h=m;
    else if (res < val) l=m;
    else return m->value;
  }
  double val_h = EVAL(h);
  double val_l = EVAL(l);
  if (val_h == val_l) return (0.5*(l->value+h->value));
  assert((val_l <= val) && (val_h >= val));
  double f = (val - val_l) / (val_h - val_l);  
  return (l->value + f*(h->value-l->value));
#undef EVAL
}

void clear_ranks(void) {
  int64_t i;
  for (i=0; i<num_splines; i++) {
    gsl_interp_free(splines[i].spline);
    gsl_interp_accel_free(splines[i].accel);
    free(splines[i].x);
    free(splines[i].y);
  }
  free(splines);
  num_splines = 0;
  splines = NULL;
}

void load_ranks(char *filename) {
  int64_t j;
  char buffer[1024];
  struct spline s = {0};
  SHORT_PARSETYPE;
  void **data = NULL;
  enum parsetype *pt = NULL;
  FILE *in = check_fopen(filename, "r");
  while (fgets(buffer, 1024, in)) {
    if (buffer[0] == '\n' || buffer[0] == ' ') continue;
    if (buffer[0] == '#') {
      //if (!strncmp(buffer, "#scaling =", 10))
      //sscanf(buffer+11, "%lf", &scaling);
      continue;
    }
    if (sscanf(buffer, "%lf %"SCNd64, &s.value, &s.num_points)!=2) continue;
    s.x = check_realloc(0, sizeof(double)*s.num_points, "Allocating x");
    s.y = check_realloc(0, sizeof(double)*s.num_points, "Allocating y");
    //s.scale = atof(scale);
    check_realloc_s(data, sizeof(void *), s.num_points*2+2);
    check_realloc_s(pt, sizeof(enum parsetype), s.num_points*2+2);
    data[0] = &s.value;
    pt[0] = F64;
    data[1] = &s.num_points;
    pt[1] = D64;
    for (j=0; j<s.num_points; j++) {
      data[j*2+2] = s.x+j;
      data[j*2+3] = s.y+j;
      pt[j*2+2] = pt[j*2+3] = F64;
    }
    int64_t n = stringparse(buffer, data, pt, s.num_points*2+2);
    if (n!=(s.num_points*2+2)) continue;
    const gsl_interp_type *T = (s.num_points > 4) ? gsl_interp_akima : gsl_interp_cspline;
    s.spline = gsl_interp_alloc(T, s.num_points);
    s.accel = gsl_interp_accel_alloc();
    gsl_interp_init(s.spline, s.x, s.y, s.num_points);
    check_realloc_every(splines, sizeof(struct spline), num_splines, 1000);
    splines[num_splines] = s;
    num_splines++;
  }
  fclose(in);
  qsort(splines, num_splines, sizeof(struct spline), sort_splines);
}


int main(int argc, char **argv)
{
  int64_t i;
  char buffer[1024];

  if (argc < 2) {
    fprintf(stderr, "Usage: %s ranks_scale.list\n", argv[0]);
    exit(1);
  }

  r250_init(87L);
  FILE *in = check_fopen(argv[1], "r");
  struct p_vmp p = {0};
  double ignore;
  while (fgets(buffer, 1024, in)) {
    if (buffer[0] == '#') continue;
    int64_t n = sscanf(buffer, "%lf %lf %lf", &p.lvmp, &ignore, &p.p);
    if (n<3) continue;
    check_realloc_every(p_vmps, sizeof(struct p_vmp), num_halos, 1000);
    p_vmps[num_halos] = p;
    num_halos++;
  }
  fclose(in);

  FILE *out = check_fopen("test.splines", "w");
  fit_splines(out, 0, 1.0, p_vmps, num_halos);
  fclose(out);
    
  load_ranks("test.splines");
  out = check_fopen("test.ranks", "w");
  fprintf(out, "#LVMP (scaled) Rank\n");
  for (i=0; i<num_halos; i++) {
    double lvmp = p_vmps[i].lvmp;
    double val = p_vmps[i].p;
    fprintf(out, "%f %e %.6f\n", lvmp, val, spline_rank(lvmp, val));
  }
  fclose(out);
  return 0;
}


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <math.h>
#include <stdint.h>
#include <sys/mman.h>
#include "universal_constants.h"
#include "inet/rsocket.h"
#include "check_syscalls.h"
#include "corr_lib.h"
#include "comm_lib.h"
#include "stringparse.h"
#include "make_sf_catalog.h"
#include "config_vars.h"
#include "mt_rand.h"
#include "perftimer.h"
#include "integrate.h"

#define FAST3TREE_TYPE struct catalog_galaxy
#define FAST3TREE_PREFIX CORR
#define FAST3TREE_DIM 2
#include "fast3tree.c"


int64_t max_galaxies = 0;
struct catalog_galaxy_weights *cgw = NULL;
struct catalog_galaxy *weights_base = NULL;

struct fast3tree *corr_tree = NULL;
struct fast3tree_results *res = NULL;

double wcorr[NUM_CORR_BINS_PER_STEP];
double wxcorr[NUM_XCORR_BINS_PER_STEP];
double ca[NUM_CORR_THRESHES*NUM_CORR_TYPES]={0};
int64_t corr[NUM_BINS], corr_q[NUM_BINS], corr_sf[NUM_BINS], ccorr_sf_q[NUM_BINS];

int64_t conf_counts[NUM_CONF_BINS];
double dens_counts[NUM_DENS_BINS], dens_sf[NUM_DENS_BINS];
double dens_sf_ssfr[NUM_DENS_BINS];
double conf_sfr1[NUM_CONF_BINS],conf_sfr2[NUM_CONF_BINS],conf_sfr11[NUM_CONF_BINS],conf_sfr22[NUM_CONF_BINS],
  conf_sfr12[NUM_CONF_BINS];


float calc_z_weight(float dz, float pi_max, float inv_z_error2) {
  return(cached_rank((pi_max-dz)*inv_z_error2)-cached_rank((-pi_max-dz)*inv_z_error2));
}

void write_corr_galaxies(int64_t chunk_num, int64_t step, int64_t num_to_process) {
  int64_t i;
  char buffer[1024];
  snprintf(buffer, 1024, "%s/corr_galaxies_step%"PRId64"_box%"PRId64".dat", OUTBASE, step, chunk_num);
  FILE *out = check_fopen(buffer, "w");
  fprintf(out, "#%"PRId64" External galaxies\n", num_to_process - (galaxy_offsets[step+1]-galaxy_offsets[step]));
  fprintf(out, "#X Y Z SM SFR External?\n");
  for (i=0; i<num_to_process; i++) {
    struct catalog_galaxy *cg = galaxies+offsets[num_scales]-num_to_process-1+i;
    int external = (i < (num_to_process - (galaxy_offsets[step+1]-galaxy_offsets[step]))) ? 1 : 0;
    fprintf(out, "%f %f %f %f %f %f %d\n", cg->pos[0], cg->pos[1], cg->pos[2], cg->lsm, cg->lsfr, cg->weight, external);
  }
  fclose(out);
}

void corr_client(char *address, char *port, int64_t chunk_num) {
  char buffer[100];
  sprintf(buffer, "Corr Client.%"PRId64".%"PRId64, box_id, chunk_num);
  set_rsocket_role(buffer);
  set_timer_role("Corr Client", box_id, chunk_num);
  add_timers("Snap", num_scales);
  timer_init(wait_timer, "Waiting");
  int64_t comm = connect_to_addr(address, port);
  struct galaxy_comm gc = {0};
  timer_start;
  while (1) {
    recv_from_socket(comm, &gc, sizeof(struct galaxy_comm));
    timer_stop(wait_timer);
    timer_start;
    if (gc.step < 0) break;
    //write_corr_galaxies(chunk_num, gc.step, gc.num_to_xfer);
    //fprintf(stderr, "%f: %"PRId64" galaxies\n", scales[gc.step], gc.num_to_xfer);
    struct sf_model c = calc_sf_model(gc.m, scales[gc.step]);
    calc_corrs(gc.step, gc.num_to_xfer, c);
    //calc_conformity(gc.step, gc.num_to_xfer); //No longer supported!
    calc_density(gc.step, gc.num_to_xfer, c);
    if (POSTPROCESSING_MODE)
      calc_corrs_postprocess(gc.step, gc.num_to_xfer, c);
    timer_stop(gc.step);
    timer_start;
    send_to_socket_noconfirm(comm, &gc.step, sizeof(int64_t));
  }
  exit(EXIT_SUCCESS);
}


void _add_to_corr_bins(struct catalog_galaxy *g1, struct catalog_galaxy_weights *g1w,
		       struct catalog_galaxy *g2, struct catalog_galaxy_weights *g2w,
		       int64_t b, float pi_max, float inv_z_err2) { //Note that z_err should be the combined z errors for g1 and g2
  //b is assumed to be >-1 and <NUM_BINS.
  float max_weight = g2w->max_weight*g1w->max_weight;
  if (max_weight < 1e-9) return;

  int64_t i;
  float dx = fabs(g2->pos[2]-g1->pos[2]);
  if (dx*2.0 > BOX_SIZE) dx = BOX_SIZE-dx;
  
  float z_weight = calc_z_weight(dx, pi_max, inv_z_err2);
  if (z_weight*max_weight < 1e-9) return;
  //float w[NUM_CORR_THRESHES*NUM_CORR_TYPES]={0};

  for (i=0; i<NUM_CORR_THRESHES; i++) {
    wcorr[(i*NUM_CORR_TYPES)*NUM_BINS + b] += z_weight*(g1w->sf[i]+g1w->q[i])*(g2w->sf[i]+g2w->q[i]); //All x all
    wcorr[(i*NUM_CORR_TYPES+1)*NUM_BINS+b] += z_weight*g1w->sf[i]*g2w->sf[i]; //SF x SF
    wcorr[(i*NUM_CORR_TYPES+2)*NUM_BINS+b] += z_weight*g1w->q[i]*g2w->q[i]; //Q x Q
    wcorr[(i*NUM_CORR_TYPES+3)*NUM_BINS+b] += z_weight*(g1w->q[i]*g2w->sf[i]); //X-correlation Q x SF (nb. SF x Q = All x All - SFxSF - QxQ - QxSF)
  }

  for (i=0; i<NUM_XCORR_THRESHES; i++) {
    int64_t i1 = XCORR_BIN1[i];
    int64_t i2 = XCORR_BIN2[i];
    wxcorr[(i*NUM_CORR_TYPES)*NUM_BINS + b] += z_weight*(g1w->sf[i1]+g1w->q[i1])*(g2w->sf[i2]+g2w->q[i2]); //All x all
    wxcorr[(i*NUM_CORR_TYPES+1)*NUM_BINS+b] += z_weight*(g1w->sf[i1]+g1w->q[i1])*g2w->q[i2]; //All x Q (nb. All x SF = All x all - All x Q)
    wxcorr[(i*NUM_CORR_TYPES+2)*NUM_BINS+b] += z_weight*g1w->q[i1]*g2w->q[i2]; //Q x Q
    wxcorr[(i*NUM_CORR_TYPES+3)*NUM_BINS+b] += z_weight*g1w->sf[i1]*g2w->sf[i2]; //SF x SF
  }
}



void _bin_galaxy(struct catalog_galaxy *g, struct catalog_galaxy_weights *gw, struct tree3_node *n, float pi_max, float inv_z_err2) {
  int64_t i, j;
  float *c = g->pos;
  //Check on max and min distances to box corners
  float db=0, ds=0, e1, e2;
  for (i=0; i<FAST3TREE_DIM; i++) {
    e1 = n->min[i]-c[i];
    e2 = c[i]-n->max[i];
    if (fabs(e1)>fabs(e2)) {
      db+=e1*e1;
    } else {
      db+=e2*e2;
    }
    if (e1 > 0) ds+=e1*e1;
    else if (e2 > 0) ds+=e2*e2;
    if (ds >= max_dist) {
      //printf("%f %f %f %f %f %f %f\n", g->pos[0], g->pos[1], n->min[0], n->max[0], n->min[1], n->max[1], ds);
      return;
    }
  }

  if (ds>0) {
    int64_t b1=corr_bin_of(db);
    int64_t b2=corr_bin_of(ds);
    if (b1==b2) {
      if (b1>-1 && b1 < NUM_BINS) {
	for (int64_t i=0; i<n->num_points; i++) {
	  struct catalog_galaxy_weights *g2w = cgw+(n->points+i-weights_base);
	  _add_to_corr_bins(g, gw, n->points+i, g2w, b1, pi_max, inv_z_err2);
	}
      }
      return;
    }
  }
  if (db < min_dist) return;

  if (n->div_dim < 0) {
    for (i=0; i<n->num_points; i++) {
      float dx = 0;
      for (j=0; j<2; j++) {
	float ds = g->pos[j] - n->points[i].pos[j];
	dx += ds*ds;
      }
      int64_t b = corr_bin_of(dx);
      if (b<0 || b>=NUM_BINS) continue;
      struct catalog_galaxy_weights *g2w = cgw+(n->points+i-weights_base);
      _add_to_corr_bins(g, gw, n->points+i, g2w, b, pi_max, inv_z_err2);
    }
  }

  else {
    _bin_galaxy(g, gw, n->left, pi_max, inv_z_err2);
    _bin_galaxy(g, gw, n->right, pi_max, inv_z_err2);
  }
}


void _old_bin_galaxy(struct catalog_galaxy *g, struct tree3_node *n, double pi_max) {
  int64_t i, j;
  float *c = g->pos;
  //Check on max and min distances to box corners
  float db=0, ds=0, e1, e2;
  for (i=0; i<FAST3TREE_DIM; i++) {
    e1 = n->min[i]-c[i];
    e2 = c[i]-n->max[i];
    if (fabs(e1)>fabs(e2)) {
      db+=e1*e1;
    } else {
      db+=e2*e2;
    }
    if (e1 > 0) ds+=e1*e1;
    else if (e2 > 0) ds+=e2*e2;
    if (ds >= max_dist)
      return;
  }

  if (ds>0) {
    int64_t b1=corr_bin_of(db);
    int64_t b2=corr_bin_of(ds);
    if (b1==b2) {
      if (b1>-1 && b1 < NUM_BINS) {
	int64_t i;
	int64_t q = (g->lsfr > 0) ? 0 : 1;
	float box_size2 = BOX_SIZE/2.0;
	for (i=0; i<n->num_points; i++) {
	  float dx = fabs(g->pos[2]-n->points[i].pos[2]);
	  if (dx > box_size2) dx = BOX_SIZE-dx;
	  if (dx > pi_max) continue;
	  corr[b1]++;
	  if (!q && (n->points[i].lsfr>0)) corr_sf[b1]++;
	  else if (q && (n->points[i].lsfr<=0)) corr_q[b1]++;
	  else ccorr_sf_q[b1]++;
	}
      }
      return;
    }
  }
  if (db < min_dist) return;

  if (n->div_dim < 0) {
    int64_t q = (g->lsfr > 0) ? 0 : 1;
    float box_size2 = BOX_SIZE/2.0;
    for (i=0; i<n->num_points; i++) {
      float dx = fabs(g->pos[2]-n->points[i].pos[2]);
      if (dx > box_size2) dx = BOX_SIZE-dx;
      if (dx > pi_max) continue;
      dx = 0;
      for (j=0; j<2; j++) {
	float ds = g->pos[j] - n->points[i].pos[j];
	dx += ds*ds;
      }
      int64_t b = corr_bin_of(dx);
      if (b<0 || b>=NUM_BINS) continue; 
      corr[b]++;
      if (!q && (n->points[i].lsfr>0)) corr_sf[b]++;
      else if (q && (n->points[i].lsfr<=0)) corr_q[b]++;
      else ccorr_sf_q[b]++;
    }
  }

  else {
    _old_bin_galaxy(g, n->left, pi_max);
    _old_bin_galaxy(g, n->right, pi_max);
  }
}



void init_corr_tree(struct catalog_galaxy *g2, int64_t num_g2) {
  if (!corr_tree) corr_tree = fast3tree_init(num_g2, g2);
  else fast3tree_rebuild(corr_tree, num_g2, g2);
}

void clear_corrs(void) {
  memset(wcorr, 0, sizeof(double)*NUM_CORR_BINS_PER_STEP);
  memset(wxcorr, 0, sizeof(double)*NUM_XCORR_BINS_PER_STEP);
  memset(ca, 0, sizeof(double)*NUM_CORR_THRESHES*NUM_CORR_TYPES);
  memset(corr, 0, sizeof(int64_t)*NUM_BINS);
  memset(corr_sf, 0, sizeof(int64_t)*NUM_BINS);
  memset(corr_q, 0, sizeof(int64_t)*NUM_BINS);
  memset(ccorr_sf_q, 0, sizeof(int64_t)*NUM_BINS);
}

void bin_single_galaxy(struct catalog_galaxy *g) {
  //cc++;
  _old_bin_galaxy(g, corr_tree->root, CORR_LENGTH_PI);
}

double corr_min_sm(float scale) {
  double min_sm;
  if (scale<CORR_HIGHZ_SCALE)
    min_sm = CORR_MIN_SM_HIGHZ;
  else
    min_sm = CORR_MIN_SM2;

  if (POSTPROCESSING_MODE) {
    min_sm = CORR_MIN_SM;
    if (min_sm > POSTPROCESS_CORR_MIN_SM) {
      min_sm = POSTPROCESS_CORR_MIN_SM;
    }
  }
  return log10(min_sm);
}

void partition_galaxies(struct catalog_galaxy *g, int64_t num_g, float thresh, int64_t *left, int64_t *right) {
  int64_t i;
  struct catalog_galaxy tmp;
  *left = num_g;
  for (i=0; i<*left; i++) {
    if (g[i].lsm > thresh) {
      *left = *left - 1;
      tmp = g[*left];
      g[*left] = g[i];
      g[i] = tmp;
      i--;
    }
  }
  *right = num_g - *left;
}


void bin_galaxies(float scale, struct catalog_galaxy *g, int64_t num_g, struct catalog_galaxy *g2, int64_t num_g2, int64_t step, struct sf_model c,
		  float thresholds[NUM_CORR_THRESHES], int64_t post) {
  int64_t i, j;

  //Ignore galaxies more than 3.5 sigma below the minimum mass threshold for observations
  float min_sm = corr_min_sm(scale); //Log10 units
  min_sm -= 3.5*c.obs_sm_sig;
  int64_t smaller=0, larger=0;
  partition_galaxies(g2, num_g2, min_sm, &smaller, &larger);
  g2 += smaller;
  num_g2 = larger;

  init_corr_tree(g2, num_g2);
  clear_corrs();

  double pi_max = CORR_LENGTH_PI;
  if (scale < CORR_HIGHZ_SCALE) pi_max = CORR_LENGTH_PI_HIGHZ;
  if (post) pi_max = POSTPROCESS_CORR_LENGTH_PI;
  struct catalog_galaxy_weights w = {{0}};
  float sm_scatter_const = (c.obs_sm_sig > 0) ? 1.0/c.obs_sm_sig : 1e3;
  float sfr_scatter_const = (c.obs_sfr_sig > 0.05) ? 1.0/c.obs_sfr_sig : 20.0;
  float translate_const = moustakas_translate_const(scale);

  float z_error = REDSHIFT_ERROR;
  if (post || scale <= CORR_HIGHZ_SCALE) z_error = REDSHIFT_ERROR_HIGHZ;
  if (z_error < 1) z_error *= SPEED_OF_LIGHT/scale; //note that 1/a = (1+z)
  double hz = 100.0*sqrt(Om*pow(scale, -3.0)+Ol)*scale;
  z_error /= hz; //Convert to comoving Mpc/h
  float inv_z_error2 = 1.0/(sqrt(2)*z_error); // Combined z errors for source and target galaxy
  
  calc_galaxy_weights(sm_scatter_const, sfr_scatter_const, translate_const, g2, num_g2, thresholds, post);
  
  for (i=0; i<num_g; i++) {
    if (g[i].lsm < min_sm) continue;
    _calc_galaxy_weight(sm_scatter_const, sfr_scatter_const, translate_const, thresholds, g+i, &w, post);
    _bin_galaxy(g+i, &w, corr_tree->root, pi_max, inv_z_error2);
    for (j=0; j<NUM_CORR_THRESHES; j++) {
      ca[j*NUM_CORR_TYPES]    += w.sf[j]+w.q[j];         //All
      ca[j*NUM_CORR_TYPES+1]  += w.sf[j];                //SF
      ca[j*NUM_CORR_TYPES+2]  += w.q[j];                 //Q
      ca[j*NUM_CORR_TYPES+3]  = ca[j*NUM_CORR_TYPES+2];  //Q  (for QxSF)
    }
  }

  if (step > -1) {
    float *corr_loc = fcorrs + (step*NUM_CORR_BINS_PER_STEP);
    if (post) corr_loc = fcorrs_post + (corr_loc - fcorrs);
    for (i=0; i<NUM_CORR_BINS_PER_STEP; i++) corr_loc[i] = wcorr[i];

    float *xcorr_loc = fxcorrs + (step*NUM_XCORR_BINS_PER_STEP);
    if (post) xcorr_loc = fxcorrs_post + (xcorr_loc - fxcorrs);
    for (i=0; i<NUM_XCORR_BINS_PER_STEP; i++) xcorr_loc[i] = wxcorr[i];

    float *count_loc = fccounts + step*(NUM_CORR_TYPES*NUM_CORR_THRESHES);
    if (post) count_loc = fcounts_post + (count_loc - fccounts);
    for (i=0; i<(NUM_CORR_TYPES*NUM_CORR_THRESHES); i++) count_loc[i] = ca[i];
  }
}


void translate_sfrs(struct catalog_galaxy *g, float c) {
  g->lsfr -= c + 0.65*(g->lsm - 10.0);
}

double original_ssfr(struct catalog_galaxy *g, float c) {
  return (g->lsfr + c + 0.65*(g->lsm - 10.0) - g->lsm);
}


void _calc_galaxy_weight(float sm_scatter_const, float sfr_scatter_const, float c, float thresholds[NUM_CORR_THRESHES], struct catalog_galaxy *g, struct catalog_galaxy_weights *gw, int64_t post) {
  int64_t j;

  //Currently, SF frac does not change with stellar mass...
  float quenched_sfr = (post) ? (g->lsm + log_quenched_ssfr) : (c + 0.65*(g->lsm - 10.0));
  float thresh_sfr = g->lsfr - quenched_sfr; //SFR - Moustakas_thresh
  float q_weight = cached_rank(-thresh_sfr * sfr_scatter_const);
  float sf_weight = 1.0-q_weight;
  gw->max_weight = 0;
    
  for (j=0; j<NUM_CORR_THRESHES; j++) {
    float sm_weight = cached_rank((g->lsm-thresholds[j])*sm_scatter_const);
    gw->sf[j] = g->weight*sm_weight*sf_weight;
    gw->q[j] =  g->weight*sm_weight*q_weight;
  }

  //Make bins exclusive
  for (j=0; j<NUM_CORR_THRESHES-1; j++) {
    gw->sf[j] -= gw->sf[j+1];
    gw->q[j] -= gw->q[j+1];
  }

  for (j=0; j<NUM_CORR_THRESHES; j++)
    if (gw->sf[j]+gw->q[j]>gw->max_weight) gw->max_weight = gw->sf[j]+gw->q[j];
}

void calc_galaxy_weights(float sm_scatter_const, double sfr_scatter_const, float c, struct catalog_galaxy *g2, int64_t num_to_process, float thresholds[NUM_CORR_THRESHES], int64_t post) {
  int64_t i;
  if (num_to_process > max_galaxies) {
    max_galaxies = num_to_process;
    check_realloc_s(cgw, sizeof(struct catalog_galaxy_weights), max_galaxies);
  }

  //struct catalog_galaxy *g2 = galaxies + offsets[num_scales]-num_to_process-1;
  weights_base = g2;
  for (i=0; i<num_to_process; i++)
    _calc_galaxy_weight(sm_scatter_const, sfr_scatter_const, c, thresholds, g2+i, cgw+i, post);
}


void calc_corrs(int64_t step, int64_t num_to_process, struct sf_model c) {
  struct catalog_galaxy *g1 = galaxies + galaxy_offsets[step];
  struct catalog_galaxy *g2 = galaxies + offsets[num_scales]-num_to_process-1;

  init_dists();

  float min_threshes[NUM_CORR_THRESHES];
  assert(NUM_CORR_THRESHES==4);
  min_threshes[0] = log10(CORR_MIN_SM);
  min_threshes[1] = log10(CORR_MIN_SM2);
  min_threshes[2] = log10(CORR_MIN_SM3);
  min_threshes[3] = log10(CORR_MIN_SM4);

  if (scales[step] < CORR_HIGHZ_SCALE) {
    min_threshes[2] = log10(CORR_MIN_SM_HIGHZ);
    min_threshes[3] = log10(CORR_MAX_SM_HIGHZ);
  }

  bin_galaxies(scales[step], g1, galaxy_offsets[step+1]-galaxy_offsets[step], g2, num_to_process, step, c, min_threshes, 0);
}

void calc_corrs_postprocess(int64_t step, int64_t num_to_process, struct sf_model c) {
  struct catalog_galaxy *g1 = galaxies + galaxy_offsets[step];
  struct catalog_galaxy *g2 = galaxies + offsets[num_scales]-num_to_process-1;

  float min_threshes[4];
  min_threshes[0] = 0;
  min_threshes[1] = log10(POSTPROCESS_CORR_MIN_SM);
  min_threshes[2] = log10(POSTPROCESS_CORR_MIN_SM2);
  min_threshes[3] = log10(POSTPROCESS_CORR_MIN_SM3);

  bin_galaxies(scales[step], g1, galaxy_offsets[step+1]-galaxy_offsets[step], g2, num_to_process, step, c, min_threshes, 1);
}

void _calc_conformity(double scale, struct catalog_galaxy *g1, int64_t n1, struct catalog_galaxy *g2, int64_t n2) {
  int64_t i,j;
  if (!corr_tree) corr_tree = fast3tree_init(n2, g2);
  else fast3tree_rebuild(corr_tree, n2, g2);
  if (!res) res = fast3tree_results_init();

  float min = log10(CONFORMITY_MIN_SM);
  float max = log10(CONFORMITY_MAX_SM);

  float hz = 100.0*sqrt(Om*pow(scale, -3.0)+Ol)*scale;
  float z_dist_neighbor = CONFORMITY_NEIGHBOR_DV / hz;
  float z_dist_exclude  = CONFORMITY_EXCLUSION_DV / hz;
  float xy_dist_exclude = CONFORMITY_EXCLUSION_DX*CONFORMITY_EXCLUSION_DX;

  memset(conf_counts, 0, sizeof(int64_t)*NUM_CONF_BINS);
  memset(conf_sfr1,   0, sizeof(double)*NUM_CONF_BINS);
  memset(conf_sfr2,   0, sizeof(double)*NUM_CONF_BINS);
  memset(conf_sfr11,  0, sizeof(double)*NUM_CONF_BINS);
  memset(conf_sfr22,  0, sizeof(double)*NUM_CONF_BINS);
  memset(conf_sfr12,  0, sizeof(double)*NUM_CONF_BINS);

  init_conf_dists();

  for (i=0; i<n1; i++) {
    struct catalog_galaxy *g = g1+i;
    if (g->lsm < min || g->lsm > max) continue;
    fast3tree_find_sphere(corr_tree, res, g->pos, CONFORMITY_MAX_DX*h0);
    float min_dist = -1;
    struct catalog_galaxy *closest = NULL;
    float min_neighbor = g->lsm - CONFORMITY_NEIGHBOR_DSM;
    for (j=0; j<res->num_points; j++) {
      if (res->points[j]->lsm < min_neighbor) continue;
      float dz = fabs(res->points[j]->pos[2] - g->pos[2]);
      if (dz > BOX_SIZE/2.0) dz = BOX_SIZE - dz;
      if (dz > z_dist_exclude) continue;

      float dx = res->points[j]->pos[0] - g->pos[0];
      float dy = res->points[j]->pos[1] - g->pos[1];
      if (!dx && !dy) continue;
      float dist = dx*dx+dy*dy;
      if ((res->points[j]->lsm > g->lsm) &&
	  dist < xy_dist_exclude) break;

      if (dz > z_dist_neighbor) continue;
      if (min_dist < 0 || dist < min_dist) {
	min_dist = dist;
	closest = res->points[j];
      }
    }
    if (j<res->num_points || min_dist < 0) continue;
    int64_t b = conf_bin_of(min_dist);
    if (b<0 || b>=NUM_CONF_BINS) continue;
    conf_counts[b]++;
    conf_sfr1[b]  += g->lsfr;
    conf_sfr2[b]  += closest->lsfr;
    conf_sfr11[b] += g->lsfr*g->lsfr;
    conf_sfr22[b] += closest->lsfr*closest->lsfr;
    conf_sfr12[b] += g->lsfr*closest->lsfr;
  }
}


//Assumes SFRs are already translated
void calc_conformity(int64_t step, int64_t num_to_process) {
  int64_t i, n1=galaxy_offsets[step+1]-galaxy_offsets[step];
  int64_t n2=num_to_process, l;
  struct catalog_galaxy *g1 = galaxies + galaxy_offsets[step];
  struct catalog_galaxy *g2 = galaxies + offsets[num_scales]-num_to_process-1;
  float min = log10(CONFORMITY_MIN_SM);
  if (scales[step] < CONFORMITY_MIN_SCALE) return;
  partition_galaxies(g2, n2, min-CONFORMITY_NEIGHBOR_DSM, &l, &n2);
  g2 += l;

  _calc_conformity(scales[step], g1, n1, g2, n2);

  float *conf = fconfs + step*NUM_CONF_BINS_PER_STEP;
  for (i=0; i<NUM_CONF_BINS; i++) {
    conf[i]                 = conf_counts[i];
    conf[i+NUM_CONF_BINS]   = conf_sfr1[i];
    conf[i+NUM_CONF_BINS*2] = conf_sfr2[i];
    conf[i+NUM_CONF_BINS*3] = conf_sfr11[i];
    conf[i+NUM_CONF_BINS*4] = conf_sfr22[i];
    conf[i+NUM_CONF_BINS*5] = conf_sfr12[i];
  }
}

struct dw_helper {
  float sm1, sm2, dsm_min, dsm_max, sm_scatter_const;
};

double density_weight_integral(double x, void *extra_data) {
  struct dw_helper *d = extra_data;
  float dsm = (x-d->sm1)*d->sm_scatter_const;
  float psm = expf(-0.5*dsm*dsm);
  float psm2 = cached_rank((d->sm2-(x+d->dsm_min))*d->sm_scatter_const)-
    cached_rank((d->sm2-(x+d->dsm_max))*d->sm_scatter_const);
  return psm*psm2;
}

float integrate_density_weight(float sm1, float sm2, float sm_min, float sm_max, float dsm_min, float dsm_max, float sm_scatter_const, float sm_weight) {
  //Integrates P(SM2>(SM1+dsm_min)&SM2<(SM1+dsm_max)|SM1)P(SM1=x) for x=sm_min to x=sm_max
  struct dw_helper d={0};
  d.sm1 = sm1;
  d.sm2 = sm2;
  d.dsm_min = dsm_min;
  d.dsm_max = dsm_max;
  d.sm_scatter_const = sm_scatter_const;
  /*  float est_sm_w = (sm1 < sm_min) ? density_weight_integral(sm_min) :
      density_weight_integral(sm_max);*/
  float tol = 1e-4*sm_weight;
  if (!(tol>0)) tol=1e-4;
  float val=adaptiveSimpsons(density_weight_integral, &d,
			     sm_min, sm_max, tol, 10);
  float ret = val*sm_scatter_const*(0.5*M_2_SQRTPI*M_SQRT1_2)/sm_weight; //1/sqrt(2*pi*scatter^2)*integral
  if (ret>1 || !isfinite(ret)) ret=1;
  return ret; 
}


float approx_neighbor_integral(float sm1, float sm2, float sm_sig) {
  float p1[5]={1.024948e+01, 2.357396e-01, 1.843233e-01, 2.498690e-01, 7.187006e-02};   //sig=0.07
  float p2[5]={1.024807e+01, 2.222332e-01, 2.043431e-01, 2.502337e-01, 1.027049e-01};   //sig=0.1
  float params[5];
  for (int64_t i=0; i<5; i++)
    params[i] = p1[i]+(p2[i]-p1[i])*(sm_sig-0.07)/(0.1-0.07);
  float mid = params[0];
  float width = params[1];
  float trans = params[2];
  float sep = params[3];
  float width_min = params[4];
  float dsm = (sm1-mid)/width;
  float width2 = width_min*(1.0+(M_SQRT2-1.0)*expf(-dsm*dsm));

  float midp = (mid-width)+2.0*width*cached_rank((sm1-mid)/trans);
  return cached_rank((sm2-(midp-sep))/width2)-
    cached_rank((sm2-(midp+sep))/width2);
}

float approx_larger_integral(float sm1, float sm2, float sm_sig) {
  float p1[4]={1.024936e+01, 2.355509e-01, 1.837131e-01, 7.174562e-02}; //sig=0.07
  float p2[4]={1.024876e+01, 2.231106e-01, 2.054721e-01, 1.034060e-01}; //sig=0.1
  float params[4];
  for (int64_t i=0; i<4; i++)
    params[i] = p1[i]+(p2[i]-p1[i])*(sm_sig-0.07)/(0.1-0.07);
  float mid = params[0];
  float width = params[1];
  float trans = params[2];
  float width_min = params[3];
  float dsm = (sm1-mid)/width;
  float width2 = width_min*(1.0+(M_SQRT2-1.0)*expf(-dsm*dsm));

  float midp = (mid-width)+2.0*width*cached_rank((sm1-mid)/trans);
  return cached_rank((sm2-midp)/width2);
}



void _calc_density(double scale, struct catalog_galaxy *g1, int64_t n1, struct catalog_galaxy *g2, int64_t n2, struct sf_model c) {
  int64_t i,j,k;
  if (!corr_tree) corr_tree = fast3tree_init(n2, g2);
  else fast3tree_rebuild(corr_tree, n2, g2);
  if (!res) res = fast3tree_results_init();

  float sm_scatter_const = (c.obs_sm_sig > 0) ? 1.0/(c.obs_sm_sig) : 1e3;
  float sfr_scatter_const = (c.obs_sfr_sig > 0.05) ? 1.0/c.obs_sfr_sig : 20.0;
  float translate_const = moustakas_translate_const(scale);
  
  float min = log10(DENS_MIN_SM);
  float max = log10(DENS_MAX_SM);

  float hz = 100.0*sqrt(Om*pow(scale, -3.0)+Ol)*scale;
  float z_dist_exclude  = DENS_EXCLUSION_DV / hz;
  float xy_dist_exclude = DENS_EXCLUSION_DX*DENS_EXCLUSION_DX*h0*h0;
  float min_dx =  DENS_MIN_DX*DENS_MIN_DX*h0*h0;
  float max_dx =  DENS_MAX_DX*DENS_MAX_DX*h0*h0;

  float z_error = REDSHIFT_ERROR;
  if (z_error < 1) z_error *= SPEED_OF_LIGHT/scale; //note that 1/a = (1+z)
  z_error /= hz; //Convert to comoving Mpc/h
  float inv_z_error2 = 1.0/(sqrt(2)*z_error); // Combined z errors for source and target galaxy

  //int64_t integral_count = 0;
  
  memset(dens_counts, 0, sizeof(double)*NUM_DENS_BINS);
  memset(dens_sf, 0, sizeof(double)*NUM_DENS_BINS);
  memset(dens_sf_ssfr, 0, sizeof(double)*NUM_DENS_BINS);

  assert(DENS_NEIGHBOR_MIN_DSM < DENS_NEIGHBOR_MAX_DSM);
  assert(fabs(min-10)<0.01);
  assert(fabs(max-10.5)<0.01);
  assert(fabs(DENS_NEIGHBOR_MIN_DSM+0.25)<0.01);
  assert(fabs(DENS_NEIGHBOR_MAX_DSM-0.25)<0.01);

  float nn[MAX_BINS_NN]={0};
  for (i=0; i<n1; i++) {
    memset(nn, 0, sizeof(float)*MAX_BINS_NN);
    nn[0] = 1;
    int64_t min_k=0, max_k=1;
    struct catalog_galaxy *g = g1+i;
    float sm_weight = cached_rank((g->lsm-min)*sm_scatter_const)-cached_rank((g->lsm-max)*sm_scatter_const);
    float weight = sm_weight*g->weight;
    if (weight < 1e-9) continue;
    
    fast3tree_find_sphere(corr_tree, res, g->pos, DENS_MAX_DX*h0);
    //float min_neighbor = g->lsm + DENS_NEIGHBOR_MIN_DSM;
    //float max_neighbor = g->lsm + DENS_NEIGHBOR_MAX_DSM;
    for (j=0; j<res->num_points; j++) {
      float dz = fabs(res->points[j]->pos[2] - g->pos[2]);
      if (dz > BOX_SIZE/2.0) dz = BOX_SIZE - dz;
      float zweight = calc_z_weight(dz, z_dist_exclude, inv_z_error2);

      float nweight = 0;
      if (zweight > 1e-9) {
	nweight = /*integrate_density_weight(g->lsm, res->points[j]->lsm, min, max,
		  DENS_NEIGHBOR_MIN_DSM, DENS_NEIGHBOR_MAX_DSM, sm_scatter_const, sm_weight);*/
	  approx_neighbor_integral(g->lsm, res->points[j]->lsm, c.obs_sm_sig);
	nweight *= res->points[j]->weight*zweight;
      }
            
      float dx = res->points[j]->pos[0] - g->pos[0];
      float dy = res->points[j]->pos[1] - g->pos[1];
      if (!dx && !dy) continue;
      float dist = dx*dx+dy*dy;

      //If the galaxy is larger:
      if (dist < xy_dist_exclude) {
	float prob_larger = 
	  /*integrate_density_weight(g->lsm, res->points[j]->lsm, min, max,
	    0, 5, sm_scatter_const, sm_weight); */
	  approx_larger_integral(g->lsm, res->points[j]->lsm, c.obs_sm_sig);
	weight *= 1.0-prob_larger*zweight;
      }

      if (dist > min_dx && dist < max_dx && nweight > 1e-9) {
	k=max_k;
	float nw = nweight, nwm1 = 1.0-nweight;
	if (k==MAX_BINS_NN) k--;
	nn[k] += nw*nn[k-1];
	k--;
	for (; k>min_k; k--) nn[k] = nwm1*nn[k] + nw*nn[k-1];
	nn[min_k]*=nwm1;
	if (max_k < MAX_BINS_NN) max_k++;
	if (nn[min_k] < 1e-9 && min_k<MAX_BINS_NN-2) {
	  min_k++;
	  nn[min_k]+=nn[min_k-1];
	  nn[min_k-1]=0;
	}
      }
    }
    
    float thresh_sfr = g->lsfr - (translate_const + 0.65*(g->lsm - 10.0)); //SFR - Moustakas_thresh
    float sf_weight = cached_rank(thresh_sfr * sfr_scatter_const);
    //if (nn[5]>0.1) fprintf(stderr, "%f %f %e %e %e\n", g->lsm, g->lsfr, nn[5], weight, sf_weight);
    for (k=0; k<MAX_BINS_NN; k++) {
      int64_t b = dens_bin_of(k);
      if (b<0 || b>=NUM_DENS_BINS) continue;
      dens_counts[b]+=weight*nn[k];
      dens_sf[b]+=sf_weight*weight*nn[k];
      dens_sf_ssfr[b] += sf_weight*weight*nn[k]*(g->lsfr - g->lsm);
    }
  }
  //fprintf(stderr, "%f: %"PRId64" integrals (sig=%f)\n", scale, integral_count, c.obs_sm_sig);
}


//Assumes SFRs are already translated
void calc_density(int64_t step, int64_t num_to_process, struct sf_model c) {
  int64_t i, n1=galaxy_offsets[step+1]-galaxy_offsets[step];
  int64_t n2=num_to_process;
  struct catalog_galaxy *g1 = galaxies + galaxy_offsets[step];
  struct catalog_galaxy *g2 = galaxies + offsets[num_scales]-num_to_process-1;
  if ((scales[step] < DENS_MIN_SCALE) && !(POSTPROCESSING_MODE)) return;

  _calc_density(scales[step], g1, n1, g2, n2, c);
  for (i=0; i<NUM_DENS_BINS; i++) {
    fdens_counts[i + step*NUM_DENS_BINS] = dens_counts[i];
    fdens_sf[i + step*NUM_DENS_BINS] = dens_sf[i];
    fdens_sf_ssfr[i + step*NUM_DENS_BINS] = dens_sf_ssfr[i];
  }
}

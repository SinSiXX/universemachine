#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <inttypes.h>
#include "sf_model.h"

#define FITTING_ROUNDS 4
#define CHI2_LIMIT 1e-2
#define STEP_LIMIT 8
#define MIN_FIT_LIMIT 10
#define STEP_RATIO (1/2.0) //How quickly step sizes shrink

int64_t iterations = 0;
void (*chi2_function)(struct sf_model_allz *) = NULL;

void fitting_round(double *params, double *steps);
double eval_chi2(double *params);

void initial_fit(double *params, double *steps, void (*c2f)(struct sf_model_allz *), FILE *output) {
  int64_t i;
  chi2_function = c2f;
  fprintf(output, "Initial Chi2: %e\n", eval_chi2(params));
  for (i=0; i<FITTING_ROUNDS; i++) fitting_round(params, steps);
  double chi2 = eval_chi2(params);
  for (i=0; i<NUM_PARAMS; i++) fprintf(output, "%.12f ", params[i]);
  fprintf(output, "%e\n", chi2);
  fprintf(output, "Iterations: %"PRId64"\n", iterations);
}

inline double minimum_delta(double chi2_l, double chi2, double chi2_r)
{
  double d_chi2_dx = (chi2_r - chi2_l) / 2.0;
  double d2_chi2_dx2 = (chi2_r + chi2_l - 2*chi2);
  if (chi2 > chi2_r && chi2 > chi2_l) // We are close to a local maximum
    return ((chi2_l < chi2_r) ? -1 : 1);
  return (d2_chi2_dx2 ? (-d_chi2_dx / d2_chi2_dx2) : 0);
}


double improve_fit(double *params, double *steps, int i, double chi2)
{
  double chi2_l, chi2_r, p, dx, chi2_new;
  p = params[i];
  params[i] = p + steps[i];
  chi2_r = eval_chi2(params);
  params[i] = p - steps[i];
  chi2_l = eval_chi2(params);

  dx = minimum_delta(chi2_l, chi2, chi2_r);
  if (fabs(dx) > STEP_LIMIT) dx = copysign(STEP_LIMIT, dx);
  params[i] = p + dx*steps[i];
  chi2_new = eval_chi2(params);

  if (chi2_l < chi2_new) { chi2_new = chi2_l; params[i] = p-steps[i]; }
  if (chi2_r < chi2_new) { chi2_new = chi2_r; params[i] = p+steps[i]; }
  if (chi2 < chi2_new) { chi2_new = chi2; params[i] = p; }
  steps[i]*=STEP_RATIO;
  //  fprintf(stderr, ".");
  return chi2_new;
}


void fitting_round(double *params, double *steps)
{
  double last_chi2, chi2;
  double cur_steps[NUM_PARAMS];
  int i, j=0;
  
  memcpy(cur_steps, steps, sizeof(double)*NUM_PARAMS);
  chi2 = eval_chi2(params);
  last_chi2 = chi2*(2+CHI2_LIMIT);
  while (chi2*(1+CHI2_LIMIT) < last_chi2 || (j++ < MIN_FIT_LIMIT)) {
    last_chi2 = chi2;
    for (i=0; i<NUM_PARAMS; i++)
      chi2 = improve_fit(params, cur_steps, i, chi2);
  }
}


double eval_chi2(double *params) {
  iterations++;
  struct sf_model_allz m = {{0}};
  memcpy(m.params, params, NUM_PARAMS*sizeof(double));
  assert_model_default(&m);
  memcpy(params, m.params, NUM_PARAMS*sizeof(double));
  chi2_function(&m);
  return CHI2(m);
}

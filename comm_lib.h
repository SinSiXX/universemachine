#ifndef _COMM_LIB_H_
#define _COMM_LIB_H_

#include <inttypes.h>
#include "sf_model.h"

struct galaxy_comm {
  int64_t step, num_to_xfer;
  struct sf_model_allz m;
};

void comm_client(char *serv_addr, char *port, int sockfd, int64_t chunk_num);
void comm_loop(int64_t s, int64_t c, int64_t cat_c, int64_t corr_c);
void comm_sender(int64_t chunk_num, int64_t cat_c, char *local_addr, char *local_port);
void send_galaxies(int64_t j, int64_t step);
void receive_galaxies(int64_t j, int64_t *step);
void send_data_to_corr(int64_t corr_c, int64_t step, struct sf_model_allz m);

#endif /*_COMM_LIB_H_*/

#ifndef FIT_SMHM_H

#define NUM_PARAMS 19

#define SMHM_EFF_0(x)     (x[0])
#define SMHM_EFF_0_A(x)   (x[1])
#define SMHM_EFF_0_A2(x)  (x[2])
#define SMHM_EFF_0_Z(x)   (x[3])
#define SMHM_M_1(x)       (x[4])
#define SMHM_M_1_A(x)     (x[5])
#define SMHM_M_1_A2(x)    (x[6])
#define SMHM_M_1_Z(x)     (x[7])
#define SMHM_ALPHA(x)     (x[8])
#define SMHM_ALPHA_A(x)   (x[9])
#define SMHM_ALPHA_A2(x)  (x[10])
#define SMHM_ALPHA_Z(x)   (x[11])
#define SMHM_BETA(x)      (x[12])
#define SMHM_BETA_A(x)    (x[13])
#define SMHM_BETA_Z(x)    (x[14])
#define SMHM_DELTA(x)     (x[15])
#define SMHM_GAMMA(x)     (x[16])
#define SMHM_GAMMA_A(x)   (x[17])
#define SMHM_GAMMA_Z(x)   (x[18])

#endif /*FIT_SMHM_H*/

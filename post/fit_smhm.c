#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <inttypes.h>
#include <string.h>
#include "../sampler.h"
#include "../check_syscalls.h"
#include "../mt_rand.h"
#include "fit_smhm.h"

#define TOL 0.03
#define NUM_STEPS 500000


void load_data(char *fn);
void run_sampler(void);

struct data {
  double z, m, sm, err;
};

struct smhm {
  double m_1, alpha, beta, delta, sm_0, gamma, f_1;
};

struct data *points=NULL;
int64_t np = 0;


int main(int argc, char **argv)
{
  r250_init(87L);

  if (argc < 2) {
    fprintf(stderr, "Usage: %s data.med\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  for (int64_t i=1; i<argc; i++)
    load_data(argv[i]);

  run_sampler();
  return 0;
}


double exp10(double x) {
  double a = exp(M_LN10*x);
  return a;
}


struct smhm smhm_at_z(double z, double *f) {
  struct smhm c;
  double a = 1.0/(1.0+z);
  double a1 = a-1.0;
  double lna = log(a);

  c.m_1 = SMHM_M_1(f) + a1*SMHM_M_1_A(f) - lna*SMHM_M_1_A2(f) + z*SMHM_M_1_Z(f);
  c.sm_0 = c.m_1 + SMHM_EFF_0(f) + a1*SMHM_EFF_0_A(f) - lna*SMHM_EFF_0_A2(f) + z*SMHM_EFF_0_Z(f);
  c.alpha = SMHM_ALPHA(f) + a1*SMHM_ALPHA_A(f) - lna*SMHM_ALPHA_A2(f) + z*SMHM_ALPHA_Z(f);
  c.beta = SMHM_BETA(f) + a1*SMHM_BETA_A(f) + z*SMHM_BETA_Z(f);
  c.delta = SMHM_DELTA(f);
  c.gamma = pow(10, SMHM_GAMMA(f) + a1*SMHM_GAMMA_A(f) + z*SMHM_GAMMA_Z(f));
  return c;
}

double calc_sm_at_m(double m, struct smhm c) {
  double dm = m-c.m_1;
  double dm2 = dm/c.delta;
  double sm = c.sm_0 - log10(exp10(-c.alpha*dm) + exp10(-c.beta*dm)) + c.gamma*exp(-0.5*(dm2*dm2));
  return sm;
}

double calc_chi2(double *params) {
  int64_t i;
  double last_z = -1, chi2=0;
  struct smhm c = {0};

  for (i=0; i<np; i++) {
    if (points[i].z != last_z) {
      last_z = points[i].z;
      c = smhm_at_z(points[i].z, params);
      if (c.delta < 0) return 1e30;
    }
    double err = calc_sm_at_m(points[i].m, c) - points[i].sm;
    if (fabs(err) < TOL) err = 0;
    else err = fabs(err) - TOL;
    err /= points[i].err;
    chi2 += err*err;
  }
  return chi2;
}

void print_bestfit(double *params) {
  int64_t i, j;
  char buffer[1024];
  double last_z = -1;
  FILE *out = NULL, *out2 = NULL;
  double chi2 = calc_chi2(params);
  struct smhm c = {0};

  check_mkdir("results", 0755);
  
  for (i=0; i<np; i++) {
    if (points[i].z != last_z) {
      if (out) { fclose(out); fclose(out2); }
      snprintf(buffer, 1024, "results/best_fit_z%g.dat", points[i].z);
      out = check_fopen(buffer, "w");
      snprintf(buffer, 1024, "results/data_z%g.dat", points[i].z);
      out2 = check_fopen(buffer, "w");
      fprintf(out, "#z: %g\n", points[i].z);
      fprintf(out2, "#z: %g\n", points[i].z);
      fprintf(out, "#Chi2: %g\n", chi2);
      fprintf(out, "#Params:");
      for (j=0; j<NUM_PARAMS; j++) fprintf(out, " %g", params[j]);
      fprintf(out,"\n");
      fprintf(out, "#M SM\n");
      fprintf(out2, "#M SM_sim SM_sim_err\n");
      last_z = points[i].z;
      c = smhm_at_z(points[i].z, params);
    }
    double sm = calc_sm_at_m(points[i].m, c);
    fprintf(out, "%g %g\n", points[i].m, sm);
    fprintf(out2, "%g %g %g\n", points[i].m, points[i].sm, points[i].err);
  }
  if (out) { fclose(out); fclose(out2); }
}


/*
Eff: -1.777 + (-0.006*(a-1) + -0.000*z)*nu + -0.119*(a-1)
nu: exp(-4.000*a^2)
M_h: 11.514 + (-1.793*(a-1) + -0.251*z)*nu
Alpha: -1.412 + (0.731*(a-1) + 0.000*z)*nu
Delta: 3.508 + (2.608*(a-1) + -0.043*z)*nu
Gamma: 0.316 + (1.319*(a-1) + 0.279*z)*nu
*/

void run_sampler(void) {
  int64_t i, j;
  double initial_params[NUM_PARAMS] = {-1.25109, 0.983814, 0.127493, 0.13463, 12.0271, 2.16933, 1.81275, -0.140366, 2.27961, -0.956498, -0.68459, 0.00603781, 0.387591, -0.140332, -0.0687521, 0.45946, -1.21011, -1.02497, -0.456822};

  double best_params[NUM_PARAMS] = {0}, best_chi2 = -1;

  struct EnsembleSampler *e = new_sampler(100, NUM_PARAMS, 2.0);
  for (i=0; i<e->num_walkers; i++) {
    double pos[NUM_PARAMS];
    for (j=0; j<NUM_PARAMS; j++) pos[j] = initial_params[j]+dr250()*0.01;
    init_walker_position(e->w+i, pos);
  }
  
  for (i=0; i<NUM_STEPS; i++) {
    struct Walker *w = get_next_walker(e);
    double chi2 = calc_chi2(w->params);
    if (best_chi2 < 0 || chi2 < best_chi2) {
      memcpy(best_params, w->params, sizeof(double)*NUM_PARAMS);
      best_chi2 = chi2;
    }
    update_walker(e, w, chi2);
    if (ensemble_ready(e)) {
      update_ensemble(e);
      write_accepted_steps_to_file(e, stdout, 0);
    }
  }  
  fprintf(stderr, "#Acceptance fraction: %f\n", acceptance_fraction(e));
  free_sampler(e);
  print_bestfit(best_params);
}


void load_data(char *fn) {
  char buffer[1024];
  FILE *in = check_fopen(fn, "r");
  struct data d={0};
  while(fgets(buffer, 1024, in)) {
    if (sscanf(buffer, "%lf %lf %lf %lf", &d.z, &d.m, &d.sm, &d.err)<4) continue;
    check_realloc_every(points, sizeof(struct data), np, 1000);
    if (d.err < TOL) d.err = TOL;
    points[np]=d;
    np++;
  }
  fclose(in);
}




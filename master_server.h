#ifndef _MASTER_SERVER_H_
#define _MASTER_SERVER_H_

#define AUTO_CONFIG_NAME "auto-um.cfg"

int64_t read_positions(FILE *posfile, int64_t reset_after_hash);
int64_t setup_server_port(void);
void print_time(FILE *output);
void master_server_loop(void);
void fitter_loop(void);
void fork_box_from_master(char *master_server);

#endif /* _MASTER_SERVER_H_ */

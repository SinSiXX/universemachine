#ifndef _RANK_HALO_SPLINES_H_
#define _RANK_HALO_SPLINES_H_

/*Less likely to change*/
#define SAT 6  /* UPID column */
#define VM 16  /* Vmax column */
#define POS 17 /* X column    */
#define VX 20  /* VX column   */
#define JX 23  /* JX Column   */

/*More likely to change*/
#define NUM_LABELS 80

#define AX 48          /* Ax (shape axis) column */
#define AX500 53       /* Ax_500c column         */
#define BTOA 46        /* B/A ratio column       */
#define BTOA500c 51    /* B/A(500c) column       */

#define VMP 77         /* Vmax @ Mpeak column    */
#define VMVP 80        /* Vmax/Vpeak column      */
#define VP 63          /* Vpeak column           */


#define NUM_PROPS (NUM_LABELS+1)  /* Don't change */

struct halo_property {  /* Don't change */
  char label[50];
  int important;
  int scaling;
};



//#scale(0) id(1) desc_scale(2) desc_id(3) num_prog(4) pid(5) upid(6) desc_pid(7) phantom(8) sam_Mvir(9) Mvir(10) Rvir(11) rs(12) vrms(13) mmp?(14) scale_of_last_MM(15) vmax(16) x(17) y(18) z(19) vx(20) vy(21) vz(22) Jx(23) Jy(24) Jz(25) Spin(26) Breadth_first_ID(27) Depth_first_ID(28) Tree_root_ID(29) Orig_halo_ID(30) Snap_num(31) Next_coprogenitor_depthfirst_ID(32) Last_progenitor_depthfirst_ID(33) Last_mainleaf_depthfirst_ID(34) Tidal_Force(35) Tidal_ID(36) Rs_Klypin(37) Mmvir_all(38) M200b(39) M200c(40) M500c(41) M2500c(42) Xoff(43) Voff(44) Spin_Bullock(45) b_to_a(46) c_to_a(47) A[x](48) A[y](49) A[z](50) b_to_a(500c)(51) c_to_a(500c)(52) A[x](500c)(53) A[y](500c)(54) A[z](500c)(55) T/|U|(56) M_pe_Behroozi(57) M_pe_Diemer(58) Halfmass_Radius(59) Macc(60) Mpeak(61) Vacc(62) Vpeak(63) Halfmass_Scale(64) Acc_Rate_Inst(65) Acc_Rate_100Myr(66) Acc_Rate_1*Tdyn(67) Acc_Rate_2*Tdyn(68) Acc_Rate_Mpeak(69) Acc_Log_Vmax_Inst(70) Acc_Log_Vmax_1*Tdyn(71) Mpeak_Scale(72) Acc_Scale(73) First_Acc_Scale(74) First_Acc_Mvir(75) First_Acc_Vmax(76) Vmax\@Mpeak(77) Tidal_Force_Tdyn(78) Log_(Vmax/Vmax_max(Tdyn;Tmpeak))(79) Time_to_future_merger(80) Future_merger_MMP_ID(81)

/*Column descriptors: name, calculate(1)/don't calculate(0) splines, and expected vmax scaling power */

struct halo_property properties[NUM_PROPS] =
  /* Less likely to change */
  {{"scale",0,0}, {"id",0,0}, {"desc_scale",0,0}, {"desc_id",0,0}, {"num_prog",0,0}, 
   {"pid",0,0}, {"upid",0,0}, {"desc_pid",0,0}, {"phantom",0,0}, {"sam_mvir",0,3},
   {"mvir",0,3}, {"rvir",0,0}, {"rs",0,1}, {"vrms",0,1}, {"mmp",0,0}, 
   {"scale_of_last_MM",0,0}, {"vmax",0,1}, {"x",0,0}, {"y",0,0}, {"z",0,0},
   {"-vabs",0,1}, {"vy",0,0}, {"vz",0,0}, {"Jabs",0,5}, {"Jy",0,0}, {"Jz",0,0},
   {"Spin",0,0}, {"Breadth_first_ID",0,0}, {"Depth_first_ID",0,0}, {"Tree_root_ID",0,0},
   {"Orig_halo_ID",0,0}, {"Snap_num",0,0}, {"Next_coprogenitor_depthfirst_ID",0,0},
   {"Last_progenitor_depthfirst_ID",0,0}, {"Last_mainleaf_depthfirst_ID",0,0},
   {"Tidal_Force", 1, 0}, {"Tidal_Force_ID", 0, 0},
					      
   /* More likely to change */
   {"Rs_Klypin",0,1}, {"Mvir_all",0,3}, {"M200b",0,3}, {"M200c",0,3}, {"M500c",0,3},
   {"M2500c",0,3}, {"Xoff",0,1}, {"Voff",0,1}, {"Spin_Bullock",0,0}, {"b_to_a",0,0},
   {"c_to_a",0,0}, {"Aabs",0,1}, {"Ay",0,0}, {"Az",0,0}, {"b_to_a500c",0,0},
   {"c_to_a500c",0,0}, {"A500c_abs",0,1}, {"Ay500c",0,0}, {"Az500c",0,0}, {"TU",0,0},
   {"M_pe_Behroozi",0,3}, {"M_pe_Diemer",0,3}, {"Halfmass_Radius", 1, 1}, {"Macc",0,3},
   {"Mpeak",0,3}, {"Vacc",0,1}, {"Vpeak",0,1}, {"Halfmass_Scale",0,0},
   {"Acc_Rate_Inst",0,3}, {"Acc_Rate_100Myr",0,3},
   {"Acc_Rate_1Tdyn",0,3}, {"Acc_Rate_2Tdyn",0,3}, {"Acc_Rate_Mpeak",0,3},
   {"Acc_Log_Vmax_Inst",0,0}, {"Acc_Log_Vmax_Tdyn",0,0},
   {"Mpeak_Scale",0,0}, {"Acc_Scale",0,0}, {"First_Acc_Scale",0,0}, 
   {"First_Acc_Mvir",0,3}, {"First_Acc_Vmax",0,1}, {"VmaxMpeak",0,0}, 
   {"Tidal_Force_Tdyn",1,0}, {"DeltaLogVmax", 1, 0}, {"VmaxVpeak",1,0}};

#endif /* _RANK_HALO_SPLINES_H_ */

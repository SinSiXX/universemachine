#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <math.h>
#include <inttypes.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <assert.h>
#include "check_syscalls.h"
#include "make_sf_catalog.h"
#include "stats.h"

int main(int argc, char **argv) {
  int64_t i, new_length;
  if (argc<4) {
    printf("Usage: %s sfr_list.bin sm_thresh slope [do_stats] [sm(0)/uv(1)_dust_model] [uv_thresh]\n", argv[0]);
    exit(1);
  }
  
  double sm_thresh = atof(argv[2]);
  if (sm_thresh < 15) sm_thresh = pow(10, sm_thresh);
  double slope = atof(argv[3]);
  int64_t do_stats = 0;
  if (argc>4) do_stats = atol(argv[4]);
  int64_t dust_model = 0;
  if (argc>5) dust_model = atol(argv[5]);
  double uv_thresh = 0;
  if (argc>6) uv_thresh = atof(argv[6]);
  
  struct binstats *uv_obs = init_binstats(-24, -17, 2, 1, 0);
  struct binstats *auv_obs = init_binstats(-24, -17, 2, 1, 0);
  struct binstats *av_ir = init_binstats(8,11, 4, 1, 1);
  struct binstats *av_uv = init_binstats(8,11, 4, 1, 1);
  
  struct catalog_halo *p = check_mmap_file(argv[1], 'r', &new_length);
  int64_t num_p = new_length / sizeof(struct catalog_halo);
  if (do_stats) {
    printf("#UV SM\n");
  } else {
    printf("#ID DescID UPID Flags Uparent_Dist X Y Z VX VY VZ M V MP VMP R Rank1 Rank2 RA RARank SM ICL SFR obs_SM obs_SFR SSFR SM/HM obs_UV\n");
    printf("#Columns:\n");
    printf("#ID: Unique halo ID\n");
    printf("#DescID: ID of descendant halo (or -1 at z=0).\n");
    printf("#UPID: -1 for central halos, otherwise, ID of largest parent halo\n");
    printf("#Flags: Ignore\n");
    printf("#Uparent_Dist: Ignore\n");
    printf("#X Y Z: halo position (comoving Mpc/h)\n");
    printf("#VX VY VZ: halo velocity (physical peculiar km/s)\n");
    printf("#M: Halo mass (Bryan & Norman 1998 virial mass, Msun)\n");
    printf("#V: Halo vmax (physical km/s)\n");
    printf("#MP: Halo peak historical mass (BN98 vir, Msun)\n");
    printf("#VMP: Halo vmax at the time when peak mass was reached.\n");
    printf("#R: Halo radius (BN98 vir, comoving kpc/h)\n");
    printf("#Rank1: halo rank in Delta_vmax (see UniverseMachine paper)\n");
    printf("#Rank2, RA, RARank: Ignore\n");
    printf("#SM: True stellar mass (Msun)\n");
    printf("#ICL: True intracluster stellar mass (Msun)\n");
    printf("#SFR: True star formation rate (Msun/yr)\n");
    printf("#Obs_SM: observed stellar mass, including random & systematic errors (Msun)\n");
    printf("#Obs_SFR: observed SFR, including random & systematic errors (Msun/yr)\n");
    printf("#SSFR: observed SSFR\n");
    printf("#Obs_UV: Observed UV Magnitude (M_1500 AB)\n");
    printf("#SMHM: SM/HM ratio\n");
    printf("#ID DescID UPID Flags Uparent_Dist X Y Z VX VY VZ M V MP VMP R Rank1 Rank2 RA RARank SM ICL SFR obs_SM obs_SFR SSFR SM/HM obs_UV\n");
  }

  float uv_dust_thresh = -21 + log10(sm_thresh/1.6e9)*-2.5;
  
  for (i=0; i<num_p; i++) {
    struct catalog_halo *tp = p+i;
    if ((tp->flags & IGNORE_FLAG)) continue;
    float ssfr = 0;
    if (tp->sm > 0)
      ssfr = tp->obs_sfr / tp->obs_sm;
    float smhm = tp->sm*0.68 / tp->mp;
    float a_uv = 0;

    if (dust_model)
      a_uv = 2.5*log10f(1.0+pow(10,slope*(tp->obs_uv-uv_dust_thresh)/-2.5));
    else
      a_uv = 2.5*log10f(1.0+slope*tp->obs_sm/sm_thresh);
    
    float obs_uv = tp->obs_uv + a_uv;
    if (do_stats) {
      add_to_binstats(uv_obs, obs_uv, tp->obs_sm*2.0);
      add_to_binstats(auv_obs, obs_uv, a_uv);
      add_to_binstats(av_uv, tp->obs_sm*2.0, pow(10, obs_uv/-2.5));
      add_to_binstats(av_ir, tp->obs_sm*2.0, expm1(M_LN10*a_uv/2.5)*pow(10, obs_uv/-2.5));
    } else {
      if (uv_thresh && obs_uv > uv_thresh) continue;
      printf("%"PRId64" %"PRId64" %"PRId64" %"PRId32" %f %f %f %f %f %f %f %.3e %f %.3e %f %f %f %f %f %f %.3e %.3e %.3e %.3e %.3e %.3e %.3e %.3f\n",
	     tp->id, tp->descid, tp->upid, tp->flags, tp->uparent_dist, tp->pos[0], tp->pos[1], tp->pos[2], tp->pos[3], tp->pos[4], tp->pos[5], tp->m/0.68, tp->v, tp->mp/0.68, tp->vmp, tp->r, tp->rank1, tp->rank2, tp->ra, tp->rarank, tp->sm, tp->icl, tp->sfr, tp->obs_sm, tp->obs_sfr, ssfr, smhm, obs_uv);
    }
  }
  munmap(p, new_length);

  if (do_stats) {
    calc_median(uv_obs);
    calc_median(auv_obs);
    int64_t i;
    for (i=0; i<uv_obs->num_bins; i++) {
      if (!uv_obs->counts[i]) continue;
      double key = uv_obs->keyavg[i];
      if (uv_obs->logarithmic) key = pow(10, key);
      if (uv_obs->names && uv_obs->names[i]) fprintf(stdout, "%s ", uv_obs->names[i]);
      else fprintf(stdout, "%g ", key);
      /*fprintf(stdout, "%g %g %g %g %g %g %"PRId64"\n",
	uv_obs->med[i], uv_obs->stdup[i], uv_obs->stddn[i], uv_obs->avg[i], uv_obs->avgsd[i], sqrt(uv_obs->sd[i]), uv_obs->counts[i]);*/
      fprintf(stdout, "%g %g\n",
	      uv_obs->med[i], auv_obs->med[i]);
    }
    FILE *out = check_fopen("irx_sm.dat", "w");
    for (i=0; i<av_ir->num_bins; i++) {
      if (!av_ir->counts[i]) continue;
      fprintf(out, "%g %g %g\n", av_ir->keyavg[i], av_ir->avg[i]/av_uv->avg[i], av_uv->avg[i]);
    }
    fclose(out);
  }
  return 0;
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <math.h>
#include <inttypes.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <gsl/gsl_interp.h>
#include <unistd.h>
#include "inthash.h"
#include "make_sf_catalog.h"
#include "cached_io.h"
#include "mt_rand.h"

#define EXTRA_HALO_INFO struct halo *vpeak_halo, *mpeak_halo, *acc_halo;
#include "read_tree/read_tree.h"
#include "read_tree/read_tree.c"
#include "check_syscalls.h"

double box_size = 250;
void process_tree(void);
void load_boxlist(char *fn);
int64_t num_boxes = 0;
char **boundaries = NULL;
struct inthash *boxes = NULL;
struct cached_io **outputs = NULL;
int64_t num_outputs = 0;
float *scales = NULL;
int64_t num_scales = 0;

struct spline {
  double value;
  int64_t num_points;
  gsl_interp *spline;
  gsl_interp_accel *accel;
  double *x, *y;
};

struct spline *splines[2] = {0};
int64_t *spline_offsets[2] = {0};
int64_t num_splines[2] = {0};
double scaling[2] = {0};

void open_outputs(void);
void close_outputs(void);
void combine_outputs(void);
void load_ranks(char *path, int64_t index);

int main(int argc, char **argv) {
  int64_t i;
  char buffer[1024];
  if (argc<5) {
    printf("Usage: %s path/to/ranks scale ...\n", argv[0]);
    exit(1);
  }

  r250_init(87L);

  for (i=2; i<argc; i++) {
    read_hlist(argv[i]);
    if (i==2) {
      sprintf(buffer, "%s/mvir", argv[1]);
      load_ranks(buffer, 0, scale);
      sprintf(buffer, "%s/Spin_Bullock", argv[1]);
      load_ranks(buffer, 1, scale);
    }
    process_tree();
    delete_tree();
  }
  close_outputs();
  combine_outputs();
  return 0;
}

int sort_splines(const void *a, const void *b) {
  const struct spline *c = a;
  const struct spline *d = b;
  if (c->value < d->value) return -1;
  if (c->value > d->value) return 1;
  return 0;
}

void load_ranks(char *path, int64_t index) {
  int64_t i,j;
  char buffer[1024];
  check_realloc_s(spline_offsets[index], sizeof(int64_t), num_scales+1);
  struct spline s = {0};
  SHORT_PARSETYPE;
  void **data = NULL;
  enum parsetype *pt = NULL;
  spline_offsets[index][0] = 0;
  for (i=0; i<num_scales; i++) {
    sprintf(buffer, "%s/splines_%.5f.list", path, scales[i]);
    FILE *in = check_fopen(buffer, "r");
    while (fgets(buffer, 1024, in)) {
      if (buffer[0] == '\n' || buffer[0] == ' ') continue;
      if (buffer[0] == '#') {
	if (!strncmp(buffer, "#scaling =", 10))
	  sscanf(buffer+11, "%lf", scaling+index);
	continue;
      }
      if (sscanf(buffer, "%lf %"SCNd64, &s.value, &s.num_points)!=2) continue;
      s.x = check_realloc(0, sizeof(double)*s.num_points, "Allocating x");
      s.y = check_realloc(0, sizeof(double)*s.num_points, "Allocating y");
      check_realloc_s(data, sizeof(void *), s.num_points*2+2);
      check_realloc_s(pt, sizeof(enum parsetype), s.num_points*2+2);
      data[0] = &s.value;
      pt[0] = F64;
      data[1] = &s.num_points;
      pt[1] = D64;
      for (j=0; j<s.num_points; j++) {
	data[j*2+2] = s.x+j;
	data[j*2+3] = s.y+j;
	pt[j*2+2] = pt[j*2+3] = F64;
      }
      int64_t n = stringparse(buffer, data, pt, s.num_points*2+2);
      if (n!=(s.num_points*2+2)) continue;
      const gsl_interp_type *T = (s.num_points > 4) ? gsl_interp_akima : gsl_interp_cspline;
      s.spline = gsl_interp_alloc(T, s.num_points);
      s.accel = gsl_interp_accel_alloc();
      gsl_interp_init(s.spline, s.x, s.y, s.num_points);
      check_realloc_every(splines[index], sizeof(struct spline), num_splines[index], 1000);
      splines[index][num_splines[index]] = s;
      num_splines[index]++;
    }
    fclose(in);
    spline_offsets[index][i+1] = num_splines[index];
  }
  for (i=0; i<num_scales; i++)
    qsort(splines[index]+spline_offsets[index][i], spline_offsets[index][i+1]-spline_offsets[index][i], sizeof(struct spline), sort_splines);
}


int64_t mmap_copy(char *fn, FILE *out) {
  int64_t length=0;
  void *data = check_mmap_file(fn, 'r', &length);
  if (length > 0) {
    check_fwrite(data, 1, length, out);
    munmap(data, length);
  }
  return length;
}

void combine_outputs(void) {
  int64_t i,j;
  char buffer[1024];
  for (i=0; i<num_boxes; i++) {
    sprintf(buffer, "catalog_bin/cat.box%"PRId64".dat", i);
    FILE *out = check_fopen(buffer, "w");
    sprintf(buffer, "catalog_bin/offsets.box%"PRId64".txt", i);
    FILE *offsets = check_fopen(buffer, "w");
    fprintf(offsets, "#Output Scale Offset(halos)\n");
    int64_t offset = 0;
    for (j=0; j<num_scales; j++) {
      fprintf(offsets, "%"PRId64" %f %"PRId64"\n", j, scales[j], offset);
      sprintf(buffer, "catalog_bin/cat.box%"PRId64".snap%"PRId64".dat", i, j);
      int64_t written = mmap_copy(buffer, out);
      if (written % sizeof(struct catalog_halo)) {
	fprintf(stderr, "Error: read %"PRId64" bytes from %s, which is not divisible by size of packed halo struct (%"PRId64" bytes)\n", written, buffer, (int64_t)sizeof(struct catalog_halo));
	exit(1);
      }
      offset += written / sizeof(struct catalog_halo);
    }
    fprintf(offsets, "#Total halos: %"PRId64"\n", offset);
    fclose(offsets);
    fclose(out);
    for (j=0; j<num_scales; j++) {
      sprintf(buffer, "catalog_bin/cat.box%"PRId64".snap%"PRId64".dat", i, j);
      unlink(buffer);
    }
  }
}


void open_outputs(void) {
  int64_t i,j;
  char buffer[1024];
  num_outputs = num_boxes*halo_tree.num_lists;
  check_realloc_s(outputs, sizeof(struct cached_io *), num_outputs);
  mkdir("catalog_bin", 0755);
  check_realloc_s(scales, sizeof(float), halo_tree.num_lists);
  FILE *snaps = check_fopen("catalog_bin/snaps.txt", "w");
  for (i=0; i<num_boxes; i++) {
    for (j=0; j<halo_tree.num_lists; j++) {
      sprintf(buffer, "catalog_bin/cat.box%"PRId64".snap%"PRId64".dat", i, j);
      outputs[i*halo_tree.num_lists+j] = cfopen(buffer, 500000);
      outputs[i*halo_tree.num_lists+j]->clear = 1;
      int64_t snap = halo_tree.num_lists - j - 1;
      if (i==0) {
	scales[j] = halo_tree.halo_lists[snap].scale;
	fprintf(snaps, "%"PRId64" %f\n", j, halo_tree.halo_lists[snap].scale);
      }
    }
  }
  fclose(snaps);
  num_scales = halo_tree.num_lists;
}

void close_outputs(void) {
  int64_t i;
  for (i=0; i<num_outputs; i++) cfclose(outputs[i]);
}

void load_boxlist(char *fn) {
  char buffer[1024];
  FILE *in = check_fopen(fn, "r");
  boxes = new_inthash();
  num_boxes = -1;
  int64_t hid;
  SHORT_PARSETYPE;
  enum parsetype t = D64;
  void *data = &hid;
  while (fgets(buffer, 1024, in)) {
    if (buffer[0] == '#') {
      if (strncmp(buffer, "#Box", 4)==0) {
	num_boxes++;
	check_realloc_s(boundaries, sizeof(char *), (num_boxes+1));
      }
      if (strncmp(buffer, "#Bou", 4)==0) {
	boundaries[num_boxes] = check_strdup(buffer);
      }
      continue;
    }
    if (stringparse(buffer, &data, &t, 1) != 1) continue;
    ih_setint64(boxes, hid, num_boxes);
  }
  fclose(in);
  num_boxes++;
}

void calc_mass_vmax_peak(struct halo *h) {
  if (h->mpeak_halo) return;
  if (h->prog) {
    calc_mass_vmax_peak(h->prog);
    if (h->vmax > h->prog->vpeak_halo->vmax) h->vpeak_halo = h;
    else h->vpeak_halo = h->prog->vpeak_halo;

    if (h->mvir > h->prog->mpeak_halo->mvir) h->mpeak_halo = h;
    else h->mpeak_halo = h->prog->mpeak_halo;

    if (h->upid>-1) { // If we are a subhalo
      h->acc_halo = h->prog->acc_halo;
    } else {
      h->acc_halo = h;
    }
  } else {
    h->acc_halo = h->mpeak_halo = h->vpeak_halo = h;    
  }
}

int sort_by_dfid(const void *a, const void *b) {
  const int64_t *e = a;
  const int64_t *f = b;
  struct halo *c = all_halos.halos + *e;
  struct halo *d = all_halos.halos + *f;
  if (c->depth_first_id < d->depth_first_id) return -1;
  if (c->depth_first_id > d->depth_first_id) return 1;
  return 0;
}

double perc_to_normal(double p) {
  double x1 = -14;
  double x2 = 14;
  while (x2-x1 > 1e-7) {
    double half = 0.5*(x1+x2);
    double perc = 0.5*(1.0+erf(half));
    if (perc > p) { x2 = half; }
    else { x1 = half; }    
  }
  return ((x1+x2)*M_SQRT1_2);
}

double rand_rank(double value) {
  double neg_val = copysign(value, -1.0);
  double cumulative = 0.5*(1.0+erf(neg_val * M_SQRT1_2));
  double ra_perc = dr250()*cumulative;
  double ra_sd = perc_to_normal(ra_perc);
  return copysign(ra_sd, value);
}

double spline_rank(int64_t snap, double lvmp, double val, int64_t index) {
  struct spline *l = splines[index]+spline_offsets[index][snap];
  int64_t ns = spline_offsets[index][snap+1] - spline_offsets[index][snap];
  struct spline *h = l + ns - 1;
  while (l < h && ((l->x[0]>lvmp) || (l->x[l->num_points-1] < lvmp))) l++;
  while (h > l && ((h->x[0]>lvmp) || (h->x[h->num_points-1] < lvmp))) h--;
  if (h==l) return h->value;

#define EVAL(sp) (gsl_interp_eval(sp->spline, sp->x, sp->y, lvmp, sp->accel))
  if (EVAL(l) > val) return rand_rank(l->value);
  if (EVAL(h) < val) return rand_rank(h->value);
  while ((l+1)<h) {
    struct spline *m = l + (h-l)/2;
    double res = EVAL(m);
    if (res > val) h=m;
    else if (res < val) l=m;
    else return m->value;
  }
  double val_h = EVAL(h);
  double val_l = EVAL(l);
  if (val_h == val_l) return (0.5*(l->value+h->value));
  assert((val_l <= val) && (val_h >= val));
  double f = (val - val_l) / (val_h - val_l);  
  return (l->value + f*(h->value-l->value));
#undef EVAL
}

double halo_dist(struct halo *h1, struct halo *h2) {
  int64_t i;
  double ds = 0, dx;
  for (i=0; i<3; i++) {
    dx = fabs(h1->pos[i]-h2->pos[i]);
    if (dx > box_size/2.0) dx = box_size - dx;
    ds += dx*dx;
  }
  return sqrt(ds);
}

double host_dist(struct halo *h) {
  if (!h->mmp)
    if (h->desc && h->desc->prog) return (halo_dist(h, h->desc->prog)/(1e-3*h->desc->prog->rvir));

  if (h->upid < 0) return 2;
  if (!h->uparent) return 1.01;
  return (halo_dist(h, h->uparent)/(1e-3*h->uparent->rvir));
}

void process_tree(void) {
  int64_t j;
  struct halo *h;
  struct catalog_halo ch = {0};
  int64_t *process_order = NULL;

  check_realloc_s(process_order, sizeof(int64_t), all_halos.num_halos);
  for (j=0; j<all_halos.num_halos; j++) {
    h = &(all_halos.halos[j]);
    calc_mass_vmax_peak(h);
    process_order[j] = j;
  }

  qsort(process_order, all_halos.num_halos, sizeof(int64_t), sort_by_dfid);
  for (j=0; j<all_halos.num_halos; j++) {
    h = all_halos.halos+process_order[j];
    ch.id = h->id;
    ch.descid = h->descid;
    ch.upid = h->upid;
    memcpy(ch.pos, h->pos, sizeof(float)*3);
    memcpy(ch.pos+3, h->vel, sizeof(float)*3);
    ch.vmp = h->mpeak_halo->vmax;
    ch.lvmp = log10(ch.vmp);
    ch.mp = h->mpeak_halo->mvir;
    ch.m = h->mvir;
    ch.v = h->vmax;
    ch.r = h->rvir;
    ch.uparent_dist = host_dist(h);
    ch.ra = normal_random(0, 1);
    struct halo_list *hl = lookup_scale(h->scale);
    int64_t snap = hl - halo_tree.halo_lists;
    assert(snap >= 0);
    snap = halo_tree.num_lists - snap - 1;
    int64_t box = ih_getint64(boxes, h->tree_root_id);
    assert(box >= 0 && box < num_boxes);
    struct cached_io *cio = outputs[box*halo_tree.num_lists+snap];

    ch.rank1 = spline_rank(snap, ch.lvmp, ch.m / pow(ch.vmp, scaling[0]), 0);
    ch.rank2 = spline_rank(snap, ch.lvmp, h->spin_bullock / pow(ch.vmp, scaling[1]), 1);

    cfwrite(cio, &ch, sizeof(struct catalog_halo));
  }
}



#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <inttypes.h>
#include "mt_rand.h"
#include "check_syscalls.h"

int solve_lineq(int64_t dim, double *matrix, double *res) {
  int64_t i, j, k;
  double *tmp = NULL;
  check_realloc_s(tmp, sizeof(double), dim+1);
  for (i=0; i<dim; i++) {
    //Search for rows w/ nonzero element:
    for (j=i; j<dim; j++)
      if (matrix[j*(dim+1)+i]) break;
    if (j==dim) continue;

    //Swap row j and row i
    if (j!=i) {
      memcpy(tmp, matrix+j*(dim+1), sizeof(double)*(dim+1));
      memcpy(matrix+j*(dim+1), matrix+i*(dim+1), sizeof(double)*(dim+1));
      memcpy(matrix+i*(dim+1), tmp, sizeof(double)*(dim+1));
    }

    //Normalize row i
    double mul = 1.0/matrix[i*(dim+1)+i];
    matrix[i*(dim+1)+i] = 1;
    for (j=i+1; j<dim+1; j++) {
      matrix[i*(dim+1)+j] *= mul;
    }
    
    //Subtract row i from other rows
    for (j=0; j<dim; j++) {
      if (j==i) continue;
      double mul = -1.0*matrix[j*(dim+1)+i];
      for (k=i; k<dim+1; k++) {
	matrix[j*(dim+1)+k] += mul*matrix[i*(dim+1)+k];
      }
    }
  }
  free(tmp);

  for (i=0; i<dim; i++)
    res[i] = matrix[i*(dim+1)+dim];

  //Check matrix solution
  for (i=0; i<dim; i++)
    for (j=0; j<dim; j++) {
      if ((matrix[i*(dim+1)+j] != 0 && i!=j) ||
	  (matrix[i*(dim+1)+j] != 1 && i==j)) {
	printf("Couldn't solve b/c mat[%"PRId64"][%"PRId64"] = %g\n", i, j, matrix[i*(dim+1)+j]);
	return 0;
      }
    }

  return 1;
}


/*int main(void) {
  r250_init(87L);
  int64_t dim = 1000;
  double *matrix = NULL, *matrix2 = NULL;
  double *answers = NULL;
  check_realloc_s(matrix, sizeof(double), (dim)*(dim+1));
  check_realloc_s(answers, sizeof(double), dim);
  check_realloc_s(matrix2, sizeof(double), (dim)*(dim+1));

  int64_t i, j;
  for (i=0; i<(dim)*(dim+1); i++)
    matrix[i] = dr250();

  memcpy(matrix2, matrix, sizeof(double)*(dim)*(dim+1));
  
  int64_t res = solve_lineq(dim, matrix, answers);
  printf("%d\n", (int)res);
  for (i=0; i<dim; i++) {
    double s = 0;
    for (j=0; j<dim; j++)
      s += matrix2[i*(dim+1)+j]*answers[j];
    double err = s-matrix2[i*(dim+1)+dim];
    if (fabs(err) > 1e-9) printf("%g\n", err);
  }
  return 0;
}
*/

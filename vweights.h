#ifndef VWEIGHTS_H
#define VWEIGHTS_H

void calc_volume_cache(void);
extern double box_volume;
extern double *vweights, *total_volumes;

#endif /* VWEIGHTS_H */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>
#include <inttypes.h>
#include <math.h>
#include <assert.h>
#include <time.h>
#include "make_sf_catalog.h"
#include "inet/socket.h"
#include "inet/rsocket.h"
#include "config_vars.h"
#include "check_syscalls.h"
#include "server.h"
#include "bounds.h"
#include "box_server.h"
#include "comm_lib.h"
#include "corr_lib.h"


#define FAST3TREE_TYPE struct catalog_galaxy
#define FAST3TREE_PREFIX COMM
#define FAST3TREE_DIM 3
#include "fast3tree.c"

struct fast3tree *comm_tree = NULL;
struct client_info *comms = NULL;
int64_t num_comms = 0;
int64_t id = -1;
struct catalog_galaxy **data = NULL;
int64_t *num_cgalaxies = NULL;

void comm_client(char *serv_addr, char *port, int sockfd, int64_t chunk_num) {
  int64_t i, c, s, l, portnum, cat_c, corr_c;
  uint64_t magic;
  char *local_addr = NULL, local_port[10];
  char cmd[5] = {0};
  char buffer[100];

  sprintf(buffer, "Comm Server.%"PRId64".%"PRId64, box_id, chunk_num);
  set_rsocket_role(buffer);

  srand(time(NULL));
  c = connect_to_addr(serv_addr, port);
  recv_from_socket(c, &magic, sizeof(int64_t));
  assert (magic == UM_MAGIC);
  send_to_socket_noconfirm(c, &magic, sizeof(int64_t));
  send_to_socket_noconfirm(c, &num_scales, sizeof(int64_t));
  send_to_socket_noconfirm(c, scales, sizeof(float)*num_scales);
  send_to_socket_noconfirm(c, &chunk_num, sizeof(int64_t));
  local_addr = recv_msg_nolength(c, local_addr);

  s=-1;
  for (i=0; i<10000 && s<0; i+=29) {
    portnum = BOX_SERVER_PORT+i;
    snprintf(local_port, 10, "%"PRId64, portnum);
    s = listen_at_addr(local_addr, local_port);
  }
  if (i>=10000) {
    fprintf(stderr, "[Error] Couldn't start local comm server at %s:%d-%d!\n",
	    local_addr, (int)BOX_SERVER_PORT, (int)(portnum));
    exit(EXIT_FAILURE);
  }

  l = strlen(local_addr)+1;
  _send_to_socket(sockfd, &l, sizeof(int64_t));
  _send_to_socket(sockfd, local_addr, l);
  _send_to_socket(sockfd, &portnum, sizeof(int64_t));
  close(sockfd);

  cat_c = accept_connection(s, NULL, NULL);
  recv_from_socket(cat_c, cmd, 4);
  if (strcmp(cmd, "rdy!") != 0) {
    fprintf(stderr, "[Error] Catalog client failed to load data (%s).\n", cmd);
    exit(EXIT_FAILURE);
  }
  send_to_socket_noconfirm(c, &portnum, sizeof(int64_t));
  send_to_socket_noconfirm(c, max_bounds, sizeof(float)*6);
  send_to_socket(c, bounds, sizeof(float)*6*num_scales);

  //Fork correlation client
  int64_t pid = fork();
  if (pid < 0) system_error("Failed to fork process.");
  if (pid == 0) corr_client(local_addr, local_port, chunk_num);
  corr_c = accept_connection(s, NULL, NULL);

  //Receive client lists
  recv_from_socket(c, &id, sizeof(int64_t));
  recv_from_socket(c, &num_comms, sizeof(int64_t));
  check_realloc_s(comms, sizeof(struct client_info), num_comms);
  for (i=0; i<num_comms; i++) {
    recv_from_socket(c, comms+i, sizeof(struct client_info));
    comms[i].address = recv_msg_nolength(c, NULL);
    comms[i].bounds = recv_and_alloc(c, NULL, sizeof(float)*6*num_scales);
    comms[i].done_steps = comms[i].sent_steps = -1;
    assert(!i || (comms[i].id > comms[i-1].id));
  }
  send_to_socket(c, "thx!", 4);

  //Make/receive connections
  for (i=0; i<num_comms; i++) {
    if (comms[i].id < id) {
      int64_t cid, j;
      int64_t cs = accept_connection(s, NULL, NULL);
      recv_from_socket(cs, &cid, sizeof(int64_t));
      assert(cid < id);
      for (j=0; j<num_comms; j++) if (comms[j].id == cid) break;
      assert(j<num_comms);
      comms[j].cs = cs;
    }
    else {
      snprintf(port, 10, "%d", (int)comms[i].serv_port);
      random_sleep(0);
      comms[i].cs = connect_to_addr(comms[i].address, port);
      send_to_socket_noconfirm(comms[i].cs, &id, sizeof(int64_t));
    }
  }

  check_realloc_s(data, sizeof(struct catalog_galaxy *), num_scales);
  memset(data, 0,  sizeof(struct catalog_galaxy *)*num_scales);
  check_realloc_s(num_cgalaxies, sizeof(int64_t), num_scales);
  memset(num_cgalaxies, 0,  sizeof(int64_t)*num_scales);
  
  pid = fork();
  if (pid < 0) system_error("Failed to fork process.");
  if (pid == 0) comm_sender(chunk_num, cat_c, local_addr, local_port);

  send_to_socket(c, "rdy!", 4);
  comm_loop(s, c, cat_c, corr_c);
}


void comm_sender(int64_t chunk_num, int64_t cat_c, char *local_addr, char *local_port) {
  int64_t i, step, i_start = 0;
  int64_t c = connect_to_addr(local_addr, local_port);
  char buffer[100];
  sprintf(buffer, "Comm Sender.%"PRId64".%"PRId64, box_id, chunk_num);
  set_rsocket_role(buffer);
  
  for (i_start=0; i_start<num_comms; i_start++) if (comms[i_start].id > id) break;
  while (1) {
    //Catalog calculator done with step
    recv_from_socket(cat_c, &step, sizeof(int64_t));
    assert(step < num_scales);
    if (step < 0) exit(EXIT_SUCCESS);
    if (!comm_tree)
      comm_tree = fast3tree_init(galaxy_offsets[step+1]-galaxy_offsets[step],
				 galaxies+galaxy_offsets[step]);
    else
      fast3tree_rebuild(comm_tree, galaxy_offsets[step+1]-galaxy_offsets[step],
			galaxies+galaxy_offsets[step]);

    //Blast data to other processes
    send_to_socket_noconfirm(c, &step, sizeof(int64_t));
    for (i=0; i<num_comms; i++) {
      int64_t j = (i_start+i)%num_comms;
      send_galaxies(j,step);
    }
  }
}


void comm_loop(int64_t s, int64_t c, int64_t cat_c, int64_t corr_c) {
  int64_t i, step, corr_done = -1;
  int64_t *resp = NULL;
  struct sf_model_allz m = {{0}};
  check_realloc_s(resp, sizeof(int64_t), num_scales);
  memset(resp, 0, sizeof(int64_t)*num_scales);
  double min_scale = CORR_MIN_SCALE;
  if (POSTPROCESSING_MODE && min_scale > POSTPROCESS_CORR_MIN_SCALE)
    min_scale = POSTPROCESS_CORR_MIN_SCALE;
  while (scales[corr_done+1] < min_scale && corr_done < num_scales-1)
    corr_done++;

  int64_t sender = accept_connection(s, NULL, NULL);

  while (1) {
    clear_rsocket_tags();
    tag_rsocket(c);
    tag_rsocket(sender);
    tag_rsocket(corr_c);
    for (i=0; i<num_comms; i++) tag_rsocket(comms[i].cs);
    select_rsocket(RSOCKET_READ, 0);
    for (i=0; i<num_comms; i++) {
      if (!check_rsocket_tag(comms[i].cs)) continue;
      //Must be a client notifying/sending data
      receive_galaxies(i, &step);
      if (step > comms[i].done_steps) comms[i].done_steps = step;
      resp[step]++;
      if ((corr_done == (step-1)) && resp[step]==(num_comms+1))
	send_data_to_corr(corr_c, step, m);
    }

    //Server sending new parameter set
    if (check_rsocket_tag(c)) {
      recv_from_socket(c, &m, sizeof(struct sf_model_allz));
      send_to_socket_noconfirm(cat_c, &m, sizeof(struct sf_model_allz));
      if (INVALID(m)) {
	struct galaxy_comm gc = {0};
	gc.step = -1;
	send_to_socket(corr_c, &gc, sizeof(struct galaxy_comm));
	exit(EXIT_SUCCESS);
      }
      memset(resp, 0, sizeof(int64_t)*num_scales);
      memset(num_cgalaxies, 0, sizeof(int64_t)*num_scales);
      corr_done = -1;
      while (scales[corr_done+1] < min_scale && corr_done < num_scales-1)
	corr_done++;
      for (i=0; i<num_comms; i++)
	comms[i].done_steps = comms[i].sent_steps = -1;
    }

    if (check_rsocket_tag(corr_c)) {
      recv_from_socket(corr_c, &corr_done, sizeof(int64_t));
      //Send data to box server
      if (corr_done==num_scales-1)
	send_to_socket(c, shared_data, shared_data_length);
      else if (resp[corr_done+1]==(num_comms+1))
	send_data_to_corr(corr_c, corr_done+1, m);
    }

    //Sender notifying us that catalog is complete
    if (check_rsocket_tag(sender)) {
      recv_from_socket(sender, &step, sizeof(int64_t));
      resp[step]++;
      if ((corr_done == (step-1)) && resp[step]==(num_comms+1))
	send_data_to_corr(corr_c, step, m);
    }
  }
}


//Corr. library can modify galaxies as it wishes for given step after this call.
void send_data_to_corr(int64_t corr_c, int64_t step, struct sf_model_allz m) {
  int64_t num_galaxies_to_send = num_cgalaxies[step] + galaxy_offsets[step+1]-galaxy_offsets[step];
  int64_t location = offsets[num_scales] - num_galaxies_to_send - 1;
  struct catalog_galaxy * corr_data = galaxies + location;
  memcpy(corr_data, data[step], sizeof(struct catalog_galaxy)*num_cgalaxies[step]);
  corr_data += num_cgalaxies[step];
  free(data[step]);
  data[step] = 0;
  num_cgalaxies[step] = 0;
  memcpy(corr_data, galaxies+galaxy_offsets[step], sizeof(struct catalog_galaxy)*(galaxy_offsets[step+1]-galaxy_offsets[step]));
  struct galaxy_comm gc = {0};
  gc.step = step;
  gc.num_to_xfer = num_galaxies_to_send;
  gc.m = m;
  send_to_socket_noconfirm(corr_c, &gc, sizeof(struct galaxy_comm));
}

void _find_galaxies(struct catalog_galaxy **space, int64_t *num_to_send, float *cbounds, struct tree3_node *n) {
  if (!_fast3tree_box_intersect_box(n, cbounds)) return;
  if ((n->div_dim < 0) || _fast3tree_box_inside_box(n, cbounds)) {
    check_realloc_s(*space, sizeof(struct catalog_galaxy), (*num_to_send)+n->num_points);
    memcpy(*space+(*num_to_send), n->points, n->num_points*sizeof(struct catalog_galaxy));
    *num_to_send = *num_to_send + n->num_points;
  } else {
    _find_galaxies(space, num_to_send, cbounds, n->left);
    _find_galaxies(space, num_to_send, cbounds, n->right);
  }
}

void send_galaxies(int64_t j, int64_t step) {
  int64_t i, k;
  struct catalog_galaxy *space = NULL;
  struct galaxy_comm gc = {0};

  float cbounds[6], translate[3] = {0}; //Client bounds need to be translated to our space
  memcpy(cbounds, comms[j].bounds+(6*step), 6*sizeof(float));
  for (k=0; k<3; k++) {
    float av = 0.5*(cbounds[k]+cbounds[k+3]);
    float av2 = 0.5*(bounds[6*step+k]+bounds[6*step+k+3]);
    if (av2-av > BOX_SIZE/2.0) {
      translate[k] = -BOX_SIZE;
      cbounds[k] += BOX_SIZE;
      cbounds[k+3] += BOX_SIZE;
    }
    if (av-av2 > BOX_SIZE/2.0) {
      translate[k] = BOX_SIZE;
      cbounds[k] -= BOX_SIZE;
      cbounds[k+3] -= BOX_SIZE;
    }
    cbounds[k] -= CORR_LENGTH_RP;
    cbounds[k+3] += CORR_LENGTH_RP;
  }
  
  _find_galaxies(&space, &gc.num_to_xfer, cbounds, comm_tree->root);
  for (i=0; i<gc.num_to_xfer; i++) {
    for (k=0; k<3; k++) space[i].pos[k] += translate[k];
  }

  gc.step = step;
  send_to_socket_noconfirm(comms[j].cs, &gc, sizeof(struct galaxy_comm));
  if (gc.num_to_xfer) 
    send_to_socket_noconfirm(comms[j].cs, space, sizeof(struct catalog_galaxy)*gc.num_to_xfer);
  if (space) free(space);
}

void receive_galaxies(int64_t j, int64_t *step) {
  struct galaxy_comm gc = {0};
  recv_from_socket(comms[j].cs, &gc, sizeof(struct galaxy_comm));
  assert(gc.step < num_scales && gc.step >= 0);
  *step = gc.step;
  if (gc.num_to_xfer>0) {
    check_realloc_s(data[*step], (num_cgalaxies[*step]+gc.num_to_xfer), sizeof(struct catalog_galaxy));
    recv_from_socket(comms[j].cs, data[*step]+num_cgalaxies[*step], sizeof(struct catalog_galaxy)*gc.num_to_xfer);
    num_cgalaxies[*step] += gc.num_to_xfer;
  }
}

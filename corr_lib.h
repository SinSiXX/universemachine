#ifndef _CORR_LIB_H_
#define _CORR_LIB_H_

#include <stdint.h>
#include "make_sf_catalog.h"
#include "bin_definitions.h"

void corr_client(char *address, char *port, int64_t chunk_num);
void corr_galaxies(int64_t step, int64_t num_to_process);
void calc_corrs(int64_t step, int64_t num_to_process, struct sf_model c);
void calc_conformity(int64_t step, int64_t num_to_process);
void init_corr_tree(struct catalog_galaxy *g2, int64_t num_g2);
void clear_corrs(void);
void bin_single_galaxy(struct catalog_galaxy *g);
void _calc_conformity(double scale, struct catalog_galaxy *g1, int64_t n1, struct catalog_galaxy *g2, int64_t n2);
void translate_sfrs(struct catalog_galaxy *g, float c);
void calc_density(int64_t step, int64_t num_to_process, struct sf_model c);
void _calc_density(double scale, struct catalog_galaxy *g1, int64_t n1, struct catalog_galaxy *g2, int64_t n2, struct sf_model c);
void calc_corrs_postprocess(int64_t step, int64_t num_to_process, struct sf_model c);
void calc_galaxy_weights(float sm_scatter_const, double sfr_scatter_const, float c, struct catalog_galaxy *g2, int64_t num_to_process, float thresholds[NUM_CORR_THRESHES], int64_t post);
void _calc_galaxy_weight(float sm_scatter_const, float sfr_scatter_const, float c, float thresholds[NUM_CORR_THRESHES], struct catalog_galaxy *g, struct catalog_galaxy_weights *gw, int64_t post);
void bin_galaxies(float scale, struct catalog_galaxy *g, int64_t num_g, struct catalog_galaxy *g2, int64_t num_g2, int64_t step, struct sf_model c,
		  float thresholds[NUM_CORR_THRESHES], int64_t post);

//extern int64_t corr[NUM_BINS], corr_q[NUM_BINS], corr_sf[NUM_BINS], ccorr_sf_q[NUM_BINS];
extern double wcorr[NUM_CORR_BINS_PER_STEP];
extern double ca[NUM_CORR_THRESHES*NUM_CORR_TYPES];
extern int64_t corr[NUM_BINS], corr_q[NUM_BINS], corr_sf[NUM_BINS], ccorr_sf_q[NUM_BINS];

extern int64_t conf_counts[NUM_CONF_BINS];
extern double conf_sfr1[NUM_CONF_BINS],conf_sfr2[NUM_CONF_BINS],conf_sfr11[NUM_CONF_BINS],conf_sfr22[NUM_CONF_BINS],
  conf_sfr12[NUM_CONF_BINS];
extern float corr_dists[NUM_BINS+1];
extern double dens_counts[NUM_DENS_BINS], dens_sf[NUM_DENS_BINS];
extern double dens_sf_ssfr[NUM_DENS_BINS];

#endif /* _CORR_LIB_H_ */

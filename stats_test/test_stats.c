#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <inttypes.h>
#include "../stats.h"
#include "../mt_rand.h"

int main(int argc, char **argv) {
  int64_t i;
  r250_init(87L);
  struct binstats *bs = init_binstats(-10, 10, 0.01, 1, 0);
  int64_t max_i = 100;
  if (argc>1) max_i = atol(argv[1]);
  for (i=0; i<max_i; i++) {
    double x = normal_random(0, 1);
    double y = normal_random(0, 1);
    add_to_binstats(bs, x, y);
  }
  print_medians(bs, stdout);
  return 0;
}

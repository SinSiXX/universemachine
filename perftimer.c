#ifdef TIME_PROFILING
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <inttypes.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include "check_syscalls.h"

#ifdef __APPLE__
#include <mach/mach.h>
#include <mach/mach_time.h>
#else
#include <time.h>
int64_t timer_mach_absolute_time(void) {
  struct timespec t;
  clock_gettime(CLOCK_REALTIME, &t);
  return t.tv_sec*1e9+t.tv_nsec;
}
#endif /*__APPLE__*/

char *timer_role = NULL;

void set_timer_role(char *role, int64_t box, int64_t chunk) { 
  char buffer[1024];
  snprintf(buffer, 1024, "%s.%"PRId64".%"PRId64, role, box, chunk);
  if (timer_role) free(timer_role);
  timer_role = check_strdup(buffer);
}

int64_t timer_t0 = 0;
int64_t timer_last_write_t = 0;
int64_t *timers = NULL;
int64_t *timer_counts = NULL;
char **timer_descs = NULL;
int64_t num_timers = 0;

int64_t add_timers(char *desc, int64_t num) {
  int64_t orig_timers = num_timers;
  char buffer[1024];
  int64_t i;
  if (num <= 0) return -1;
  num_timers += num;
  check_realloc_s(timers, num_timers, sizeof(int64_t));
  check_realloc_s(timer_counts, num_timers, sizeof(int64_t));
  check_realloc_s(timer_descs, num_timers, sizeof(char *));
  memset(timers+orig_timers, 0, sizeof(int64_t)*num);
  memset(timer_counts+orig_timers, 0, sizeof(int64_t)*num);
  memset(timer_descs+orig_timers, 0, sizeof(char *)*num);
  if (num==1) {
    check_realloc_s(timer_descs[orig_timers], sizeof(char), strlen(desc)+1);
    memcpy(timer_descs[orig_timers], desc, sizeof(char)*(strlen(desc)+1));
  }
  else {
    for (i=0; i<num; i++) {
      snprintf(buffer, 1024, "%s %"PRId64, desc, i);
      check_realloc_s(timer_descs[orig_timers+i], sizeof(char), strlen(buffer)+1);
      memcpy(timer_descs[orig_timers+i], buffer, sizeof(char)*(strlen(buffer)+1));
    }
  }
  return orig_timers;
}

int64_t add_timer(char *desc) {
  return add_timers(desc, 1) - 0;
}

void write_timing(int64_t _tnow) {
  char buffer[1024];
  int64_t i;
  if (!timer_last_write_t) mkdir("timing_profiles/", 0755);
  timer_last_write_t = _tnow;
  if (timer_role) {
    snprintf(buffer, 1024, "timing_profiles/%s", timer_role);
    for (i=0; i<strlen(buffer); i++)
      if (buffer[i]==' ') buffer[i] = '_';
  }
  else {
    snprintf(buffer, 1024, "timing_profiles/profile.%d", getpid());
  }
  FILE *out = check_fopen(buffer, "w");
  if (timer_role) fprintf(out, "Role: %s\n", timer_role);
  for (i=0; i<num_timers; i++) {
    double spc = 0;
    if (timer_counts[i]) spc = (double)timers[i]/(double)timer_counts[i]/1e9;
    check_fprintf(out, "%s: %fs (%fs per call, %"PRId64" calls)\n",
		  timer_descs[i], (double)timers[i]/1e9, spc, timer_counts[i]);
  }
  fclose(out);
}

#else /* !TIME_PROFILING */
#endif /* TIME_PROFILING */

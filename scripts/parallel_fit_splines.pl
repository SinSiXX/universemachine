#!/usr/bin/perl -w
use File::Basename;
use Cwd 'abs_path';
BEGIN {
    my $um_directory = abs_path(dirname(__FILE__)."/..");
    push @INC, "$um_directory/perl";
    our $rank_halo_splines = "$um_directory/rank_halo_splines";
    if (! -x $rank_halo_splines) {
	print "Executable rank_halo_splines file not found at expected location ($rank_halo_splines).\n";
	print "Make sure to run \"make treereg\" to generate.\n";
	exit;
    }
}
use MultiThread;

$MultiThread::threadlimit = 16;
$MultiThread::threadlimit = $ARGV[0] if (@ARGV);

opendir DIR, ".";
my @hlists = map { /(\d+\.\d+)/ } grep {/^hlist.*\d+\.list/} readdir DIR;
closedir DIR;

for (@hlists) {
    next unless MultiThread::ForkChild();
    system($rank_halo_splines, $_, "DeltaLogVmax");
    exit;
}
MultiThread::WaitForChildren();


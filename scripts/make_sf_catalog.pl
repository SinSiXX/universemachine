#!/usr/bin/perl -w
use lib qw(perl);
use MultiThread;
use ReadConfig;

unless (@ARGV > 1) {
    die "Usage: $0 config.cfg parameter_file (num_threads) (output_sfh?) (skip?)\n";
}

my $cfg = new ReadConfig($ARGV[0]);
my $nb = $cfg->{NUM_BLOCKS};
die "NUM_BLOCKS not specified in config file $ARGV[0]\n" unless ($nb);
die "Could not open param file $ARGV[1]\n" unless (-r $ARGV[1]);
$MultiThread::threadlimit = $ARGV[2] if ($ARGV[2]);
my $output_sfh = ($ARGV[3]) ? $ARGV[3] : "";
my $min = 0;
my $max = $nb-1;
my $num_threads = $ARGV[2];
if (defined $ARGV[4]) {
    unlink "$cfg->{OUTBASE}/cat_done.$ARGV[4]";
    $min = $ARGV[4]*$num_threads;
    exit if ($min > $max);
    $max = $ARGV[4]*($num_threads+1)-1;
    $max = $nb-1 if ($max > $nb-1);
}


for ($min..$max) {
    next unless MultiThread::ForkChild();
    system("./make_sf_catalog \"$ARGV[0]\" $_ $output_sfh < \"$ARGV[1]\"");
    exit;
}

MultiThread::WaitForChildren();
if (defined $ARGV[4]) {
    system("touch", "$cfg->{OUTBASE}/cat_done.$ARGV[4]");
    if (!$ARGV[4]) {
	my @done;
	my $num_nodes = $nb/$num_threads;
	$num_nodes = int($nb/$num_threads) + (($nb % $num_threads) ? 1 : 0);
	while (@done < $num_nodes) {
	    opendir DIR, "$cfg->{OUTBASE}";
	    @done = map { "$cfg->{OUTBASE}/$_" } grep { /^cat_done\.(\d+)$/ } readdir DIR;
	    closedir DIR;
	    sleep 1;
	}
	unlink @done;
    } else {
	exit;
    }
}

opendir DIR, "$cfg->{OUTBASE}";
@sfr = grep { /^sfr_catalog_\d+\.\d+\.\d+\.bin/ } readdir DIR;
rewinddir DIR;
my @sfh = grep { /^sfh_catalog_\d+\.\d+\.\d+\.txt/ } readdir DIR;
closedir DIR;

join_files(@sfr);
#join_files(@sfh);

sub scale_sort {
    my ($c, $d) = @_;
    my ($sc, $nc) = ($c =~ /(\d+\.\d+)\.(\d+)/);
    my ($sd, $nd) = ($d =~ /(\d+\.\d+)\.(\d+)/);
    return (($sc <=> $sd) || ($nc <=> $nd));
}

sub join_files {
    my @files = @_;
    my %groups = ();
#    my $thrlimit = $MultiThread::threadlimit;
#    $MultiThread::threadlimit = 8;
    for (sort { scale_sort($a, $b) } @files) {
	my $s = (/(\d+\.\d+)/)[0];
	push @{$groups{$s}}, $_;
    }
    for my $s (sort {$a <=> $b } keys %groups) {
	next unless MultiThread::ForkChild();
	my @jf = @{$groups{$s}};
	my $bf = $jf[0];
	$bf =~ s/\.\d+\.(\w+)$/\.$1/;
	$bf = "$cfg->{OUTBASE}/$bf";
	open OUT, ">$bf" or die "Failed to output to $bf!\n";
	my ($c) = 0;
	for my $f (@jf) {
	    open IN, "<", "$cfg->{OUTBASE}/$f";
	    my ($n, $buffer);
	    if ($f =~ /\.txt$/ and $c) {
		#Swallow header
		do {
		    $buffer = <IN>;
		} while ($buffer =~ /^#/);
		print OUT $buffer;
	    }
	    while (($n = read(IN, $buffer, 1000000))) {
		print OUT $buffer;
	    }
	    close IN;
	    $c++;
	}
	close OUT;
	for my $f (@jf) {
	    unlink "$cfg->{OUTBASE}/$f";
	}
	exit;
    }
    MultiThread::WaitForChildren();
#    $MultiThread::threadlimit = $thrlimit;
}

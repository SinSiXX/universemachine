#!/usr/bin/perl -w
BEGIN{
    use File::Spec;
    our $dir = File::Spec->rel2abs( __FILE__ );
    $dir =~ s/UniverseMachine.*?$/UniverseMachine/;
}
use lib "$dir/perl";
use MultiThread;

$MultiThread::threadlimit = 16;
opendir DIR, ".";
my @hlists = map { /(\d+\.\d+)/ } grep {/^hlist.*\d+\.list/} readdir DIR;
closedir DIR;

for (@hlists) {
    next unless MultiThread::ForkChild();
    system("$dir/rank_halo_splines $_"); #DeltaLogVmax
    exit;
}
MultiThread::WaitForChildren();


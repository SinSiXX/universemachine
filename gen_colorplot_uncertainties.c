#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <math.h>
#include <inttypes.h>
#include "check_syscalls.h"
#include "make_sf_catalog.h"
#include "stringparse.h"
#include "universal_constants.h"
#include "stats.h"

struct sf_model_allz *ms = NULL;
int64_t num_ms = 0;
double *med_sfrs=NULL, *av_sfrs=NULL, *fsfs = NULL, *half=NULL, *last_fq;

struct sf_model *cs = NULL;

int sort_double(const void *a, const void *b) {
  const double *c = a;
  const double *d = b;
  if (*c < *d) return -1;
  if (*d < *c) return 1;
  return 0;
}

double doexp10(double x) {
  return exp(M_LN10*x);
}

double sfr_at_vmp(double lv, struct sf_model c) {
  double vd = lv - c.v_1;
  double vd2 = vd/c.delta;
  return (c.epsilon * (1.0/(doexp10(c.alpha*vd) + doexp10(c.beta*vd)) + c.gamma*exp(-0.5*vd2*vd2)));
}

double max_mass(double z) {
  return 13.5351-0.23712*z+2.0187*exp(-z/4.48394); //evolving from 1e-9 @ z=0 to 1e-7 @ z=10
}

double mass_to_vmax (double m, double z);

void calc_models_at_z(double z, double label, FILE *fmed_sfr, FILE *fav_sfr, FILE *ffsf, FILE *fhalf) {
  //double sfr_lim = 0;
  int64_t j,i;
  double a = 1.0/(1.0+z);
  for (i=0; i<num_ms; i++) {
    cs[i] = calc_sf_model(ms[i], a);
    double scatter = sqrt(cs[i].sig_sf*cs[i].sig_sf + cs[i].sig_sf_uncorr*cs[i].sig_sf_uncorr);
    cs[i].obs_sfr_offset = exp(pow((scatter*log(10)),2.0)/2.0); //Scatter correction
  }
  
  memset(last_fq, 0, sizeof(double)*num_ms);
  for (i=0; i<num_ms; i++) { half[i] = 15; }
  
  double m_max = max_mass(z);
  for (j=200; j<=300; j++) {
    double m = (double)j/20.0;
    if (m > m_max) break;
    double vmax = mass_to_vmax(m, z);
    for (i=0; i<num_ms; i++) {
      double sfr2 = sfr_at_vmp(vmax, cs[i]);
      double sfr = log10(sfr2);
      //double sfr3 = sfr;
      //if (sfr < sfr_lim) sfr = sfr_lim;
      med_sfrs[i] = sfr;
      double fq = 0.5+0.5*erf((vmax-cs[i].q_lvmp)/(cs[i].q_sig_lvmp*sqrt(2)));
      fq = cs[i].fq_min + (1.0-cs[i].fq_min)*fq;
      if (fq>=0.5 && last_fq[i] < 0.5) {
	half[i] = m + (0.5-fq)*(1.0/20.0)/(fq-last_fq[i]);
      }
      last_fq[i] = fq;

      double fsf = 1.0 - fq;
      sfr2 *= fsf*cs[i].obs_sfr_offset; //Scatter correction
	
      //double ma_rate = ma_rate_avg_mnow($m, $a);  //Uncertainties will be the same as for SFRs
      //my $sfr_ma = $sfr2 / (0.16*$ma_rate);
      //$sfr_ma = 1e-5 if ($sfr_ma < 1e-5);
      //$sfr_ma = log($sfr_ma)/log(10);
	
      sfr2 = (sfr2>0) ? log10(sfr2) : 0;
      av_sfrs[i] = sfr2;
      fsfs[i] = fsf;
      //if (sfr2 < sfr_lim) sfr2 = sfr_lim;

      //$data2 .= "$label $m $sfr2\n";
      //$data3 .= "$label $m $fsf\n";
      //$data4 .= "$label $m $sfr_ma\n";
    }

#define ONE_S_WIDTH(x) (0.5*(x[(int64_t)(num_ms*0.5*(1.0+ONE_SIGMA))] - x[(int64_t)(num_ms*0.5*(1.0-ONE_SIGMA))]))
    fprintf(fmed_sfr, "%lg %lg %lg ", label, m, med_sfrs[0]);
    qsort(med_sfrs, num_ms, sizeof(double), sort_double);
    fprintf(fmed_sfr, "%lg\n", ONE_S_WIDTH(med_sfrs));
    fprintf(fav_sfr, "%lg %lg %lg ", label, m, av_sfrs[0]);
    qsort(av_sfrs, num_ms, sizeof(double), sort_double);
    fprintf(fav_sfr, "%lg\n", ONE_S_WIDTH(av_sfrs));
    fprintf(ffsf, "%lg %lg %lg ", label, m, fsfs[0]);
    qsort(fsfs, num_ms, sizeof(double), sort_double);
    fprintf(ffsf, "%lg\n", ONE_S_WIDTH(fsfs));
  }
  fprintf(fhalf, "%lg %lg ", label, half[0]);
  double bf = half[0];
  qsort(half, num_ms, sizeof(double), sort_double);
  fprintf(fhalf, "%lg %lg\n", half[(int64_t)(num_ms*0.5*(1.0+ONE_SIGMA))]-bf,
	  bf-half[(int64_t)(num_ms*0.5*(1.0-ONE_SIGMA))]);
}

int main(int argc, char **argv) {
  int64_t i;
  char buffer[2048];
  struct sf_model_allz m = {{0}}, b={{0}};
  enum parsetype types[NUM_PARAMS+1];
  void *data[NUM_PARAMS+1];
  SHORT_PARSETYPE;
  for (i=0; i<NUM_PARAMS+1; i++) {
    types[i] = F64;
    data[i] = &(m.params[i]);
  }
  data[NUM_PARAMS] = &(CHI2(m));
  while (fgets(buffer, 2048, stdin)) {
    if (stringparse(buffer, data, types, NUM_PARAMS+1)<NUM_PARAMS+1) continue;
    check_realloc_every(ms, sizeof(struct sf_model_allz), num_ms, 1000);
    ms[num_ms] = m;
    num_ms++;
    if (!CHI2(b) || CHI2(m) < CHI2(b)) {
      b=m;
      ms[num_ms-1] = ms[0];
      ms[0] = b;
    }
  }

  check_realloc_s(cs, sizeof(struct sf_model), num_ms);
  check_realloc_s(med_sfrs, sizeof(double), num_ms);
  check_realloc_s(av_sfrs, sizeof(double), num_ms);
  check_realloc_s(fsfs, sizeof(double), num_ms);
  check_realloc_s(half, sizeof(double), num_ms);
  check_realloc_s(last_fq, sizeof(double), num_ms);

  FILE *fmed_sfrs = check_fopen("plots/data/med_sfrs.dat", "w");
  FILE *fav_sfrs = check_fopen("plots/data/av_sfrs.dat", "w");
  FILE *ffsfs = check_fopen("plots/data/fsfs.dat", "w");
  FILE *fhalf = check_fopen("plots/data/fhalf.dat", "w");
  fprintf(fmed_sfrs, "#1+z Log10(Mass/Msun) Log10(Median_SFR_for_SF_Halos/(Msun/yr)) One-sigma-uncertainty(dex)\n");
  fprintf(fav_sfrs, "#1+z Log10(Mass/Msun) Log10(Average_SFR_for_All_Halos/(Msun/yr)) One-sigma-uncertainty(dex)\n");
  fprintf(ffsfs, "#1+z Log10(Mass/Msun) Star-Forming-Fraction One-sigma-uncertainty\n");
  fprintf(fhalf, "#1+z Log10(Mass/Msun@50%%Quenched) Err+ Err-\n");

  
  for (i=0; i<300; i++) {
    double l1pz = (double)i / (double)250.0;
    double z = pow(10, l1pz)-1.0;
    calc_models_at_z(z, 1.0+z, fmed_sfrs, fav_sfrs, ffsfs, fhalf);
    printf("Calculated %"PRId64" models at z=%f\n", num_ms, z);
  }

  fclose(fmed_sfrs);
  fclose(fav_sfrs);
  fclose(ffsfs);
  fclose(fhalf);
  return 0;
}


double mass_to_vmax (double m, double z) {
  double a = 1.0/(1.0+z);
  double a200 = a / 0.3782;
  double m200 = log10((1.0/0.68)*1.115e12 / ((pow(a200, -0.142441) + pow(a200, -1.78959))));
  double v200 = log10(200);
  double exp_vp = (m - m200)/3.0 + v200;
  return exp_vp;
}

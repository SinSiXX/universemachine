#ifndef _LIGHTCONE_H_
#define _LIGHTCONE_H_
#include "lightcone_cache.h"

#define COL_SIZE 150

struct lightcone_data {
  struct halo_cat *cat;
  float origin[3];
  float inv_rot[3][3];
  float ytan, ztan;
  float d2min, d2max;
  FILE *output;
};

void lightcone_cb(int x, int y, int z, void *extra_data);
void test_lightcone_overlap_cb(int x, int y, int z, void *extra_data);

void collision_cb(int x, int y, int z, void *extra_data);
void clear_collision_test(void);

#endif /* _LIGHTCONE_H_ */

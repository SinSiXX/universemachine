#ifndef _MUZZIN_UVJ_H_
#define _MUZZIN_UVJ_H_
#include <inttypes.h>


int muzzin_uvj_quenched(float uv, float vj, float a);
void gen_uvj_lookups(void);
float uvj_qf_interp(float uv, float vj, float a);

double fq_monte_carlo(double uv, double vj, double a);
double integrate_muzzin_uvj(double uv, double vj, double a);

struct muzzin_helper {
  double uv, vj, a;
};

struct uvj_lookup {
  float *lookup;
  float uv[2], vj[2];
  int64_t uv_bins, vj_bins;
};

#define UVJ_UV_MAX 2.8
#define UVJ_UV_MIN 0
#define UVJ_VJ_MAX 2.8
#define UVJ_VJ_MIN 0
#define UVJ_BPUNIT 20
#define UVJ_TOL 1e-6
#define UVJ_LINEAR 1

#endif /* _MUZZIN_UVJ_H_ */

#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include "make_sf_catalog.h"
#include "check_syscalls.h"
#include "stringparse.h"

struct particle {
  float pos[2];
};

#define FAST3TREE_DIM 2
#define FAST3TREE_TYPE struct particle
#include "fast3tree.c"

struct particle *p = NULL;
int64_t num_p = 0;


#define WL_BPDEX 8
#define WL_MIN_R -2
//#define NUM_WL_BINS 23

double bin_edges[NUM_WL_BINS] = {0};

void calc_bin_edges(void) {
  for (int64_t i=0; i<NUM_WL_BINS; i++)
    bin_edges[i] = pow(10, 2.0*(WL_MIN_R + (double)(i)/(double)WL_BPDEX)); //bin_edges hold r^2
}

int64_t bin_of(double r2) {
  int64_t i;
  for (i=NUM_WL_BINS-1; i>=0; i--)
    if (bin_edges[i] < r2) break;
  return i;
}


int main(int argc, char **argv) {
  if (argc < 4) {
    fprintf(stderr, "Usage: %s box_size particles acc_list\n", argv[0]);
    exit(1);
  }

  float box_size = atof(argv[1]);
  char buffer[1024];
  FILE *in = check_fopen(argv[2], "r");
  struct particle tp;
  void *data[2] = {tp.pos, tp.pos+1};
  enum parsetype pt[2] = {PARSE_FLOAT32, PARSE_FLOAT32};
  while (fgets(buffer, 1024, in)) {
    if (stringparse(buffer, data, pt, 2)!=2) continue;
    check_realloc_every(p, sizeof(struct particle), num_p, 1000);
    p[num_p] = tp;
    num_p++;
  }
  fclose(in);

  calc_bin_edges();

  struct fast3tree *tree = fast3tree_init(num_p, p);
  struct fast3tree_results *res = fast3tree_results_init();
  _fast3tree_set_minmax(tree, 0, box_size);

  int64_t i, length = 0;
  struct catalog_halo *ph = check_mmap_file(argv[3], 'r', &length);
  mlock(ph, length);
  assert(!(length % sizeof(struct catalog_halo)));
  int64_t num_halos = length / sizeof(struct catalog_halo);

  printf("#ID X Y Z Particle Counts\n#Bin starts: 0");
  for (i=0; i<NUM_WL_BINS-1; i++)
    printf(" %f", pow(10, WL_MIN_R+i/(double)WL_BPDEX));
  printf("\n#Bin ends:");
  for (i=0; i<NUM_WL_BINS; i++)
    printf(" %f", pow(10, WL_MIN_R+i/(double)WL_BPDEX));
  printf("\n");
  printf("#All units: comoving Mpc/h\n");

  for (i=0; i<num_halos; i++) {
    fast3tree_find_sphere_periodic(tree, res, ph[i].pos, pow(10, WL_MIN_R+(NUM_WL_BINS-1)/(double)WL_BPDEX));
    int64_t j;
    int64_t bins[NUM_WL_BINS] = {0};
    printf("%"PRId64" %f %f %f", ph[i].id, ph[i].pos[0], ph[i].pos[1], ph[i].pos[2]);
    int64_t k=0;
    for (j=0; j<res->num_points; j++) {
      double dx, ds=0;
      for (k=0; k<2; k++) {
	dx = fabs(ph[i].pos[k] - res->points[j]->pos[k]);
	if (dx > box_size/2.0) dx = box_size - dx;
	ds += dx*dx;
      }
      int64_t bin = bin_of(ds);
      if ((bin < 0)||(bin >= NUM_WL_BINS)) continue;
      assert((bin >=0) && (bin < NUM_WL_BINS));
      bins[bin]++;
    }
    for (k=0; k<NUM_WL_BINS; k++)
      printf(" %"PRId64, bins[k]);
    printf("\n");
  }
  munmap(ph, length);
  return 0;
}

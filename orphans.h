#ifndef _ORPHANS_H_
#define _ORPHANS_H_

#define NUM_STEPS 10
#define SOFTENING_LENGTH 0.1

#include <inttypes.h>

#ifndef EXTRA_HALO_INFO
#define EXTRA_HALO_INFO struct halo *vpeak_halo, *mpeak_halo, *acc_halo, *first_acc_halo; float tidal_av, delta_log_vmax; int64_t orphan_count, orphan_id;
#endif /* EXTRA_HALO_INFO */

#include "read_tree/read_tree.h"
void evolve_orphan_halo(struct halo *h, struct halo *p1, struct halo *p2);

#define ORPHAN_ID_OFFSET ((int64_t)1000000000000000)
#define ORPHAN_VMAX_RATIO_LIMIT 0.2
#define ORPHAN_VMAX_LIMIT 80.0

#endif /* _ORPHANS_H_ */

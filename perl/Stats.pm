package BinStats;
use Carp;

our $ONE_SIGMA = 0.341344746;
our $LOG10 = log(10);

sub new {
    my ($self, $bin_start, $bin_end, $bpunit, $fullstats, $log) = @_;
    my $stats = {};
    $stats->{bin_start} = $bin_start;
    $stats->{bin_end} = $bin_end;
    $stats->{bpunit} = $bpunit;
    $stats->{num_bins} = int(($bin_end-$bin_start)*$bpunit + 1.0000001);
    $stats->{fullstats} = $fullstats;
    $stats->{logarithmic} = $log;
    $stats->{log10} = log(10);
    bless $stats, $self;
    $stats->clear();
    return $stats;
}

sub clear {
    my $bs = shift;
    for (qw/keyavg avg m2 sd counts/) {
	$bs->{$_} = [map { 0 } (0..$bs->{num_bins})];
    }

    if ($bs->{fullstats}) {
	for (qw/med stdup stddn avgsd/) {
	    $bs->{$_} = [map { 0 } (0..$bs->{num_bins})];
	}
	$bs->{bvals} = [map { [] } (0..$bs->{num_bins})];
    }
}

sub set_bin_names {
    my $bs = shift;
    $bs->{names} = (ref($_[0])) ? [@{$_[0]}] : [@_];
}

sub add {
    my ($bs, $key, $val) = @_;
    if ($bs->{logarithmic}) {
	if ($key <= 0) {$key = -1000; }
	else { $key = log($key)/$LOG10; }
    }
	
    my $bin = ($key-$bs->{bin_start})*$bs->{bpunit};
    if ($bin < 0 || $bin > $bs->{num_bins}) { $bin = $bs->{num_bins}; }
    $bs->{counts}[$bin]++;
    $bs->{keyavg}[$bin] += ($key - $bs->{keyavg}[$bin])/$bs->{counts}[$bin];
    my $delta = $val - $bs->{avg}[$bin];
    $bs->{avg}[$bin] += $delta / $bs->{counts}[$bin];
    $bs->{m2}[$bin] += $delta*($val - $bs->{avg}[$bin]);
    $bs->{sd}[$bin] = $bs->{m2}[$bin] / $bs->{counts}[$bin];
    
    if ($bs->{fullstats}) {
	push @{$bs->{bvals}[$bin]}, $val;
    }
} 

sub calc_jackknife_errs {
    my $bs = shift;
    if (!$bs->{fullstats}) {
	croak "Binstats structure needs to be set as fullstats before calculating jackknife errors!";
    }
    
    for (my $i=0; $i<$bs->{num_bins}; $i++) {
	if (!$bs->{counts}[$i]) {
	    $bs->{avgsd}[$i] = 0;
	}
	elsif ($bs->{counts}[$i] < 2) {
	    $bs->{avgsd}[$i] = $bs->{bvals}[$i][0];
	}
	else {
	    my $sd = 0;
	    for (my $j=0; $j<$bs->{counts}[$i]; $j++) {
		my $mj = ($bs->{avg}[$i] * $bs->{counts}[$i] - $bs->{bvals}[$i][$j])/($bs->{counts}[$i]-1);
		my $diff = $mj - $bs->{avg}[$i];
		$sd += $diff*$diff;
	    }
	    $sd = ($bs->{counts}[$i]-1)*$sd / (($bs->{counts}[$i]));
	    $bs->{avgsd}[$i] = sqrt($sd);
	}
    }
}

sub calc_median {
    my $bs = shift;

    if (!$bs->{fullstats}) {
	croak "Binstats structure needs to be set as fullstats before calculating median!";
    }

    for (my $i=0; $i<$bs->{num_bins}; $i++) {
	if (!$bs->{counts}[$i]) {
	    $bs->{med}[$i] = $bs->{stdup}[$i] = $bs->{stddn}[$i] = 0;
	}
	else {
	    @{$bs->{bvals}[$i]} = sort { $a <=> $b } @{$bs->{bvals}[$i]};
	    $bs->{med}[$i] = $bs->{bvals}[$i][$bs->{counts}[$i]*0.5];
	    $bs->{stdup}[$i] = $bs->{bvals}[$i][$bs->{counts}[$i]*(0.5+$ONE_SIGMA)] - $bs->{med}[$i];
	    $bs->{stddn}[$i] = $bs->{med}[$i] - $bs->{bvals}[$i][$bs->{counts}[$i]*(0.5-$ONE_SIGMA)];
	}
    }
    $bs->calc_jackknife_errs();
}

sub print_avgs {
    my $bs = shift;
    my $output = (shift()) || \*STDOUT;
    for (my $i=0; $i<$bs->{num_bins}; $i++) {
	next unless ($bs->{counts}[$i]);
	my $key = $bs->{keyavg}[$i];
	if ($bs->{logarithmic}) { $key = 10**$key; }
	if ($bs->{names} && $bs->{names}[$i]) {
	    print $output ($bs->{names}[$i]." ");
	} else {
	    print $output $key." ";
	}
	print $output ("$bs->{avg}[$i] ".sqrt($bs->{sd}[$i]). " #$bs->{counts}[$i]\n");
    }
}

sub print_medians {
    my $bs = shift;
    my $output = (shift()) || \*STDOUT;
    $bs->calc_medians();
    for (my $i=0; $i<$bs->{num_bins}; $i++) {
	next unless ($bs->{counts}[$i]);
	my $key = $bs->{keyavg}[$i];
	if ($bs->{logarithmic}) { $key = 10**$key; }
	if ($bs->{names} && $bs->{names}[$i]) {
	    print $output ($bs->{names}[$i]." ");
	} else {
	    print $output ($key." ");
	}

	printf $output ("%g %g %g #avg: %g; avg_err: %g; sd: %g; counts: %g\n",
			$bs->{med}[$i], $bs->{stdup}[$i], $bs->{stddn}[$i], $bs->{avg}[$i], 
			$bs->{avgsd}[$i], sqrt($bs->{sd}[$i]), $bs->{counts}[$i]);
    }
}

sub print_keyname {
    my $bs = shift;
    my $bin = shift;
    my $output = (shift()) || \*STDOUT;
    my $key = $bs->{keyavg}[$bin];
    if ($bs->{logarithmic}) { $key = 10**$key; }
    if ($bs->{names} && $bs->{names}[$i]) {
	print $output ($bs->{names}[$i]);
    } else {
	print $output ($key);
    }
}

sub print_5stats {
    my $bs = shift;
    my $bin = shift;
    my $output = (shift()) || \*STDOUT;
    printf $output (" %g %g %g %g %g",
		    $bs->{avg}[$bin], $bs->{avgsd}[$bin], $bs->{med}[$bin], 
		    $bs->{stdup}[$bin], $bs->{stddn}[$bin]);
}


1;

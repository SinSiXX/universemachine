#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include "sf_model.h"
#include "make_sf_catalog.h"
#include "check_syscalls.h"
#include "stringparse.h"

struct particle {
  float pos[2];
};

#define FAST3TREE_DIM 2
#define FAST3TREE_TYPE struct particle
#include "fast3tree.c"

struct particle *p = NULL;
int64_t num_p = 0;


double bin_edges[NUM_WL_BINS] = {0};

void calc_bin_edges(void) {
  for (int64_t i=0; i<NUM_WL_BINS; i++)
    bin_edges[i] = pow(10, 2.0*(WL_MIN_R + (double)(i)/(double)WL_BPDEX)); //bin_edges hold r^2
}

int64_t bin_of(double r2) {
  int64_t i;
  for (i=NUM_WL_BINS-1; i>=0; i--)
    if (bin_edges[i] < r2) break;
  return i+1;
}


int main(int argc, char **argv) {
  if (argc < 6) {
    fprintf(stderr, "Usage: %s box_size particles scale halo_catalog_dir num_blocks\n", argv[0]);
    exit(1);
  }

  int64_t i, j, n, snap;
  int64_t num_scales=0, offset, *offsets = NULL;
  float scale, *scales = NULL;
  
  float box_size = atof(argv[1]);
  char buffer[1024];
  FILE *in = check_fopen(argv[2], "r");
  struct particle tp;
  void *data[2] = {tp.pos, tp.pos+1};
  enum parsetype pt[2] = {PARSE_FLOAT32, PARSE_FLOAT32};
  while (fgets(buffer, 1024, in)) {
    if (stringparse(buffer, data, pt, 2)!=2) continue;
    check_realloc_every(p, sizeof(struct particle), num_p, 1000);
    p[num_p] = tp;
    num_p++;
  }
  fclose(in);
  calc_bin_edges();


  double a = atof(argv[3]);
  char *INBASE = argv[4];
  int64_t num_blocks = atol(argv[5]);

  struct fast3tree *tree = fast3tree_init(num_p, p);
  struct fast3tree_results *res = fast3tree_results_init();
  _fast3tree_set_minmax(tree, 0, box_size);

  
  snprintf(buffer, 1024, "%s/shear_catalog_%f.dat", INBASE, a);
  FILE *out = check_fopen(buffer, "w");

  fprintf(out, "#ID X Y Z Particle Counts\n#Bin starts: 0");
  for (i=0; i<NUM_WL_BINS-1; i++)
    fprintf(out, " %f", pow(10, WL_MIN_R+i/(double)WL_BPDEX));
  fprintf(out, "\n#Bin ends:");
  for (i=0; i<NUM_WL_BINS; i++)
    fprintf(out, " %f", pow(10, WL_MIN_R+i/(double)WL_BPDEX));
  fprintf(out, "\n");
  fprintf(out, "#All units: comoving Mpc/h\n");

  
  for (i=0; i<num_blocks; i++) {
    snprintf(buffer, 1024, "%s/offsets.box%"PRId64".txt", INBASE, i);
    FILE *in = check_fopen(buffer, "r");
    while (fgets(buffer, 1024, in)) {
      if (buffer[0] == '#') {
	if (!strncmp(buffer, "#Total halos: ", 14)) {
	  offset = atol(buffer+14);
	  check_realloc_every(offsets, sizeof(int64_t), num_scales, 100);
	  offsets[num_scales] = offset;
	}
	continue;
      }
      n = sscanf(buffer, "%"SCNd64" %f %"SCNd64, &snap, &scale, &offset);
      if (n!=3) continue;
      check_realloc_every(offsets, sizeof(int64_t), num_scales, 100);
      check_realloc_every(scales, sizeof(float), num_scales, 100);
      scales[num_scales] = scale;
      offsets[num_scales] = offset;
      num_scales++;
    }
    fclose(in);
    
    for (j=0; j<num_scales; j++)
      if (fabs(scales[j]-a) < 1e-5) break;
    
    if (j==num_scales) {
      fprintf(stderr, "[Error] Could not find scale %f with tolerance 1e-5.\n", a);
      exit(EXIT_FAILURE);
    }
    
    int64_t length = 0, k;
    snprintf(buffer, 1024, "%s/cat.box%"PRId64".dat", INBASE, i);
    struct catalog_halo *ch = check_mmap_file(buffer, 'r', &length);

    for (k=offsets[j]; k<offsets[j+1]; k++) {
      fast3tree_find_sphere_periodic(tree, res, ch[k].pos, pow(10, WL_MIN_R+(NUM_WL_BINS-1)/(double)WL_BPDEX));
      int64_t l, m;
      int64_t bins[NUM_WL_BINS] = {0};
      fprintf(out, "%"PRId64" %f %f %f", ch[k].id, ch[k].pos[0], ch[k].pos[1], ch[k].pos[2]);
      for (l=0; l<res->num_points; l++) {
	double dx, ds=0;
	for (m=0; m<2; m++) {
	  dx = fabs(ch[k].pos[m] - res->points[l]->pos[m]);
	  if (dx > box_size/2.0) dx = box_size - dx;
	  ds += dx*dx;
	}
	int64_t bin = bin_of(ds);
	if ((bin < 0)||(bin >= NUM_WL_BINS)) continue;
	assert((bin >=0) && (bin < NUM_WL_BINS));
	bins[bin]++;
      }
      for (l=0; l<NUM_WL_BINS; l++)
	fprintf(out, " %"PRId64, bins[l]);
      fprintf(out, "\n");
    }
    munmap(ch, length);
    num_scales = 0;
  }
  fclose(out);
  return 0;
}

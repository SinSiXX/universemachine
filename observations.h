#ifndef OBSERVATIONS_H
#define OBSERVATIONS_H
#include <inttypes.h>
#include "sf_model.h"

#define NUM_TYPES          16
#define SMF_TYPE           0
#define UVLF_TYPE          1
#define QF_TYPE            2  //Moustakas quenched threshold
#define QF11_TYPE          3  //Threshold of 1e-11 for quenched
#define QF_UVJ_TYPE        4  //Muzzin UVJ quenched threshold
#define SSFR_TYPE          5
#define CSFR_TYPE          6
#define CSFR_UV_TYPE       7
#define CORR_TYPE          8
#define XCORR_TYPE         9
#define CONF_TYPE          10
#define WL_TYPE            11
#define CDENS_FSF_TYPE     12
#define CDENS_SSFR_SF_TYPE 13
#define UVSM_TYPE          14
#define IRX_TYPE           15

struct observation {
  int64_t step1, step2, smb1, smb2, r1, r2;
  double val, err_l, err_h, z_low, z_high, sm_low, sm_high, sm_low2, sm_high2;
  double r_low, r_high, dsm;
  double model_val, chi, chi2, orig_err_h, orig_err_l, weight;
  int64_t linear_errors, type, file_id, covariance_id, covariance_idx, model_counts;
  char subtype;
};

struct pca_errors {
  int64_t covariance_id, obs_start, num_obs, num_pcas;
  //First element of pca array is Std. deviation
  double *pcas;
};

void load_observations(struct observation **obs, int64_t *num_obs);
double calc_chi2(struct observation *obs, int64_t num_obs, struct sf_model_allz m);
int64_t sm_to_bin(double sm);

extern char *obs_data_types[NUM_TYPES];

#endif /* OBSERVATIONS_H */

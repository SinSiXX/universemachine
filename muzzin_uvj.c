#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <inttypes.h>
#include "integrate.h"
#include "mt_rand.h"
#include "check_syscalls.h"
#include "config_vars.h"
#include "muzzin_uvj.h"

int muzzin_uvj_quenched(float uv, float vj, float a) {
  if (uv < 1.3 || vj > 1.5) return 0;
  if (a > 0.5) {
    if (uv<(vj*0.88+0.69)) return 0;
  } else if (uv<(vj*0.88+0.59)) return 0;
  return 1;
}

double muzzin_vj_limit(double uv, float a) {
  double max=0;
  if (a > 0.5)
    max = ((uv-0.69)/0.88);
  else
    max = ((uv-0.59)/0.88);
  if (max > 1.5) return 1.5;
  return max;  
}


double fq_monte_carlo(double uv, double vj, double a) {
  int64_t i;
  double sum=0;
  double trials = 10000;
  for (i=0; i<trials; i++) {
    double du = normal_random(0,UVJ_SCATTER);
    double dv = normal_random(0,UVJ_SCATTER);
    double dj = normal_random(0,UVJ_SCATTER);
    double nuv = uv + du - dv;
    double nvj = vj + dv - dj;
    sum += muzzin_uvj_quenched(nuv,nvj,a);
  }
  return sum/trials;
}

double _integrate_muzzin_uvj(double uv, void *extra_data) {
  struct muzzin_helper *mh = extra_data;
  double vj_limit = muzzin_vj_limit(uv,mh->a);
  double duv = (uv-mh->uv)/(M_SQRT2*UVJ_SCATTER);
  double uv_prob = exp(-0.5*(duv*duv));
  double vj_cen = mh->vj-(uv-mh->uv)/3.0;
  double dvj = (vj_limit-vj_cen)/(2.0*UVJ_SCATTER); //Includes factor of sqrt(2) for erf()
  double vj_prob = 0.5+0.5*erf(dvj);
  return vj_prob*uv_prob;
}

double integrate_muzzin_uvj(double uv, double vj, double a) {
  struct muzzin_helper mh;
  mh.uv=uv;
  mh.vj=vj;
  mh.a=a;
  double uv_min = 1.3; //Limit from Muzzin+ 2013.
  double uv_max = (uv>uv_min) ? uv : uv_min;
  uv_max += UVJ_SCATTER*5.0*M_SQRT2; //Go 5-sigma up
  double result = adaptiveSimpsons(_integrate_muzzin_uvj, &mh, uv_min, uv_max, 1e-9, 15);
  return result/(UVJ_SCATTER*2.0*sqrt(M_PI));
}

struct uvj_lookup uvj_lookup[2];

void gen_uvj_lookups(void) {
  int64_t i, j, k;
  for (i=0; i<2; i++) {
    double a = 0.2+0.5*i; //One lookup for low z, one for high z
    struct uvj_lookup *u = uvj_lookup+i;
    double uv_max = UVJ_UV_MAX;
    for (; uv_max>UVJ_UV_MIN; uv_max -= 0.001)
      if (fabs(integrate_muzzin_uvj(uv_max, 1.5, a)-0.5)>UVJ_TOL/2.0) break;

    double vj_max = UVJ_VJ_MAX;
    for (; vj_max > UVJ_VJ_MIN; vj_max -= 0.001)
      if (integrate_muzzin_uvj(uv_max, vj_max, a)>UVJ_TOL/2.0) break;

    double vj_min = 0;
    double uv_min = 0;
    for (; uv_min < uv_max; uv_min += 0.001)
      if (integrate_muzzin_uvj(uv_min, vj_min, a)>UVJ_TOL/2.0) break;

    //fprintf(stderr, "%g %g %g %g\n", uv_min, uv_max, vj_min, vj_max);
    u->uv[0] = (int)(uv_min*UVJ_BPUNIT);
    u->uv[1] = (int)(uv_max*UVJ_BPUNIT+1);
    u->vj[0] = (int)(vj_min*UVJ_BPUNIT);
    u->vj[1] = (int)(vj_max*UVJ_BPUNIT+1);
    u->uv_bins = u->uv[1] - u->uv[0] + 1;
    u->vj_bins = u->vj[1] - u->vj[0] + 1;
    for (j=0; j<2; j++) {
      u->uv[j] /= (double) UVJ_BPUNIT;
      u->vj[j] /= (double) UVJ_BPUNIT;
    }
    u->lookup = NULL;
    check_realloc_s(u->lookup, sizeof(float), u->uv_bins*u->vj_bins);

    for (j=0; j<u->uv_bins; j++) {
      for (k=0; k<u->vj_bins; k++) {
	int64_t bin = j*u->vj_bins + k;
	double uv = u->uv[0] + (double)j/(double)UVJ_BPUNIT;
	double vj = u->vj[0] + (double)k/(double)UVJ_BPUNIT;
	u->lookup[bin] = integrate_muzzin_uvj(uv, vj, a);
      }
    }
  }
}


float parabola_fit(float y1, float y2, float y3, float xv) {
  float a = 0.5*(y1+y3)-y2;
  if (fabs(a)<1e-4) return y2+(xv*(y3-y2));
  float x0 = (y1-y3)/(4.0*a);
  xv -= x0;
  return (y2 + a*(xv*xv-x0*x0));
}

float uvj_qf_interp(float uv, float vj, float a) {
  struct uvj_lookup *u = uvj_lookup;
  if (a>=0.5) u++;
  if (vj>u->vj[1]) return 0; //Star-forming
  if (uv<u->uv[0]) return 0; //Star-forming
  if (vj<u->vj[0]) vj=u->vj[0]; //Approximate solution
  if (uv>u->uv[1]) uv=u->uv[1]; //Approximate solution
  float fuv = (uv-u->uv[0])*(float)UVJ_BPUNIT;
  float fvj = (vj-u->vj[0])*(float)UVJ_BPUNIT;
  int buv = fuv;
  int bvj = fvj;
  fuv -= buv;
  fvj -= bvj;
#if UVJ_LINEAR
    buv *= u->vj_bins;
    float v1 = u->lookup[buv+bvj] + fvj*(u->lookup[buv+bvj+1]-u->lookup[buv+bvj]);
    buv += u->vj_bins;
    float v2 = u->lookup[buv+bvj] + fvj*(u->lookup[buv+bvj+1]-u->lookup[buv+bvj]);
    return (v1 + fuv*(v2-v1));
#else 
    if (buv==0) { buv++; fuv-=1; }
    if (bvj==0) { bvj++; fvj-=1; }
    buv--;
    buv *= u->vj_bins;
    float v0 = parabola_fit(u->lookup[buv+bvj-1], u->lookup[buv+bvj], u->lookup[buv+bvj+1], fvj);
    buv += u->vj_bins;
    float v1 = parabola_fit(u->lookup[buv+bvj-1], u->lookup[buv+bvj], u->lookup[buv+bvj+1], fvj);
    buv += u->vj_bins;
    float v2 = parabola_fit(u->lookup[buv+bvj-1], u->lookup[buv+bvj], u->lookup[buv+bvj+1], fvj);
    return parabola_fit(v0, v1, v2, fuv);
#endif /*UVJ_LINEAR*/
}


#ifdef UVJ_MAIN
int main(void) {
  double uv, vj;
  r250_init(87L);
  gen_uvj_lookups();
  double max_z0_err = 0;
  double max_z2_err = 0;
  for (uv=0; uv<2.5; uv+=0.01) {
    for (vj=0; vj<2.5; vj+=0.01) {
      double mc_z0 = uvj_qf_interp(uv, vj, 1); //fq_monte_carlo(uv,vj,1);
      double mc_z2 = uvj_qf_interp(uv, vj, 0.33); //fq_monte_carlo(uv,vj,0.33);
      double test_z0 = integrate_muzzin_uvj(uv, vj, 1);
      double test_z2 = integrate_muzzin_uvj(uv, vj, 0.33);
      if (fabs(mc_z0-test_z0)>max_z0_err)
	max_z0_err = fabs(mc_z0-test_z0);
      if (fabs(mc_z2-test_z2)>max_z2_err)
	max_z2_err = fabs(mc_z2-test_z2);
      printf("%g %g %g %g %g %g %g %g\n", vj, uv, mc_z0, test_z0, fabs(mc_z0-test_z0), mc_z2, test_z2, fabs(mc_z2-test_z2));
    }
  }
  fprintf(stderr, "Max z0 err: %g (%g bins)\n", max_z0_err, (double)(uvj_lookup[1].uv_bins*uvj_lookup[1].vj_bins));
  fprintf(stderr, "Max z2 err: %g (%g bins)\n", max_z2_err, (double)(uvj_lookup[0].uv_bins*uvj_lookup[0].vj_bins));
  return 0;
}
#endif /* UVJ_MAIN */

#ifndef _MATRICES_H_
#define _MATRICES_H_

void mult_matrix(float in1[3][3], float in2[3][3], float out[3][3]);
void mult_vector(float mat[3][3], float in[3], float out[3]);
void copy_matrix(float in[3][3], float out[3][3]);
void inv_rot_matrix(float in[3][3], float out[3][3]);
void rotate3(float p, float a, float theta, float rot[3][3]);
void rand_rotation(float rot[3][3]);
int lightcone_test(float v[3], float inv_rot[3][3], 
		   float ytan, float ztan, float d2min, float d2max);

#endif /* _MATRICES_H_ */


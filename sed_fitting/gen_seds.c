#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <inttypes.h>
#include "../check_syscalls.h"
#include "../universe_time.h"
#include "../all_luminosities.h"
#include "../sampler.h"
#include "../mt_rand.h"
#include "../smloss.h"

#define NUM_FILTERS 13
#define NUM_SCALES 50
#define NUM_WALKERS 128
#define TOTAL_STEPS 100000
#define CONST_TIMESCALE 2e7

float scales[NUM_SCALES+1] = {0};
float stimes[NUM_SCALES+1] = {0};
float remn[NUM_SCALES+1] = {0};
float sfh[NUM_SCALES+1] = {0};
float dt[NUM_SCALES+1] = {0};


struct lum_point {
  int filter;
  double mag, err_u, err_d;
};

struct lum_point *lps = NULL;
int64_t nlps = 0;


#include "sed_common.c"


int main(int argc, char **argv) {
  enum lum_types lt[NUM_FILTERS] = {L_m1500, L_f435w, L_f606w, L_f775w, L_f814w, L_f850lp, L_f098m, L_f105w, L_f125w, L_f140w, L_f160w, L_irac1, L_irac2};
  if (argc < 2) {
    fprintf(stderr, "Usage: %s z < mcmc_fit.dat\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  
  int64_t i;
  double z = atof(argv[1]);
  //double max_age = atof(argv[5])*1e9;
  double max_age = 1e11;
  
  init_time_table(0.307, 0.6777);
  scales[NUM_SCALES] = 1.0/(1.0+z);
  double t0 = scale_to_years(scales[NUM_SCALES])-scale_to_years(0);
  stimes[NUM_SCALES] = t0;
  if (max_age >= t0) scales[0] = 0;
  else scales[0] = years_to_scale(scale_to_years(scales[NUM_SCALES]) - max_age);
  stimes[0] = scale_to_years(scales[0]) - scale_to_years(0);
  stimes[NUM_SCALES] = t0;
  for (i=1; i<NUM_SCALES; i++) {
    double dtmin = 1e6;
    double dtmax = t0 - stimes[0];
    double t = scale_to_years(scales[NUM_SCALES]) - pow(dtmin/dtmax, (double)(i)/(double)(NUM_SCALES-1))*dtmax;
    scales[i] = years_to_scale(t);
    stimes[i] = scale_to_years(scales[i]) - scale_to_years(0);
  }

  for (i=0; i<NUM_SCALES+1; i++) {
    float a1 = (i>0) ? 0.5*(scales[i]+scales[i-1]) : scales[0];
    float a2 = (i<NUM_SCALES) ? 0.5*(scales[i]+scales[i+1]) : scales[i];
    dt[i] = scale_to_years(a2) - scale_to_years(a1);
    remn[i] = calc_sm_loss_int(a1, a2, scales[NUM_SCALES]);
  }


  float *lum_tables[NUM_FILTERS] = {0};
  load_luminosities("../fsps");
  for (i=0; i<NUM_FILTERS; i++)
    gen_lum_lookup(&(lum_tables[i]), scales, NUM_SCALES, lt[i], (i==0) ? 0 : z);

  
  char buffer[1024];
  float lums[NUM_FILTERS] = {0};
  //  float lums2[NUM_FILTERS] = {0};
  int64_t printed_header = 0;
  while (fgets(buffer, 1024, stdin)) {
    if (buffer[0] == '#') {
      if (buffer[1] != 'T' || buffer[2] != 'o') printf("%s", buffer);
      continue;
    }
    else if (!printed_header) {
      printf("#Total_SM Plaw Dust Dust2 Burst_F Burst_T Z_Offset Chi2 Rem_SM A_UV");
      for (i=0; i<NUM_FILTERS; i++)
	printf(" %s", lum_names[lt[i]]);
      printf("\n");
      printed_header = 1;
    }
    double total_sm, plaw, dust, dust2, burst_f, burst_t, Z_offset, chi2;
    if (sscanf(buffer, "%lf %lf %lf %lf %lf %lf %lf %lf", &total_sm, &plaw, &dust, &dust2, &burst_f, &burst_t, &Z_offset, &chi2)!=8) continue;
    if (burst_f==0 || burst_t == 0)
      gen_sfh(total_sm, plaw);
    else
      gen_sfh_two_c(total_sm, plaw, burst_f, burst_t);
    double rem_sm = 0;
    float auv = 0;
    for (i=0; i<NUM_SCALES+1; i++) rem_sm += sfh[i]*remn[i];
    lums_of_sfh(lum_tables, sfh, scales, NUM_SCALES, dust, dust2, burst_t, Z_offset, lums, &auv);
    //lums_of_sfh(lum_tables, sfh, scales, NUM_SCALES, 0, dust2, lums2);
    printf("%f %f %f %f %f %f %e %f %f %f", total_sm, plaw, dust, dust2, burst_f, burst_t, Z_offset, chi2, log10(rem_sm), auv);
    for (i=0; i<NUM_FILTERS; i++) printf(" %f", lums[i]);
    printf("\n");
  }

  

  return 0;
}

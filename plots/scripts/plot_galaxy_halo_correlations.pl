#!/usr/bin/perl -w
use lib qw(../perl ../../perl perl);
use XmGrace;
use Common;



new XmGrace(xlabel => $hm_label,
	    ylabel => "\\h{3.5}Rank Correlation of\\nGalaxy SFR to Halo Assembly Rate",
	    xmin => 1e10, xmax => 1e15,
	    XmGrace::linscale('y', 0, 1, 0.2, 1),
	    color_errorstrip => 1,
	    transparent_colors => 1,
	    data => [{data => "file:data/galaxy_halo_correlations/corr_z0.dat", metatype => "errorstrip", transform => XmGrace::tolin(0)},
		     {data => "file:data/galaxy_halo_correlations/corr_z1.dat", metatype => "errorstrip", transform => XmGrace::tolin(0)},
		     {data => "file:data/galaxy_halo_correlations/corr_z0.dat", transform => XmGrace::tolin(0), legend => "z = 0"},
		     {data => "file:data/galaxy_halo_correlations/corr_z1.dat", transform => XmGrace::tolin(0), legend => "z = 1"},

])->write_pdf("AGR/galaxy_halo_correlations.agr");


new XmGrace(xlabel => $hm_label,
	    ylabel => 'Satellite Disruption Threshold (v\smax\N / v\sMpeak\N)',
	    xmin => 1e10, xmax => 1e15,
	    XmGrace::linscale('y', 0.4, 0.8, 0.1, 1),
	    color_errorstrip => 1,
	    transparent_colors => 1,
	    data => [{data => "file:data/orphan_thresh.dat", metatype => "errorstrip", transform => XmGrace::tolin(0)},
		     {data => "file:data/orphan_thresh.dat", transform => XmGrace::tolin(0)},

])->write_pdf("AGR/orphan_thresh.agr");

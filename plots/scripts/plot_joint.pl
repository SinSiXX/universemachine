#!/usr/bin/perl -w
use lib qw(../perl ../../perl perl);
use GraphSetup;


my $bestfit_fn = $scriptdir."/comp.dat";
open IN, "<", $bestfit_fn or die "Couldn't open $bestfit_fn!\n";
my @bestfit = split(" ", scalar <IN>);
close IN;
shift @bestfit;
@bestfit = translate(@bestfit);


open IN, "<", "$scriptdir/data/parameter_correlations/pcorrs.dat";
while (<IN>) {
    my (@a) = split;
    next if (/^#/);
    $corrs{$a[0]}{$a[1]} = sprintf("%.02f", $a[2]);
}
close IN;

my $fn = "$mcmcdir/mcmc_pdf_stats.dat";
open IN, "<", $fn or die "Couldn't open $fn!\n";
while (<IN>) {
    if (/hist/) {
	my (@a) = split;
	$a[0] =~ /_(\d+)\.dat/;
	my ($j) = ($1);
	my $max = max_of("$mcmcdir/$a[0]");
	if ($j==$params and $bestfit[$params]<$a[1]) {
	    $a[1] = $bestfit[$params];
	}
	new XmGrace(xlabel => $labels[$j],
		    ylabel => 'Probability Density',
		    legendxy => "0.86, 0.82",
		    data => [{data => "file:$mcmcdir/$a[0]"},
			     {data => "$bestfit[$j] 0\n$bestfit[$j] $max", lstyle => 3, legend => "Best-Fit Model"}],
		    XmGrace::linscale('x', $a[1], $a[2]),
		    XmGrace::linscale('y', 0, $max),
	    )->write_pdf("pdfs/AGR/hist_${j}.agr");
    }
    if (/dist/) {
	my (@a) = split;
	$a[0] =~ /(\d+)_(\d+)/;
	my ($j, $k) = ($1, $2);
	warn("$j $k\n");
	if ($j==$params and $bestfit[$params]<$a[1]) {
	    $a[1] = $bestfit[$params];
	}
	if ($k==$params and $bestfit[$params]<$a[3]) {
	    $a[3] = $bestfit[$params];
	}
	warn("$bestfit[$j] $bestfit[$k] $corrs{$j}{$k}\n");
	new XmGrace(xlabel => $labels[$j],
		    ylabel => $labels[$k],
		    legendxy => "0.25, 0.23",
		    text => [($j<$params and $k<$params) ? {text => "Rank Corr: $corrs{$j}{$k}", pos => "0.28, 0.8"} : ()],
		    data => [{data => "file:$mcmcdir/$a[0]", metatype => "xydensity", ssize => 1.6},
			     {data => "$bestfit[$j] $bestfit[$k]", metatype => "scatter", ssize => 1.6, stype => 1, legend => "Best-Fit Model"}],
		    XmGrace::linscale('x', $a[1], $a[2]),
		    XmGrace::linscale('y', $a[3], $a[4]),
	    )->write_pdf("pdfs/AGR/dist_${j}_${k}.agr");
    }
#    exit if (++$c == 10);
}
close IN;
    

sub max_of {
    my $fn = shift;
    local ($_, *IN);
    open IN, "<", $fn;
    my $max = 0;
    while (<IN>) {
	my @a = split;
	$max = $a[1] if ($a[1] > $max);
    }
    close IN;
    return $max;
}

sub translate {
     my @names = qw/EFF_0 EFF_0_A EFF_0_A2 EFF_0_A3 V_1 V_1_A V_1_A2 V_1_A3 ALPHA ALPHA_A ALPHA_A2 ALPHA_A3 BETA BETA_A BETA_A2 DELTA GAMMA GAMMA_A GAMMA_A2 INTR_SCATTER INTR_SCATTER_A INTR_SCATTER_SF_UNCORR R_CEN R_CEN_A R_WIDTH R_MIN RDECAY Q_LVMP Q_LVMP_A Q_LVMP_Z Q_SIG_LVMP Q_SIG_LVMP_A Q_SIG_LVMP_Z Q_MIN Q_MIN_A ICL_DIST OBS_SM_SIG OBS_SM_SIG_Z MU MU_A KAPPA_A DUST_M DUST_M_Z DUST_WIDTH ORPHAN_THRESH CHI2_2 CHI2/;
     my %line;
     @line{@names} = @_;
     $line{EFF_0_A} -= $line{EFF_0_A2} + $line{EFF_0_A3};
     $line{V_1_A} -= $line{V_1_A2} + $line{V_1_A3};
     $line{ALPHA_A} -= $line{ALPHA_A2} + $line{ALPHA_A3};
     $line{BETA_A} -= $line{BETA_A2};
     $line{GAMMA_A} -= $line{GAMMA_A2};
     $line{Q_LVMP_A} -= $line{Q_LVMP_Z};
     $line{Q_SIG_LVMP_A} -= $line{Q_SIG_LVMP_Z};
     $line{DUST_WIDTH} = 0.23/$line{DUST_WIDTH};
     $line{DUST_M} = -20-2.5*($line{DUST_M}-9.2);
     $line{DUST_M_Z} = -2.5*$line{DUST_M_Z};
     $line{CHI2_2} = $line{CHI2} unless ($line{CHI2_2}>0);
     return @line{@names};
}

BEGIN {
our $params = 46;

    our @labels = qw(
\xe\f{}\s0\N
\xe\f{}\sa\N
\xe\f{}\sz\N
\xe\f{}\sla\N
V\s0
V\sa
V\sz
V\sla
\xa\f{}\s0
\xa\f{}\sa
\xa\f{}\sz
\xa\f{}\sla
\xb\f{}\s0
\xb\f{}\sa
\xb\f{}\sz
\xd
\xg\f{}\s0
\xg\f{}\sa
\xg\f{}\sz
\xs\f{}\sSF,0
\xs\f{}\sSF,a
f\sshort
V\sR,0
V\sR,a
r\swidth
r\smin
\xt\f{}\sR
V\sQ,0
V\sQ,a
V\sQ,z
\xs\f{}\sVQ,0
\xs\f{}\sVQ,a
\xs\f{}\sVQ,z
Q\smin,0
Q\smin,a
f\smerge
\xs\f{}\sSM
\xs\f{}\sSM,z
\xm\f{}\s0
\xm\f{}\sa
\xk\f{}
M\sdust,4
M\sdust,z
\xa\f{}\sdust
T\smerge,300
T\smerge,1000
\xc\S\v{-0.3}2
	);
}


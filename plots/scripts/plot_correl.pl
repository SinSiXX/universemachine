#!/usr/bin/perl -w
use lib qw(../perl ../../perl perl);
use GraphSetup;

our $datadir = "$dir/data/corrs";
our $plotdir = "$dir/graphs/corrs";
File::Path::make_path($plotdir, "$plotdir/AGR", "$plotdir/EPS", "$plotdir/PNG", "$plotdir/PDF");

my @zs = (0, 0.1, 0.5, 1, 2, 3, 4, 5, 6, 7, 8, 9);
my @scales = get_scales_at_redshifts("$datadir", "corr", @zs);
my %zs = map { $scales[$_] => $zs[$_] } (0..$#zs);
my @sm_lower = qw(10.00 10.50 11.00);
my @sm_upper = qw(10.50 11.00 14.00);
open OUT, ">$plotdir/scales.txt";
print OUT "#Redshift Scale_Factor_Of_Snapshot_Used Actual_Redshift\n";
print OUT "$zs[$_] $scales[$_] ", 1/$scales[$_]-1,"\n" for (0..$#zs);
close OUT;

for my $a (@scales) {
    for (0..$#sm_lower) {
	my $sml = $sm_lower[$_];
	my $smh = $sm_upper[$_];
	my $fn = sprintf("corr_sm%.1f_%.1f_z%g", $sml, $smh, $zs{$a});
	$fn =~ tr/./_/;
	my $sm_text = sprintf('10\S%g\N M%s < M\s*\N < 10\S%g\N M%s', $sml, $sun_txt, $smh, $sun_txt);
	new XmGrace(xlabel => $rp_label,
		    ylabel => $wp_label,
		    legendxy => "0.8, 0.8",
		    xmin => 0.098, xmax => 10,
		    ymin => 8, ymax => 4500,
		    color_errorstrip => 1,
		    transparent_colors => $hq,
		    text => [{text => "z = $zs{$a}\\n".$sm_text,  pos => "0.3, 0.23"}],
		    data => [{data => "file[0,7,8,9]:$datadir/corr_sm${sml}-${smh}_a$a.dat", metatype => "errorstrip", fcolor => 18},
			     {data => "file[0,1,2,3]:$datadir/corr_sm${sml}-${smh}_a$a.dat", metatype => "errorstrip", fcolor => 17},
			     {data => "file[0,4,5,6]:$datadir/corr_sm${sml}-${smh}_a$a.dat", metatype => "errorstrip", fcolor => 19},
			     {data => "file[0,7,8,9]:$datadir/corr_sm${sml}-${smh}_a$a.dat", lcolor => 2, legend => "Quenched",}, 
			     {data => "file[0,1,2,3]:$datadir/corr_sm${sml}-${smh}_a$a.dat", lcolor => 1, legend => "All",}, 
			     {data => "file[0,4,5,6]:$datadir/corr_sm${sml}-${smh}_a$a.dat", lcolor => 3, legend => "Star-Forming"}])->write_pdf("$plotdir/AGR/$fn.agr");
    }
}


@scales = get_scales_at_redshifts("$datadir", "corr", 0, 1, 2);
@types = qw(all sf q);
@labels = ("All Galaxies", "Star-Forming Galaxies", "Quenched Galaxies");
for (0..$#sm_lower) {
    my $sml = $sm_lower[$_];
    my $smh = $sm_upper[$_];
    for (0..$#types) {
	my $type = $types[$_];
	my $offset = 1+3*$_;
	my $color = (1,3,2)[$_];
	my $label = '\v{0.5}'.$labels[$_];
	my $columns = join(",", 0, (map {$_+$offset} (0..2)));
	my $fn = sprintf("corr_cross_redshift_sm%.1f_%.1f_%s", $sml, $smh, $type);
	$fn =~ tr/./_/;
	my $sm_text = sprintf('10\S%g\N M%s < M\s*\N < 10\S%g\N M%s', $sml, $sun_txt, $smh, $sun_txt);
	new XmGrace(xlabel => $rp_label,
		    ylabel => $wp_label,
		    legendxy => "0.95, 0.8",
		    xmin => 0.098, xmax => 10,
		    ymin => 8, ymax => 4500,
		    transparent_colors => $hq,
		    fading_colors => 1,
		    text => [{text => $label,  pos => "1.05, 0.78", just => 2},
			     {text => $sm_text,  pos => "0.3, 0.3"},
			     {text => '\xs\f{}\sz\N = 0.0033(1+z)', pos => "0.3, 0.25"},
			     {text => '\xp\f{}\smax\N = 80 Mpc/h', pos => "0.3, 0.2"},
			     {text => "Model Predictions", pos => "0.3, 0.8"}
		    ],
		    data => [(map { {data => "file[$columns]:$datadir/corr_sm${sml}-${smh}_a$scales[$_].dat", metatype => "errorstrip", fcolor => $color+6*($_+1)} } (0..$#scales)),
			     (map { {data => "file[$columns]:$datadir/corr_sm${sml}-${smh}_a$scales[$_].dat", lcolor => $color+6*$_, legend => "z = $zs{$scales[$_]}"}}  (0..$#scales))])->write_pdf("$plotdir/AGR/$fn.agr");
    }

    my $fn = sprintf("corr_cross_redshift_ratios_sm%.1f_%.1f", $sml, $smh);
    my $sm_text = sprintf('10\S%g\N M%s < M\s*\N < 10\S%g\N M%s', $sml, $sun_txt, $smh, $sun_txt);
    $fn =~ tr/./_/;
    new XmGrace(xlabel => $rp_label,
		ylabel => 'w\sp,Q|SF\N(R\sp\N) / w\sp,All\N(R\sp\N)',
		legend2xy => "0.7, 0.83",
		l2vgap => 0.045,
		legendxy => "0.95, 0.35",
		xmin => 0.098, xmax => 10,
		ymin => 0.09, ymax => 10,
		transparent_colors => $hq,
		fading_colors => 1,
		text => [{text => $sm_text,  pos => "0.3, 0.3"},
			 {text => '\xs\f{}\sz\N = 0.0033(1+z)', pos => "0.3, 0.25"},
			 {text => '\xp\f{}\smax\N = 80 Mpc/h', pos => "0.3, 0.2"},
			 {text => "Model Predictions", pos => "0.3, 0.8"}
		],
		data => [(map { {data => "file[0,7,8,9]:$datadir/corr_ratios_sm${sml}-${smh}_a$scales[$_].dat", metatype => "errorstrip", fcolor => 2+6*($_+1)} } (0..$#scales)),
			 (map { {data => "file[0,4,5,6]:$datadir/corr_ratios_sm${sml}-${smh}_a$scales[$_].dat", metatype => "errorstrip", fcolor => 3+6*($_+1)} } (0..$#scales)),
			 (map { {data => "file[0,7,8,9]:$datadir/corr_ratios_sm${sml}-${smh}_a$scales[$_].dat", lcolor => 2+6*$_}} (0..$#scales)),
			 (map { {data => "file[0,4,5,6]:$datadir/corr_ratios_sm${sml}-${smh}_a$scales[$_].dat", lcolor => 3+6*$_}} (0..$#scales)),
			 (map { { data => "1e5 1", legend => "z = $zs{$scales[$_]}", lcolor => 1+6*$_ } } (0..$#scales)),
			 (map { { data => "1e5 1", legend2 => $labels[$_], lcolor => (4-$_) } } (2,1)),
			 
])->write_pdf("$plotdir/AGR/$fn.agr"); 
}
    
MultiThread::WaitForChildren();

if (-d $paperdir and -d "$paperdir/autoplots") {
    my @files = <$plotdir/PDF/corr_cross_*.pdf>;
    system("cp", @files, "$paperdir/autoplots");
}

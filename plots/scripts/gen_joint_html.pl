#!/usr/bin/perl -w
use lib qw(../perl ../../perl);
use CGI;
use GraphSetup;
use File::Copy;
use File::Path qw(make_path);
my $cells = 45;

my @size = (1720, 1500);
my @im_size = (1720, 1500);
my $convert_images = 1;

my $q = new CGI;
my $dir = "$scriptdir/html/images";
make_path($dir);
if ($convert_images) {
    my $fn = "$scriptdir/PNG/parameter_correlations.png";
    my $fn_out = "$scriptdir/html/images/parameter_correlations.png";
    system("convert", "-extract", "$size[0]x$size[1]+350+250", $fn, $fn_out);
    for my $j (0..$cells) {
	next if ($j==36 or $j==26);
	my $fn = "$scriptdir/pdfs/PNG/hist_$j.png";
	my $fn_out = "$scriptdir/html/images/hist_$j.png";
	system("convert", "-extract", "722x542+70+70", $fn, $fn_out);
	#    copy($fn, $dir) or die "Couldn't copy $fn!\n";
	for my $k (0..$cells) {
	    next if ($j==$k or $j==36 or $k==36 or $j==26 or $k==26);
	    my $fn = "$scriptdir/pdfs/PNG/dist_${j}_$k.png";
	    my $fn_out = "$scriptdir/html/images/dist_${j}_$k.png";
	    system("convert", "-extract", "722x542+70+70", $fn, $fn_out);
	    #	copy($fn, $dir) or die "Couldn't copy $fn!\n";
	}
    }
}

open OUT, ">$scriptdir/html/pcorrs.html" or die "Couldn't open $scriptdir/html/pcorrs.html for writing!\n";
print OUT $q->start_html(
    -title => "Parameter Value Distributions for the UniverseMachine",
    -bgcolor => "white",
    -script => "function writeText(txt) { document.getElementById(\"preview\").innerHTML = txt; }",
    );
print OUT $q->h1("Parameter Rank Correlations");
print OUT "<table style=\"width:100%\">\n<tr>\n<td>\n";
print OUT $q->img({-src => "images/parameter_correlations.png", height => int($size[1]*0.5), width => int($size[0]*0.5), usemap => '#params'});
print OUT "</td>\n<td>";
print OUT $q->h2("Preview");
print OUT $q->p({-id => "preview"}, "Hover mouse over image to get distribution preview; click to directly access file.");
print OUT "</td>\n</tr>\n</table>\n";
print OUT $q->p("Download the full joint distributions <a href=\"joint_pdfs.tar.gz\">here</a>.");
print OUT "<map name=\"params\">\n";
my @offset = (38, 3);
my @cadence = (13.227, 13.659);
my @j_coords = map { int($offset[0] + $_*$cadence[0]) } (0..$cells);
my @k_coords = map { int($offset[1] + $_*$cadence[1]) } (0..$cells);
for my $j (0..$cells) {
    next if ($j==36 or $j==26);
    my $real_j = ($j>36) ? ($j-1) : $j;
    $real_j-- if ($j>26);
    my $lookup_j = $j;
    if ($lookup_j >= 35 and $lookup_j < 44 ) { $lookup_j = 35+($lookup_j+1-35)%9; $lookup_j++ if ($lookup_j==36)}
    for my $k (0..$cells) {
	next if ($k==36 or $k==26);
	my $real_k = ($k>36) ? ($k-1) : $k;
	$real_k-- if ($real_k>26);
	my @c = ($j_coords[$real_j], $k_coords[$real_k],
		 $j_coords[$real_j+1], $k_coords[$real_k+1]);
	my $c = join(",", @c);
	my $lookup_k = $k;
	if ($lookup_k >= 35 and $lookup_k < 44 ) { $lookup_k = 35+($lookup_k+1-35)%9; $lookup_k++ if ($lookup_k==36) }
	my $fn = ($j==$k) ? "images/hist_$lookup_j.png" : "images/dist_${lookup_j}_$lookup_k.png";
	print OUT "<area shape =\"rect\" coords =\"$c\" onmouseover=\"writeText('<img src=\\'$fn\\' />')\" href=\"$fn\" />\n";
	#print OUT "<area shape =\"rect\" coords =\"$c\" onmouseover=\"writeText('Hi!')\" href=\"$fn\" />\n";
    }
}
print OUT "</map>\n";
print OUT $q->end_html();
close OUT;


__END__

<!DOCTYPE html>
<html>
<head>
<script>
function writeText(txt) {
    document.getElementById("desc").innerHTML = txt;
}
</script>
</head>

<body>
<img src ="planets.gif" width ="145" height ="126" alt="Planets" usemap="#planetmap" />

<map name="planetmap">
<area shape ="rect" coords ="0,0,82,126"
onmouseover="writeText('The Sun and the gas giant planets like Jupiter are by far the largest objects in our Solar System.')"
href ="sun.htm" target ="_blank" alt="Sun" />

<area shape ="circle" coords ="90,58,3"
onmouseover="writeText('The planet Mercury is very difficult to study from the Earth because it is always so close to the Sun.')"
href ="mercur.htm" target ="_blank" alt="Mercury" />

<area shape ="circle" coords ="124,58,8"
onmouseover="writeText('Until the 1960s, Venus was often considered a twin sister to the Earth because Venus is the nearest planet to us, and because the two planets seem to share many characteristics.')"
href ="venus.htm" target ="_blank" alt="Venus" />
</map> 

<p id="desc">Mouse over the sun and the planets and see the different descriptions.</p>

</body>
</html>



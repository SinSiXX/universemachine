#!/usr/bin/perl -w
use lib qw(../perl ../../perl perl);
use GraphSetup;

our $datadir = "$dir/data/rejuvenation";
our $plotdir = "$dir/graphs/rejuvenation";
File::Path::make_path($plotdir, "$plotdir/AGR", "$plotdir/EPS", "$plotdir/PNG", "$plotdir/PDF");


my @zs = (0, 1, 2);
my @scales = get_scales_at_redshifts("$datadir", "rejuv", @zs);
my %zs = map { $scales[$_] => $zs[$_] } (0..$#zs);
open OUT, ">$plotdir/scales.txt";
print OUT "#Redshift Scale_Factor_Of_Snapshot_Used Actual_Redshift\n";
print OUT "$zs[$_] $scales[$_] ", 1/$scales[$_]-1,"\n" for (0..$#zs);
close OUT;

my $fn = sprintf("rejuv_sm");
$fn =~ tr/./_/;
new XmGrace(xlabel => $sm_label,
	    ylabel => "Fraction with Past Rejuvenation",
	    XmGrace::logscale('x', 1e9, 1e12),
	    XmGrace::linscale('y', 0, 1, 0.2, 1),
	    color_errorstrip => 1,
	    transparent_colors => $hq,
	    legendxy => "0.3, 0.8",
	    data => [(map {{data => "file:$datadir/rejuv_a$scales[$_].dat", metatype => "errorstrip", transform => XmGrace::tolin(0), smooth => 1}} (0..2)),
		     (map {{data => "file:$datadir/rejuv_a$scales[$_].dat", legend => "z = $zs[$_]", transform => XmGrace::tolin(0), smooth => 1}} (0..2)),
	    ])->write_pdf("$plotdir/AGR/$fn.agr");

$fn = sprintf("rejuv_hm");
$fn =~ tr/./_/;
new XmGrace(xlabel => $hm_label,
	    ylabel => "Fraction with Past Rejuvenation",
	    XmGrace::logscale('x', 1e11, 1e15),
	    XmGrace::linscale('y', 0, 1, 0.2, 1),
	    color_errorstrip => 1,
	    transparent_colors => $hq,
	    legendxy => "0.3, 0.8",
	    data => [(map {{data => "file:$datadir/rejuv_hm_a$scales[$_].dat", metatype => "errorstrip", transform => XmGrace::tolin(0), smooth => 1}} (0..2)),
		     (map {{data => "file:$datadir/rejuv_hm_a$scales[$_].dat", legend => "z = $zs[$_]", transform => XmGrace::tolin(0), smooth => 1,
			 ($_==0) ? (xmax => 8e14) : ()}} (0..2)),
	    ])->write_pdf("$plotdir/AGR/$fn.agr");
	    

MultiThread::WaitForChildren();

if (-d $paperdir and -d "$paperdir/autoplots") {
    my @files = <$plotdir/PDF/rejuv_*.pdf>;
    system("cp", @files, "$paperdir/autoplots");
}

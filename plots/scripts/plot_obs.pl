#!/usr/bin/perl -w
use lib qw(../perl ../../perl perl);
use GraphSetup;

our $datadir = "$dir/data";
our $plotdir = "$dir/graphs/observations";
File::Path::make_path($plotdir, "$plotdir/AGR", "$plotdir/EPS", "$plotdir/PNG", "$plotdir/PDF");


sub to_zp1 {
    my (@a) = split(" ", $_[0]);
    $a[0] = 1.0/$a[0];
    return "@a";
}

sub csfr_transform {
    my ($x, @rest) = split(" ", $_[0]);
    $x = 1+$x;
    if (@rest==1) {
	return $x." ".( 10**$rest[0]);
    }
    return XmGrace::tolin(1..3)->($x." @rest");
}

sub csfr_transform_model {
    my ($x, @rest) = split(" ", $_[0]);
    $x = 1/$x;
    return "$x @rest";
}

sub do_transforms {
    my ($type, $st, $z1, $z2, $sm1, $sm2, $r1, $r2, $v, $u, $d) = @_;
    my $x = 0;
    if ($type =~ /^csfr/) { 
	$x = 0.5*($z1+$z2);
	return csfr_transform("$x $v $u $d");
    }
    if ($type =~ /^cdens_fsf/) {
	$x = 0.5*($sm1+$sm2);
	return np1("$x $v $u $d");
    }
    if ($type =~ /^correlation/ or $type =~ /^cross_correlation/) {
	$x = sqrt(0.5*($r1*$r1+$r2*$r2));
	my @v = split(" ", scalar XmGrace::tolin(1..3)->("$x $v $u $d"));
	$_ /= $h0 for (@v);
	return "@v";
    }
    if ($type =~ /^irx/) {
	$x = 0.5*($sm1+$sm2);
	my @v = split(" ", scalar XmGrace::tolin(1..3)->("$x $v $u $d"));
	return "@v";
    }
}


sub select_data_type {
    my $type = shift;
    my ($zlow, $zhigh, $smlow, $smhigh, $stype) = @_;
    return sub {
	my @a = split(" ", $_[0]);
	return "" unless ($a[0] eq $type);
	my ($st, $z1, $z2, $sm1, $sm2, $r1, $r2, $v, $u, $d) = @a[1,2,3,6,7,10,11,12,13,14];
	return "" if ($zlow and $zlow > 0.5*($z1+$z2));
	return "" if ($zhigh and $zhigh < 0.5*($z1+$z2));
	return "" if ($smlow and $smlow > 0.5*($sm1+$sm2));
	return "" if ($smhigh and $smhigh < 0.5*($sm1+$sm2));
	return "" if ($stype and $stype ne $st);
	return do_transforms($type, $st, $z1, $z2, $sm1, $sm2, $r1, $r2, $v, $u, $d);
    };
}

sub select_model_type {
    my $sub = select_data_type(@_);
    return sub {
	my @a = split(" ", $_[0]);
	@a[12,13,14] = @a[15,16,17];
	return $sub->("@a");
    };
}


sub np1 {
    my (@a) = split(" ", $_[0]);
    $a[0]++;
    return "@a";
}

sub av_np1 {
    my (@a) = split(" ", $_[0]);
    my $x = 1+0.5*($a[1]+$a[0]);
    my @rest = @a[2..4];
    return XmGrace::tolin(1..3)->("$x @rest");
}

sub irx_transform {
    my ($z, $sub) = @_;
    return sub {
	my @a = split(" ", $sub->($_[0]));
	return "" unless (@a);
	$a[0] -= ($z-5.5)/30;
	return "@a";	
    };
}

my %irx_data = ("4" => [3.5, 4.5],
		"5" => [4.5, 5.5],
		"6" => [5.5, 6.5],
		"7" => [6.5, 7.5]);

$c = 6;
my @irx_data;
push @irx_data, {data => "file:$datadir/obs.txt", type => "xydydy", metatype => "errorstrip", fcolor => 18, transform => select_model_type("irx", @{$irx_data{4}})};
for (sort {$a <=> $b} keys %irx_data) {
    push @irx_data, {data => "file:$datadir/obs.txt", type => "xydydy", metatype => "scatter", ssize => 1, legend => "Data", transform => irx_transform($_, select_data_type("irx", @{$irx_data{$_}})), scolor => $c, lcolor => $c, legend => "z = $_"};
    $c++;
}
$c = 6;
for (sort {$a <=> $b} keys %irx_data) {
    push @irx_data, {data => "file:$datadir/obs.txt", type => "xy", transform => select_model_type("irx", @{$irx_data{$_}}), scolor => $c, lcolor => $c};
    $c++;
}
push @irx_data, {data => "0 0", legend => "Model", lcolor => 1};

new XmGrace(xlabel => $uv_label,
	    ylabel => 'Average Infrared Excess',
	    rainbow => 1,
	    legendxy => "0.27, 0.4",
	    ymin => 0.03, ymax => 10, yscale => "Logarithmic", ymajortick => 10, yminorticks => 9,
	    xmin => -22.2, xmax => -17.8, xmajortick => 1, xminorticks => 1, xscale => "Normal",
	    remap_colors => {18 => "192, 255, 255"},
	    data => \@irx_data)->write_pdf("$plotdir/AGR/irx_comp.agr");




$fn = sprintf("csfrs");
$fn =~ tr/./_/;
new XmGrace(xlabel => "z", ylabel => $csfr_label, xmin => 0, xmax => 10,
	    xminorticks => 1, xmajortick => 1, ymin => 1e-3, ymax => 2e-1,
	    @x_zlabel,
	    color_errorstrip => 1,
	    transparent_colors => $hq,
	    legendxy => "0.3, 0.31", 
	    data => [{data => "file[0,7,8,9]:$datadir/csfrs/csfrs.dat", metatype => "errorstrip",  transform => \&csfr_transform_model, fcolor => 18},
				     
		     {data => "file:$datadir/obs.txt", type => "xydydy", metatype => "scatter", ssize => 1, legend => "Data", transform => select_data_type("csfr")},
		     {data => "file:$datadir/obs.txt", type => "xydydy", metatype => "scatter", ssize => 1, scolor => 1, lcolor => 1, transform => select_data_type("csfr_(uv)")},
		     {data => "file:$datadir/csfrs/csfrs.dat", legend => "Model (Observed)", transform => \&csfr_transform_model},
		     {data => "file[0,7]:$datadir/csfrs/csfrs.dat", legend => "Model (True)",  transform => \&csfr_transform_model, lcolor => 2},
		     
	    ])->write_pdf("$plotdir/AGR/$fn.agr");

$fn = sprintf("central_sf_fraction");
$fn =~ tr/./_/;
new XmGrace(legendxy => "0.3, 0.45",
	    xlabel => "Neighbour Counts [0.3 Mpc < R\\sp\\N < 4 Mpc]", 
	    ylabel => 'Star-Forming Fraction',
	    xmin => 0.8, xmax => 150,
	    ymin => 0.4, ymax => 0.8,
	    color_errorstrip => 1,
	    transparent_colors => $hq,
	    xscale => "Logarithmic",
	    xticklabels => {((1 => 0), (map { 1+$_ => $_ } (1,10,100)), (map {(1+$_ => "", 1+10*$_ => "")} (2..9)))},
	    text => [{text => '10\S10\N M'.$sun_txt.' < M\s*\N < 10\S10.5\N M'.$sun_txt.'\n\v{-0.2}Primary Galaxies Only', pos => "0.3, 0.25"}],
	    data => [{data => "file:$datadir/obs.txt", metatype => "errorstrip", transform => select_model_type("cdens_fsf"), fcolor => 18},
		     {data => "file:$datadir/obs.txt", type => "xydydy", metatype => "scatter", ssize => 1, legend => "Data", transform => select_data_type("cdens_fsf")},
		     {data => "file:$datadir/obs.txt", type => "xy", legend => "Model", transform => select_model_type("cdens_fsf"), lcolor => 2}
	    ])->write_pdf("$plotdir/AGR/$fn.agr");



$fn = sprintf("central_av_ssfr");
$fn =~ tr/./_/;
new XmGrace(legendxy => "0.3, 0.45",
	    xlabel => "Neighbour Counts [0.3 Mpc < R\\sp\\N < 4 Mpc]", 
	    ylabel => 'Average SSFR for SF Galaxies [yr\S-1\N]',
            yaxisformat => "scientific",
            yaxisprec => 0,
	    xmin => 0.8, xmax => 150,
	    ymin => 0, ymax => 1e-10, ymajortick => 2e-11, yminorticks => 1,
	    xscale => "Logarithmic",
	    xticklabels => {((1 => 0), (map { 1+$_ => $_ } (1,10,100)), (map {(1+$_ => "", 1+10*$_ => "")} (2..9)))},
	    text => [{text => '10\S10\N M'.$sun_txt.' < M\s*\N < 10\S10.5\N M'.$sun_txt.'\n\v{-0.2}Primary Galaxies Only', pos => "0.3, 0.25"}],
	    data => [{data => "file:$obsdir/density/behroozi_z0.01_z0.05.cdens_ssfr_sf", type => "xydydy", metatype => "scatter", ssize => 1, transform => \&av_np1},
	    ])->write_pdf("$plotdir/AGR/$fn.agr");

$fn = sprintf("central_av_sm");
new XmGrace(legendxy => "0.3, 0.45",
	    xlabel => "Neighbour Counts [0.3 Mpc < R\\sp\\N < 4 Mpc]", 
	    ylabel => 'Log-Averaged Stellar Mass [M\s'.$sun_txt.']',
            yaxisformat => "scientific",
            yaxisprec => 1,
	    xmin => 0.8, xmax => 150,
	    ymin => 1e10, ymax => 2e10, ymajortick => 2e9, yminorticks => 1,
	    xscale => "Logarithmic",
	    xticklabels => {((1 => 0), (map { 1+$_ => $_ } (1,10,100)), (map {(1+$_ => "", 1+10*$_ => "")} (2..9)))},
	    text => [{text => '10\S10\N M'.$sun_txt.' < M\s*\N < 10\S10.5\N M'.$sun_txt.'\n\v{-0.2}Primary Galaxies Only', pos => "0.3, 0.25"}],
	    data => [{data => "file:$obsdir/density/behroozi_z0.01_z0.05.cdens_sm", type => "xydydy", metatype => "scatter", ssize => 1, transform => \&av_np1},
	    ])->write_pdf("$plotdir/AGR/$fn.agr");

open IN, "<$datadir/obs.txt";
while (<IN>) {
    next unless (/^correlation /);
    my @a = split;
    my ($st,@keys) = @a[1,2,3,6,7];
    $corrs{"@keys"}{$st}++;
}
close IN;
my @corrs = sort keys %corrs;
for my $corr (@corrs) {
    my ($z1, $z2, $sm1, $sm2) = split(" ", $corr);
    my $zavg = sprintf("%.2f", (0.5*($z1**3+$z2**3))**(1/3));
    my ($z1t) = sprintf("%g", $z1);
    my ($z2t) = sprintf("%g", $z2);
    my $sm = sprintf("%g", 0.5*($sm1+$sm2));
    my %text = ('10.4' => '10\S10.3\N M'.$sun_txt.' < M\s*\N < 10\S10.5\N M'.$sun_txt, 
		'10.75' => '10\S10.5\N M'.$sun_txt.' < M\s*\N < 10\S11\N M'.$sun_txt,
		'12' => '10\S11\N M'.$sun_txt.' < M\s*\N');
    my $fn = sprintf("corr_sm%g_%g_z%g", $sm1, $sm2, $zavg);
    $fn =~ tr/./_/;
    new XmGrace(xlabel => $rp_label,
		ylabel => $wp_label,
		legendxy => "0.8, 0.8",
		xmin => 0.098, xmax => 10,
		ymin => 9.8, ymax => 4000,
		color_errorstrip => 1,
		transparent_colors => $hq,
		text => [{text => (($z1 =~ /0\.0/) ? '\v{0.5}z = '.$zavg.'\N\n' : '\v{0.5}'.$z1t.' < z < '.$z2t.'\N\n').'\v{0.4}Autocorrelation\n'.$text{$sm}, pos => "0.3, 0.28"}], 
		data => [{data => "file:$datadir/obs.txt", metatype => "errorstrip", transform => select_model_type("correlation", $z1, $z2, $sm1, $sm2, 'q'), fcolor => 18},
			 (defined($corrs{$corr}{a}) ? {data => "file:$datadir/obs.txt", metatype => "errorstrip", transform => select_model_type("correlation", $z1, $z2, $sm1, $sm2, 'a'), fcolor => 17} : ()),
			 {data => "file:$datadir/obs.txt", metatype => "errorstrip", transform => select_model_type("correlation", $z1, $z2, $sm1, $sm2, 's'), fcolor => 19},
			 {data => "file:$datadir/obs.txt", metatype => "scatter", legend => "Quenched", ssize => 1, scolor => 2, lcolor => 2, type => "xydydy",  transform => select_data_type("correlation", $z1, $z2, $sm1, $sm2, 'q')},
			 (defined($corrs{$corr}{a}) ?
			  {data => "file:$datadir/obs.txt", metatype => "scatter", legend => "All", ssize => 1, scolor => 1, lcolor => 1, type => "xydydy",  transform => select_data_type("correlation", $z1, $z2, $sm1, $sm2, 'a')} : ()),
			 {data => "file:$datadir/obs.txt", metatype => "scatter", legend => "Star-Forming", ssize => 1, scolor => 3, lcolor => 3, type => "xydydy",  transform => select_data_type("correlation", $z1, $z2, $sm1, $sm2, 's')},
			 
			 {data => "file:$datadir/obs.txt", transform => select_model_type("correlation", $z1, $z2, $sm1, $sm2, 'q'), lcolor => 2},
			 (defined($corrs{$corr}{a}) ? {data => "file:$datadir/obs.txt", transform => select_model_type("correlation", $z1, $z2, $sm1, $sm2, 'a'), lcolor => 1} : ()),
			 {data => "file:$datadir/obs.txt", transform => select_model_type("correlation", $z1, $z2, $sm1, $sm2, 's'), lcolor => 3},
			    {data => "0 0", legend => "Model", lcolor => 1},
			 
		])->write_pdf("$plotdir/AGR/$fn.agr");
}



open IN, "<$datadir/obs.txt";
while (<IN>) {
    next unless (/^cross_correlation /);
    my @a = split;
    my ($st,@keys) = @a[1,2,3,6,7];
    $xcorrs{"@keys"}{$st}++;
}
close IN;
my @xcorrs = sort keys %xcorrs;
for my $corr (@xcorrs) {
    my ($z1, $z2, $sm1, $sm2) = split(" ", $corr);
    my $zavg = sprintf("%.2f", (0.5*($z1**3+$z2**3))**(1/3));
    my ($z1t) = sprintf("%g", $z1);
    my ($z2t) = sprintf("%g", $z2);
    my $sm = sprintf("%g", 0.5*($sm1+$sm2));
    my %text = ('10.4' => '10\S10.3\N M'.$sun_txt.' < M\s*\N < 10\S10.5\N M'.$sun_txt, 
		'10.75' => '10\S10.5\N M'.$sun_txt.' < M\s*\N < 10\S11\N M'.$sun_txt,
		'12' => '10\S11\N M'.$sun_txt.' < M\s*\N');
    my $fn = sprintf("xcorr_sm%g_%g_z%g", $sm1, $sm2, $zavg);
    $fn =~ tr/./_/;
    new XmGrace(xlabel => $rp_label,
		ylabel => $wp_label,
		legendxy => "0.8, 0.8",
		xmin => 0.098, xmax => 10,
		ymin => 9.8, ymax => 4000,
		color_errorstrip => 1,
		transparent_colors => $hq,
		text => [{text => (($z1 =~ /0\.0/) ? '\v{0.5}z = '.$zavg.'\N\n' : '\v{0.5}'.$z1t.' < z < '.$z2t.'\N\n').'\v{0.4}Cross Correlation\n'.("[".$text{$sm}.'] \x\#{b4}\f{} ['.$text{10.4}."]"), pos => "0.3, 0.28"}], 
		data => [(defined($xcorrs{$corr}{a}) ? {data => "file:$datadir/obs.txt", metatype => "errorstrip", transform => select_model_type("cross_correlation", $z1, $z2, $sm1, $sm2, 'a'), fcolor => 17} : ()),
			 (defined($xcorrs{$corr}{a}) ?
			  {data => "file:$datadir/obs.txt", metatype => "scatter", legend => "All", ssize => 1, scolor => 1, lcolor => 1, type => "xydydy",  transform => select_data_type("cross_correlation", $z1, $z2, $sm1, $sm2, 'a')} : ()),
			 (defined($xcorrs{$corr}{a}) ? {data => "file:$datadir/obs.txt", transform => select_model_type("cross_correlation", $z1, $z2, $sm1, $sm2, 'a'), lcolor => 1} : ()),
			    {data => "0 0", legend => "Model", lcolor => 1},
			 
		])->write_pdf("$plotdir/AGR/$fn.agr");
}


MultiThread::WaitForChildren();

if (-d $paperdir and -d "$paperdir/autoplots") {
    my @files = (<$plotdir/PDF/csfr*.pdf>, <$plotdir/PDF/central_*.pdf>, <$plotdir/PDF/corr_sm*.pdf>, <$plotdir/PDF/xcorr_sm*.pdf>, <$plotdir/PDF/irx_comp*.pdf>);
    system("cp", @files, "$paperdir/autoplots");
}

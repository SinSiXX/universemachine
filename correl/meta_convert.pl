#!/usr/bin/perl -w

my @lims = qw/10.3 10.5 11 13/;
my $cpath = "../../SDSS/DR10/corrs";
for (0..($#lims-1)) {
    my ($ml, $mh) = @lims[$_,$_+1];
    my $rmin = ($ml < 11) ? 0.09 : 0.19;
    system("perl convert_to_corr_plus_pca.pl $cpath/corr_all_${ml}_$mh.dat $cpath/corr_q_${ml}_$mh.dat $cpath/corr_sf_${ml}_$mh.dat correl_${ml}_$mh.dat $rmin 10\n");
}

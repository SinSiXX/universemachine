#!/usr/bin/perl -w
while (<>) {
    next if (/^#/);
    my ($r, $a, $sf, $q) = split;
    push @{$a{$r}}, ($a>0) ? log($a)/log(10) : -1;
    push @{$sf{$r}}, ($sf > 0) ? log($sf)/log(10) : -1;
    push @{$q{$r}}, ($q>0) ? log($q)/log(10) : -1;
}

my @keys = sort { $a <=> $b } keys %a;

trial_decorrelate(4,6);
trial_decorrelate(8,7);
trial_decorrelate(1,8);
trial_decorrelate(6,9);

sub trial_decorrelate {
    my ($b, $c) = @_;

    for my $i (0..$#keys) {
	($av[$i], $stdev[$i]) = AvStDev($a{$keys[$i]});
	for my $j (0..$#keys) {
	    $corr[$i][$j] = Correlate($a{$keys[$i]}, $a{$keys[$j]});
	    printf "%+.3f ", $corr[$i][$j];
	}
	print "\n";
    }
    print "\n";

    print "Trial decorrelation ($keys[$b]: $av[$b]+/-$stdev[$b]):\n";
    open OUT, ">covar$c.dat";
    my $kb = $keys[$b];
    for my $i (0..(@{$a{$kb}}-1)) {
	my $norm = $a{$kb}[$i];
	my $r = ($norm - $av[$b])/$stdev[$b];
	for my $k (0..$#keys) {
	    local $_ = $keys[$k];
	    $a{$_}[$i] -= $r*$corr[$k][$b]*$stdev[$k]+$av[$k];
	    $a{$_}[$i] = 0 if ($k==$b);
	    $q{$_}[$i] -= $r*$corr[$k][$b]*$stdev[$k]+$av[$k];
	    $sf{$_}[$i] -= $r*$corr[$k][$b]*$stdev[$k]+$av[$k];
	    print OUT "$_ ", 10**$a{$_}[$i]," ", 10**$q{$_}[$i]," ", 10**$sf{$_}[$i],"\n";
	}
	print OUT "\n";
    }
    close OUT;
    print "\n";
}


for my $i (0..$#keys) {
    ($av[$i], $stdev[$i]) = AvStDev($a{$keys[$i]});
    for my $j (0..$#keys) {
	$corr[$i][$j] = Correlate($a{$keys[$i]}, $a{$keys[$j]});
	printf "%+.3f ", $corr[$i][$j];
    }
    print "\n";
}
print "\n";

for my $i (0..$#keys) {
    for my $j (0..$#keys) {
	$corr[$i][$j] = Correlate($a{$keys[$i]}, $sf{$keys[$j]});
	printf "%+.3f ", $corr[$i][$j];
    }
    print "\n";
}

print "\n";
for my $i (0..$#keys) {
    for my $j (0..$#keys) {
	$corr[$i][$j] = Correlate($a{$keys[$i]}, $q{$keys[$j]});
	printf "%+.3f ", $corr[$i][$j];
    }
    print "\n";
}


sub Correlate {
    my ($sx, $sy, $sx2, $sy2, $sxy, $sw) = map { 0 } (1..6);
    my ($m1, $m2) = @_;
    my $n = @$m1/2;
    for (0..($n-1)) {
        my ($x, $y) = ($m1->[$_], $m2->[$_]);
        $sx += $x;
        $sy += $y;
        $sw++;
    }
    return 0 unless ($sw > 0);
    $sx /= $sw;
    $sy /= $sw;
    for (0..($n-1)) {
        my ($x, $y) = ($m1->[$_], $m2->[$_]);
        $sx2 += ($x-$sx)*($x-$sx);
        $sy2 += ($y-$sy)*($y-$sy);
        $sxy += ($x-$sx)*($y-$sy);
    }
    $sx2 /= $sw;
    $sy2 /= $sw;
    $sxy /= $sw;
    return 0 unless ($sx2*$sy2 > 0);
    return ($sxy/(sqrt($sx2*$sy2)));
}


sub AvStDev {
    my ($sx, $sx2, $sw) = map { 0 } (1..6);
    my ($m1) = @_;
    my $n = @$m1/2;
    for (0..($n-1)) {
        my ($x) = ($m1->[$_]);
        $sx += $x;
        $sw++;
    }
    return 0 unless ($sw > 0);
    $sx /= $sw;
    for (0..($n-1)) {
        my ($x) = ($m1->[$_]);
        $sx2 += ($x-$sx)*($x-$sx);
    }
    $sx2 /= $sw;
    return ($sx, 0) unless ($sx2 > 0);
    return ($sx, sqrt($sx2));
}

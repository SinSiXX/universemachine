#!/usr/bin/perl -w
use Statistics::PCA;

while (<>) {
    next if (/^#/);
    my ($r, $a, $sf, $q) = split;
    push @{$a{$r}}, ($a>0) ? log($a*$r)/log(10) : -1;
    push @{$sf{$r}}, ($sf > 0) ? log($sf*$r)/log(10) : -1;
    push @{$q{$r}}, ($q>0) ? log($q*$r)/log(10) : -1;
}

my @keys = sort {$a <=> $b} keys %a;
my @full;
for (@keys) {
    push @full, $a{$_}[0];
    push @full, $sf{$_}[0];
    push @full, $q{$_}[0];
}

for my $i (1..(@{$a{$keys[0]}}-1)) {
    my $arr;
    for (@keys) {
	push @$arr, $a{$_}[$i];
	push @$arr, $sf{$_}[$i];
	push @$arr, $q{$_}[$i];
    }
    for (0..(@$arr-1)) {
	$av[$_]+=$arr->[$_];
    }
    push @data, $arr;
}

$av[$_] /=@data for (0..$#av);
print "Offsets:\n";
for (0..$#av) {
    print " ", $full[$_]/$av[$_];
}
print "\n";


my $pca = Statistics::PCA->new;
$pca->load_data({format => 'table', data => \@data});
$pca->pca();

my @labels = map { ("a.$_", "sf.$_", "q.$_") } @keys;

my @important = (0..29);
my @reconstruction;

my $c = 0;
print "\nPC   Prop.Var.  Cum.Var.  Av. +/- SD;  Sig.Contributors\n";
for my $i ($pca->results('full')) {
#    next unless ($i->[3]<0.85);
    my @sig = map { (abs($i->[5][$_]) > 0.1) ? ([$labels[$_], $i->[5][$_]]) : () } (0..$#important);
    my $d = $data[0];
    my $s = 0;
    for (0..(@$d-1)) { $s += $i->[5][$_]*($d->[$_]-$av[$_]); }
    for (0..(@$d-1)) { $reconstruction[$c][$_] = $i->[5][$_]*$s; }
    $c++;
    next unless @sig;
    my ($av, $sd) = val_sd($i->[5]);
    printf "PC%d:  %.3f   %.3f  %.3e +/- %.3e (%.3e) [%s]", $i->[0], $i->[2], $i->[3], $av, $sd, $s, "@{$i->[5]}";
#    @sig = sort { abs($b->[1]) <=> abs($a->[1]) } @sig;
#    my $tot = 0;
#    for (@sig) {
#        $tot += $_->[1]**2;
        #printf "%s(%.3f) ", $_->[0], $_->[1];
#        last if ($tot > 0.8);
#    }
    print "\n";
}

print "\n";
my @transformed = $pca->results('transformed');
print "Expected transform: ";
for (0..29) { print "$transformed[$_][0] ";}
print "\n";

my $max_c = 5;
print "\n";
print "Reconstructions:\n";
for my $i (map {$_*3} (0..9)) {
    my $t = $av[$i];
    my $l = $labels[$i];
    $l =~ s/a\.//;
    print $l, " ", $data[0][$i];
    for (0..$max_c) {
	$t += $reconstruction[$_][$i];
	print " $t";
    }
    print "\n";
}
print "\n";


sub val_sd {
    my $pc = shift;
    my $av=0;
    my $av2=0;
    for (0..$#data) {
	my $s = 0;
	my $d = $data[$_];
	for (0..(@$d-1)) { $s += $pc->[$_]*($d->[$_]-$av[$_]); }
	$av += $s;
	$av2 += $s*$s;
    }
    $av /= @data;
    $av2 /= @data;
    $av2 -= $av*$av;
    my $sd = ($av2>0) ? sqrt($av2) : 0;
    return ($av, $sd);
}

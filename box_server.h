#ifndef _BOX_SERVER_H_
#define _BOX_SERVER_H_

#include <stdio.h>
#include <inttypes.h>

#define UM_MAGIC (uint64_t)(0x5c00b1d00b1d0000)
#define BOX_SERVER_TYPE 1
#define LAUNCHER_TYPE 0
#define QUIT_TYPE -1

int sort_by_address(const void *a, const void *b);
void print_time(FILE *output);
void box_server_loop(void);
void recover_outputs(void);

extern int64_t box_id;

#endif /*_BOX_SERVER_H_*/

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <math.h>
#include "../stats.h"
#include "../mt_rand.h"


int main(void) {
  double r;
  int64_t samples, i;
  printf("Enter samples: ");
  scanf("%"SCNd64, &samples);
  printf("Enter effect size: ");
  scanf("%lf", &r);

  r250_init(87L);
  struct correl_stats *cs = init_correl(1000);
  for (i=0; i<samples; i++) {
    double x = normal_random(0, 1);
    double y = normal_random(0, 1);
    if (dr250() > 0.5) { x+=r; y+=r; }
    add_to_correl(cs, x, y);
  }
  print_rank_correl(cs, stdout);
  return 0;
}

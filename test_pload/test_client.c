#include <stdio.h>
#include <stdlib.h>
#include "../inet/pload.h"
#include "../check_syscalls.h"

int main(void) {
  char *data = NULL;
  int64_t i;
  for (i=1; i<100; i++) {
    if (!check_fork()) {
      data = pload("out", i, 100, data, 1024*1024, NULL);
      printf("recv'd file %d.\n", data[1000000]);
      return 0;
    }
  }
  for (i=1; i<100; i++)
    check_waitpid(-1);
  return 0;
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <math.h>
#include <inttypes.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <assert.h>
#include "check_syscalls.h"
#include "make_sf_catalog.h"
#include "stats.h"

int main(int argc, char **argv) {
  int64_t i, new_length;
  if (argc<2) {
    printf("Usage: %s sfr_list.bin [centrals_only skip]\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  
  int64_t skip = 0;
  if (argc>3) skip = atol(argv[3]);
  int64_t centrals_only = 0;
  if (argc>2) centrals_only = atol(argv[2]);
  struct catalog_halo *p = check_mmap_file(argv[1], 'r', &new_length);
  int64_t num_p = new_length / sizeof(struct catalog_halo);
  struct binstats *bs = init_binstats(10,14,4,1,0);

  for (i=skip; i<num_p; i++) {
    struct catalog_halo *tp = p+i;
    if (tp->flags & IGNORE_FLAG) continue;
    if (centrals_only && tp->upid > -1) continue;
    /*float ssfr = 0;
    if (tp->sm > 0)
    ssfr = tp->obs_sfr / tp->obs_sm;*/
    float smhm = tp->sm*0.68 / tp->mp;
    if (smhm>0) smhm = log10(smhm);
    else smhm = -10;
    add_to_binstats(bs, log10(tp->mp/0.68), smhm);
  }
  munmap(p, new_length);
  print_medians(bs, stdout);
  return 0;
}

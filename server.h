#ifndef _SERVER_H_
#define _SERVER_H_

#include <stdint.h>
#include "make_sf_catalog.h"

struct client_info {
  int type, port;
  int64_t cs, serv_port, id, *ready, done_steps, sent_steps;
  char *address;
  float max_bounds[6];
  float *bounds;
  void *walker;
  int64_t chunk;
  int64_t sent_time;
};

#define timed_output(...) if (logfile) { print_time(logfile); fprintf(logfile, __VA_ARGS__); fflush(logfile); }

#endif /* _SERVER_H_ */

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <assert.h>
#include <math.h>
#include <string.h>
#include "../orphans.h"
#include "../read_tree/read_tree.h"
#include "../universe_time.h"
#include "../universal_constants.h"
#include "../masses.h"


double box_size = 250;
double Om = 0.307;
double Ol = 0.693;
double h0 = 0.68;

double dist(float *p1, float *p2) {
  double ds = 0, dx;
  int64_t i;
  for (i=0; i<3; i++) {
    dx = p1[i]-p2[i];
    ds += dx*dx;
  }
  return sqrt(ds);
}

double m_to_rvir(double m, double a);

double vir_density(double a) {
  double x = 1.0/(1.0+a*a*a*Ol/Om)-1.0;
  return ((18*M_PI*M_PI + 82.0*x - 39*x*x)/(1.0+x));
}

double dynamical_time(double a) {
  double vd = vir_density(a) * Om * CRITICAL_DENSITY;
  double dt = 1.0/sqrt((4.0*M_PI*Gc/3.0)*vd)*pow(a, 1.5) / h0; //In units of Mpc / (km/s)
  dt *= HUBBLE_TIME_CONVERSION * 100.0;
  return dt;
}



int main(int argc, char **argv) {
  struct halo h={0}, p={0}, p2={0};

  if (argc < 4) {
    printf("Usage: %s frac_vvir frac_rad initial_scale\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  init_time_table(Om, h0);
  gen_ff_cache();
  h.scale = p2.scale = atof(argv[3]);
  h.mvir = 1e12;
  h.vmax = 250;
  p2.mvir = 1e14;
  p2.rvir = m_to_rvir(p2.mvir, p2.scale);
  p2.rs = p2.rvir / 3.0;

  p2.pos[0] = p2.pos[1] = p2.pos[2] = 100;
  memcpy(h.pos, p2.pos, sizeof(float)*3);
  h.pos[0] += p2.rvir / 1e3;

  float vvir = sqrt(Gc*p2.mvir / (p2.scale*p2.rvir*1e-3));
  float v = atof(argv[1])*vvir;
  float fr = atof(argv[2]);
  if (fr > 1) fr = 1;
  if (fr < -1) fr = -1;
  h.vel[0] = fr*v;
  h.vel[1] = sqrt(1.0-fr*fr)*v;

  printf("#A X Y R V M Vmax\n");
  while (p2.scale < 1.0) {
    p = p2;
    p2.scale *= 1.01;
    p2.mvir *= p2.scale/p.scale;
    p2.rvir = m_to_rvir(p2.mvir, p2.scale);
    p2.rs = p2.rvir / 3.0;
    evolve_orphan_halo(&h, &p, &p2);
/*double calc_vmax(struct halo *h) {
  double vacc = h->first_acc_halo->vmax;
  double mratio = h->mvir / h->first_acc_halo->mvir;
  return vacc*pow(mratio, 0.44)*pow(0.5+0.5*mratio, -0.6);
  }*/
    h.vmax = 250.0*(pow(h.mvir / 1e12, 0.44)*pow(0.5+0.5*(h.mvir/1e12), -0.6));
    printf("%f %f %f %f %f %f %f\n", p2.scale, h.pos[0]-p.pos[0], h.pos[1]-p.pos[1], dist(h.pos, p.pos), dist(h.vel, p.vel), log10(h.mvir), h.vmax);
  }
  return 0;
}

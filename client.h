#ifndef _CLIENT_H_
#define _CLIENT_H_

#include <inttypes.h>


struct recipient {
  int64_t cs;
  char *address;
  char *port;
  float *bounds[6];
  void *buffer;
  int64_t buffered;
  int64_t chunk;
};

struct chunk_info {
  char *address, *port;
  float bounds[6];
};

void client(int64_t type);

#endif /* _CLIENT_H_ */

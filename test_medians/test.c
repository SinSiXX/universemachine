#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <inttypes.h>

#define UVSM_SM_BINS 100
#define UVSM_BPMAG 1

float _calc_median_uvsm(float *data, float bin_floor) {
  int64_t i, j, last_i=-1, counts=0;
  double tot = 0, tot2 = 0, min_nonzero = -1;
  for (i=0; i<UVSM_SM_BINS; i++) {
    if (data[i]>0) {
      counts++;
      if (min_nonzero < 0 || min_nonzero > data[i]) min_nonzero = data[i];
    }
  }

  if (counts < 1) return 0;

  //Smooth over bins w/ zeros
  for (i=0; i<UVSM_SM_BINS; i++) {
    if (!data[i] && last_i > -1) {
      while (!data[i] && i<UVSM_SM_BINS) { i++; }
      if (i < UVSM_SM_BINS) {
	data[last_i] -= min_nonzero / 3.0;
	data[i] -= min_nonzero / 3.0;
	assert(i>last_i+1);
	for (j=last_i+1; j<i; j++) data[j] += 2.0*min_nonzero / (float)(3.0*(i-last_i-1));
      }
    }
    if (i<UVSM_SM_BINS && data[i]) last_i = i;
  }

  for (i=0; i<UVSM_SM_BINS; i++) tot += data[i];
  
  float median = bin_floor;
  for (i=0; i<UVSM_SM_BINS; i++) {
    if (tot2 + data[i] > 0.5*tot) {
      float offset = (0.5*tot - tot2) / data[i];
      printf("%f %f %f\n", tot, tot2, offset);
      median = bin_floor + ((float)i + offset) / (float)UVSM_BPMAG;
      break;
    }
    tot2 += data[i];
  }
  return median;
}


int main(int argc, char **argv) {
  float data[UVSM_SM_BINS] = {0};
  data[60] = 2;
  data[80] = 1.5;
  data[65] = 1;
  printf("%f\n", _calc_median_uvsm(data, 0));
  return 0;
}

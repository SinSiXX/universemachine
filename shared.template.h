//shared(a,b)
//a = variable name
//b = size (in units of floats)

shared(smf, num_scales*SM_NBINS);
shared(smf_q, num_scales*SM_NBINS);
shared(smf_q2, num_scales*SM_NBINS);
shared(smf_q3, num_scales*SM_NBINS);
shared(ssfr, num_scales*SM_NBINS);
shared(uvlf, num_scales*UV_NBINS);
shared(ir_excess, num_scales*UV_NBINS);
shared(csfr, num_scales);
shared(csfr_uv, num_scales);
shared(fcorrs, num_scales*NUM_CORR_BINS_PER_STEP);
shared(fxcorrs, num_scales*NUM_XCORR_BINS_PER_STEP);
shared(fccounts, num_scales*NUM_CORR_TYPES*NUM_CORR_THRESHES);
shared(fconfs, num_scales*NUM_CONF_BINS_PER_STEP);
shared(fdens_counts, num_scales*NUM_DENS_BINS);
shared(fdens_sf, num_scales*NUM_DENS_BINS);
shared(fdens_sf_ssfr, num_scales*NUM_DENS_BINS);
shared(uvsm, UVSM_REDSHIFTS*UVSM_UV_BINS*UVSM_SM_BINS);
shared(median_uvsm, UVSM_REDSHIFTS*UVSM_UV_BINS);

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <inttypes.h>
#include <string.h>
#include "sampler.h"
#include "check_syscalls.h"
#include "io_helpers.h"
#include "mt_rand.h"


#ifndef DIM
#define DIM 2
#define NUM_PARAMS 7

double calc_val(double *model, double *pos) {
  return 0;
}

#endif /*DIM !defined*/

#ifndef TOL
#define TOL 0.03
#endif /*TOL !defined*/
 
#ifndef NUM_STEPS
#define NUM_STEPS 500000
#endif /*NUM_STEPS !defined*/

#ifndef INITIAL_GUESS
#define INITIAL_GUESS {0}
#endif /*INITIAL_GUESS !defined*/

struct point {
  double pos[DIM];
  double value, errs[2];
};

struct point *points = NULL;
int64_t num_points = 0;

  
double calc_chi2(double *model, struct point *points, int64_t num_points) {
  int64_t i;
  double chi2=0;

#ifdef INVALID_MODEL
  if (invalid_model(model)) return 1e30;
#endif /*def INVALID_MODEL */
  for (i=0; i<num_points; i++) {
    double err = calc_val(model, points[i].pos) - points[i].value;
    if (fabs(err) < TOL) err = 0;
    else err = fabs(err) - TOL;
    if (err > points[i].errs[0])
      err /= points[i].errs[0];
    else if (err < -points[i].errs[1])
      err /= points[i].errs[1];
    else {
      err /= 0.5*(points[i].errs[0]+points[i].errs[1]);
    }

    chi2 += err*err;
  }
  return chi2;
}

void load_points_from_file(char *filename) {
  int64_t i;
  char buffer[2048];
  FILE *in = check_fopen(filename, "r");
  double params[DIM+3] = {0};
  while (fgets(buffer, 2048, in)) {
    if (read_params(buffer, params, DIM+3) != (DIM+3)) continue;
    check_realloc_every(points, sizeof(struct point), num_points, 1000);
    points[num_points].value = params[DIM];
    for (i=0; i<2; i++) points[num_points].errs[i] = params[DIM+1+i];
    for (i=0; i<DIM; i++) points[num_points].pos[i] = params[i];
    num_points++;
  }
  fclose(in);
}

int main(int argc, char **argv) {
  int64_t i, j;
  double initial_params[NUM_PARAMS] = INITIAL_GUESS;
  double best_params[NUM_PARAMS] = {0}, best_chi2 = -1;

  struct EnsembleSampler *e = new_sampler(100, NUM_PARAMS, 1.7);
  if (argc < 2) {
    fprintf(stderr, "Usage: %s points.dat\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  load_points_from_file(argv[1]);
  
  for (i=0; i<num_points; i++) {
    if (points[i].errs[0] < TOL) points[i].errs[0] = TOL;
    if (points[i].errs[1] < TOL) points[i].errs[1] = TOL;
  }

  for (i=0; i<e->num_walkers; i++) {
    double pos[NUM_PARAMS];
    for (j=0; j<NUM_PARAMS; j++) pos[j] = initial_params[j]+dr250()*0.01;
    init_walker_position(e->w+i, pos);
  }
  
  for (i=0; i<NUM_STEPS; i++) {
    struct Walker *w = get_next_walker(e);
    double chi2 = calc_chi2(w->params, points, num_points);
    if (best_chi2 < 0 || chi2 < best_chi2) {
      memcpy(best_params, w->params, sizeof(double)*NUM_PARAMS);
      best_chi2 = chi2;
    }
    update_walker(e, w, chi2);
    if (ensemble_ready(e)) {
      update_ensemble(e);
#ifdef SAVE_STEPS
      if (i>NUM_STEPS/2)
	write_accepted_steps_to_file(e, stdout, 0);
#endif /*SAVE_STEPS*/
    }
  }
  free_sampler(e);
  for (i=0; i<NUM_PARAMS; i++)
    printf("%le ", best_params[i]);
  printf("%le\n", best_chi2);
  return 0;
}



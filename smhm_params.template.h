shared(smhm_med_params, (SMHM_FIT_PARAMS+1));
shared(smhm_med_cen_params, (SMHM_FIT_PARAMS+1));
shared(smhm_med_sat_params, (SMHM_FIT_PARAMS+1));
shared(smhm_med_sf_params, (SMHM_FIT_PARAMS+1));
shared(smhm_med_q_params, (SMHM_FIT_PARAMS+1));
shared(smhm_med_cen_sf_params, (SMHM_FIT_PARAMS+1));
shared(smhm_med_cen_q_params, (SMHM_FIT_PARAMS+1));
shared(smhm_true_med_params, (SMHM_FIT_PARAMS+1));
shared(smhm_true_med_cen_params, (SMHM_FIT_PARAMS+1));
shared(smhm_true_med_sat_params, (SMHM_FIT_PARAMS+1));
shared(smhm_true_med_sf_params, (SMHM_FIT_PARAMS+1));
shared(smhm_true_med_q_params, (SMHM_FIT_PARAMS+1));
shared(smhm_true_med_cen_sf_params, (SMHM_FIT_PARAMS+1));
shared(smhm_true_med_cen_q_params, (SMHM_FIT_PARAMS+1));
shared(smhm_true_icl_med_params, (SMHM_FIT_PARAMS+1));
shared(smhm_true_cen_icl_med_params, (SMHM_FIT_PARAMS+1));
